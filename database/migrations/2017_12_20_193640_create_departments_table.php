<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id', false, true)->nullable();
            $table->integer('dep_code', false, true)->nullable();
            $table->string('name')->comment('department name');
            $table->text('description')->nullable();
            $table->integer('user_id', false, true)->comment('department head')->nullable();
            $table->integer('sublocation_id', false, true)->nullable();
            $table->softDeletes();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
