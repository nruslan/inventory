<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetworkDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('network_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id', false, true)->nullable();
            $table->string('name')->nullable();
            $table->ipAddress('address')->nullable();
            $table->text('comments')->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('network_details');
    }
}
