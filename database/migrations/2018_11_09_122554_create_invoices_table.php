<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_number', false, true)->nullable();
            $table->integer('document_id', false, true)->nullable();
            $table->integer('tax_code_id', false, true)->nullable();
            $table->integer('vendor_id', false, true)->nullable();
            $table->float('amount', 8, 2)->nullable();
            $table->dateTime('submitted_at')->nullable();
            $table->date('due')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
