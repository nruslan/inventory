<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imodels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id', false, true)->nullable();
            $table->integer('type_id', false, true)->nullable();
            $table->string('name_number')->nullable();
            $table->text('description')->nullable();
            $table->softDeletes();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imodels');
    }
}
