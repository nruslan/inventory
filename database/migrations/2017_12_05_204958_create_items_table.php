<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_tag_id', false, true)->unique();
            $table->string('serial_number');
            $table->integer('sublocation_id', false, true)->nullable();
            $table->integer('imodel_id', false, true)->nullable();
            $table->date('purchase_date')->nullable();
            $table->date('disposal_date')->nullable();
            $table->decimal('price')->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
