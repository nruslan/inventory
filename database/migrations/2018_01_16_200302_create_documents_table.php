<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id', false, true)->nullable();
            //$table->integer('tax_code_id', false, true)->nullable();
            $table->integer('doctype_id', false, true)->comment('document type')->nullable();
            //$table->integer('icon_id', false, true)->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('filename')->nullable();
            $table->bigInteger('size', false, true)->comment('file size')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
