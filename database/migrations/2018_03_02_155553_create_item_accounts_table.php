<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id', false, true);
            $table->integer('item_account_type_id', false, true);
            $table->string('username')->nullable();
            $table->text('password')->nullable();
            $table->integer('strength', false, true)->nullable();
            $table->text('comments')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_accounts');
    }
}
