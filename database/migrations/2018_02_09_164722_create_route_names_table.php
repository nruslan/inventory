<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id', false, true)->nullable();
            $table->integer('parent_id', false, true)->nullable();
            $table->string('title')->nullable();
            $table->string('name')->unique();
            $table->mediumText('description')->nullable();
            $table->softDeletes();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_names');
    }
}
