<?php

use Illuminate\Database\Seeder;

class PhoneNumberTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['title' => 'Office Phone'],
            ['title' => '911 Phone'],
            ['title' => 'FAX line'],
            ['title' => 'DSL'],
        ];

        foreach($items as $item) {
            \App\PhoneNumberType::create($item);
        }
    }
}
