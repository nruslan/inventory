/*let mix = require('laravel-mix');


 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |


mix.js(['resources/js/app.js', 'node_modules/bootstrap-confirmation2/bootstrap-confirmation.js', 'node_modules/admin-lte/dist/js/adminlte.js', 'node_modules/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js'], 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.styles([
    'node_modules/admin-lte/dist/css/AdminLTE.css',
    'node_modules/admin-lte/dist/css/skins/skin-blue.css',
    'node_modules/admin-lte/plugins/iCheck/square/blue.css'
], 'public/css/admin-lte.css');


mix.js('node_modules/admin-lte/plugins/iCheck/icheck.js', 'public/js');
mix.styles([
    'node_modules/admin-lte/dist/css/AdminLTE.css',
    'node_modules/admin-lte/plugins/iCheck/square/blue.css'
], 'public/css/admin-lte-login.css');*/

const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');