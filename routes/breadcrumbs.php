<?php
// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('<i class="fa fa-dashboard"></i> Home', route('home'));
});
//Dashboard
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push('<i class="fa fa-dashboard"></i> Dashboard', route('dashboard.index'));
});

// Home > Items
Breadcrumbs::for('items', function ($trail) {
    $trail->parent('home');
    $trail->push('Inventory Items', route('items.index'));
});
// Home > Items > [Item]
Breadcrumbs::for('items-show', function ($trail, $item) {
    $trail->parent('items');
    $trail->push($item->imodel->name_number.' &bullet; '.$item->imodel->type->name, route('items.show', $item->id));
});

// Home > Documents
Breadcrumbs::for('documents', function ($trail) {
    $trail->parent('home');
    $trail->push('<i class="fa fa-file-o"></i> Documents', route('documents.index'));
});
// Home > Documents > Invoices
Breadcrumbs::for('invoices', function ($trail) {
    $trail->parent('documents');
    $trail->push('<i class="fa fa-file-text"></i> Invoices', route('invoices.index'));
});
Breadcrumbs::for('invoice', function ($trail, $invoice) {
    $trail->parent('invoices');
    $trail->push("# $invoice->invoice_number", route('invoices.show', $invoice->id));
});
Breadcrumbs::for('new_invoice', function ($trail) {
    $trail->parent('invoices');
    $trail->push('<i class="fa fa-plus-square-o"></i> New Invoices', route('invoices.create'));
});

// Home > Maintenance
Breadcrumbs::for('maintenance', function ($trail) {
    $trail->parent('home');
    $trail->push('<i class="fa fa-wrench"></i> Room Maintenance', route('maintenance.index'));
});
// Home > Maintenance > [Room]
Breadcrumbs::for('room', function ($trail, $entry) {
    $trail->parent('maintenance');
    $trail->push($entry->location->location_name , route('maintenance.show', $entry->id));
});
// Home > Maintenance > [By Room]
Breadcrumbs::for('maintenance_by_room', function ($trail, $location) {
    $trail->parent('maintenance');
    $trail->push($location->location_name , route('code.show', $location->id));
});

// Home > Password Manager
Breadcrumbs::for('password_manager', function ($trail) {
    $trail->parent('home');
    $trail->push('<i class="fa fa-lock"></i> Password Manager', route('password-manager.index'));
});
