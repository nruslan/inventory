<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
Auth::routes(['register' => false]);

Route::middleware('auth')
    ->group(function () {
        Route::get('home', function () { return redirect('home/dashboard');});

        Route::group(['prefix' => 'home'], function () {
            Route::get('dashboard', 'Master\Home\IndexController@dashboard')->name('home');
            Route::resource('password-manager', 'Master\Home\PasswordManagerController');
        });


        Route::group(['prefix' => 'dashboard'], function () {
            Route::get('/items/{id}', 'Master\Dashboard\ItemsByTypeController@items')->name('dashboard.items');
            Route::get('/printers', 'Master\Dashboard\ItemsByTypeController@printers')->name('dashboard.printers');
        });
        Route::get('/items/search', 'Master\Items\SearchController@tag_id')->name('search.tagid');
        
        Route::get('/log', 'Master\LogController@index')->name('log.index');
        Route::get('/log/create', 'Master\LogController@create')->name('log.create');
        Route::post('/log', 'Master\LogController@store')->name('log.store');
        Route::get('/log/{log}', 'Master\LogController@show')->name('log.show');
        Route::get('/log/{log}/edit', 'Master\LogController@edit')->name('log.edit');
        Route::patch('/log/{log}', 'Master\LogController@update')->name('log.update');

        Route::get('/items/location/{id}', 'Master\Items\LocationController@show')->name('items.location.show');
        Route::get('/items/location', 'Master\Items\LocationController@index')->name('items.location');
        Route::get('/items/log', 'Master\Items\LogController@index')->name('items.log');

    // Limited Access --------------------------------------------------------------------------------------------------
    Route::group(['middleware' => ['privilege']], function () {

        Route::namespace('Master')->group(function () {

            // Controllers Within The "App\Http\Controllers\Master" Namespace
            Route::get('/dashboard', 'Dashboard\IndexController@index')->name('dashboard.index');
            Route::resource('/ips', 'Items\IpsController');

            //Items
            Route::group(['prefix' => 'items'], function () {
                Route::group(['prefix' => 'reports'], function () {
                    Route::get('/', 'Items\Reports\IndexController@index')->name('items.reports.index');
                    Route::get('purchase-date-range', 'Items\Reports\DateRangeController@purchase_date_range')->name('items.reports.index');
                });
            });

            Route::resource('/items', 'Items\IndexController');
            Route::group(['prefix' => 'item'], function () {
                Route::resource('user', 'Items\UserController', ['except' =>['index', 'show']]);
                Route::resource('document', 'Items\DocumentController', ['only' =>['create', 'store', 'destroy']]);
                Route::resource('documents/list', 'Items\DocumentListController', ['only' =>['edit', 'update']]);
                Route::resource('account', 'Items\AccountController', ['names' => 'items-account']);
                Route::resource('log', 'Items\LogController',[
                    'names' => [
                        'create' => 'item_log.create',
                        'store' => 'item_log.store',
                        'show' => 'item_log.show',
                        'edit' => 'item_log.edit',
                        'update' => 'item_log.update',
                        'destroy' => 'item_log.destroy'
                    ], 'except' => ['index']
                ]);
                Route::resource('disposal-date', 'Items\DisposalDateController', ['only' =>['edit', 'update', 'destroy']]);
                Route::resource('computer-name', 'Items\ComputerNameController')
                    ->only(['store', 'destroy'])
                    ->names(['store' => 'computer_name.store', 'destroy' => 'computer_name.destroy']);
            });

            //Documents
            Route::resource('documents/invoices', 'Documents\InvoiceController');
            Route::resource('documents', 'Documents\IndexController');
            //Accounts
            Route::resource('/accounts', 'WebAccounts\IndexController');
            Route::group(['prefix' => 'accounts'], function () {
                Route::resource('user', 'WebAccounts\UsersController', ['except' =>['index']]);
                Route::resource('categories', 'WebAccounts\CategoriesController', ['only' =>['edit', 'update', 'destroy']]);
            });

            // Server Room Maintenance
            Route::resource('room/code', 'RoomMaintenance\LocationController', ['only' =>['show']]);
            Route::resource('room/maintenance', 'RoomMaintenance\IndexController');

            // End Master Namespace ------
        });

        // End Limited Access middleware -
    });

    // Admin -----------------------------------------------------------------------------------------------------------
    Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
        Route::get('/', 'Admin\IndexController@index')->name('admin');
        Route::resource('/brands', 'Admin\BrandsController');
        Route::resource('/types', 'Admin\TypesController');
        Route::resource('/models', 'Admin\ImodelsController');
        Route::resource('/locations', 'Admin\LocationController');
        Route::resource('/sublocations', 'Admin\SublocationController');

        Route::resource('/web-account-types', 'Admin\WebAccountTypesController');
        Route::resource('/groups', 'Admin\GroupsController');
        Route::resource('/icons', 'Admin\IconsController');
        Route::resource('/categories', 'Admin\CategoriesController');

        Route::resource('/users', 'Admin\User\IndexController');
        Route::resource('/users/privileges', 'Admin\User\PrivilegesController', ['only' =>['edit', 'update', 'destroy']]);
    });
});
