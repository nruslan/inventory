<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/password', 'Master\WebAccounts\IndexController@password');
Route::post('/items-password', 'Master\Items\AccountController@get_password');
Route::post('/item', 'Master\Items\IpsController@get_item');
Route::post('/get-items', 'Master\RoomMaintenance\IndexController@getItems');

Route::resource('documents', 'Master\Documents\IndexController');
