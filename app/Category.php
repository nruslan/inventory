<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    public $timestamps = false;

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'group_id', 'parent_id', 'name', 'description', 'picture_id', 'icon_id'
    ];

    public function group()
    {
        return $this->belongsTo('App\Group', 'group_id');
    }

    // ACCESSORS -------------------------------------------------------------------------------------------------------
    public function getGroupCategoryNameAttribute()
    {
        if($this->group)
            return $this->name.' <i class="fa fa-fw fa-angle-double-right" aria-hidden="true"></i> '.$this->group->name;
        else
            return $this->name.' <i class="fa fa-fw fa-angle-double-right" aria-hidden="true"></i> ---';
    }

    public function getHtmlIconAttribute()
    {
        if($this->icon_id)
            return $this->icon->icon;
        else
            return '<i class="fa fa-fw fa-times-circle-o" aria-hidden="true"></i>';
    }
}
