<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class ItemLog extends Model
{
    protected $fillable = [
        'item_id', 'user_id', 'action', 'entry'
    ];

    // DB Relationships ------------------------------------------------------------------------------------------------
    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getShortEntryAttribute()
    {
        return Str::limit(strip_tags($this->entry), 50);
    }

    public function getItemTagIdAttribute()
    {
        return $this->item_id ? $this->item->inventory_tag_id : null;
    }

    public function getItemsLocationAttribute()
    {
        return $this->item_id ? $this->item->full_location : null;
    }

    public function getDateTimeAttribute()
    {
        return date('m/d/y H:i', strtotime($this->updated_at));
    }

    public function getItemTypeNameAttribute()
    {
        return $this->item_id
            ? $this->item->imodel->type->name.' / '. $this->item->imodel->name_number.' / '. $this->item->brand
            : null;
    }
}
