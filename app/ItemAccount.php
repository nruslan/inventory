<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemAccount extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'item_id', 'item_account_type_id', 'username', 'password', 'strength', 'comments'
    ];

    // DB Relationships
    public function type()
    {
        return $this->belongsTo('App\ItemAccountType', 'item_account_type_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getHiddenPasswordAttribute()
    {
        return str_repeat("*", $this->strength);
    }

    public function getPasswordAttribute($value)
    {
        return decrypt($value);
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = $value ? encrypt($value) : null;
    }
}
