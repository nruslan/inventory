<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    public $timestamps = false;

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'model_number', 'name', 'description'
    ];

    public function imodels()
    {
        return $this->hasMany('App\Imodel', 'type_id');
    }

    // Type Category == Typetegory
    public function typetegory()
    {
        return $this->belongsTo('App\TypeCategory', 'type_category_id');
    }

    public function getTotalAttribute()
    {
        $total = null;
        $models = $this->imodels;

        foreach ($models as $model)
            $total += $model->items()->count();

        return $total;
    }

    public function getItemsAttribute()
    {
        $models = $this->imodels;

        $collection =  new \Illuminate\Database\Eloquent\Collection;

        foreach ($models as $model)
        {
            $collection = $collection->merge($model->items);
        }


        return $collection->all();//$merged->all();
    }
}
