<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomMaintenance extends Model
{
    protected $table = 'room_maintenance';

    protected $fillable = [
        'user_id', 'sublocation_id', 'date_time', 'description'
    ];

    // Database Relationships ------------------------------------------------------------------------------------------
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Sublocation', 'sublocation_id');
    }

    public function cleanedItems()
    {
        return $this->hasMany('App\EquipmentCleaning', 'room_maintenance_id');
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setDateTimeAttribute($value)
    {
        $this->attributes['date_time'] = date('Y-m-d H:i:s', $value);
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getDateAttribute()
    {
        return date('m/d/Y', strtotime($this->date_time));
    }

    public function getTimeAttribute()
    {
        return date('H:i', strtotime($this->date_time));
    }
}
