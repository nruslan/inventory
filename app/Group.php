<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    public $timestamps = false;

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'parent_id', 'name', 'description', 'picture_id', 'icon_id'
    ];

    // RELATIONSHIPS ---------------------------------------------------------------------------------------------------
    public function icon()
    {
        return $this->belongsTo('App\Icon', 'icon_id');
    }

    public function statuses()
    {
        return $this->hasMany('App\Status', 'group_id');
    }

    //ACCESSORS --------------------------------------------------------------------------------------------------------
    public function getHtmlIconAttribute()
    {
        if($this->icon_id)
            return $this->icon->icon;
        else
            return '<i class="fa fa-fw fa-times-circle-o" aria-hidden="true"></i>';
    }
}
