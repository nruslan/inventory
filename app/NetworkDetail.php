<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NetworkDetail extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'group_id', 'name', 'address', 'comments'
    ];

    public function getNameAddressAttribute()
    {
        return "$this->name $this->address";
    }
}
