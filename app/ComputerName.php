<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComputerName extends Model
{
    protected $primaryKey = 'item_id';
    public $incrementing = false;

    protected $fillable = [
        'name', 'item_id', 'operating_system_id'
    ];

    public function os()
    {
        return $this->belongsTo('App\OperatingSystem', 'operating_system_id');
    }

    /**
     * Set the computer's name.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtoupper($value);
    }
}
