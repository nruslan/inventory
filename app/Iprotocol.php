<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Iprotocol extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'item_id', 'address', 'comments'
    ];

    // DB Relations
    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }

    public function network_details()
    {
        return $this->belongsToMany('App\NetworkDetail', 'ip_network', 'iprotocol_id', 'network_details_id');
    }
}
