<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'invoice_number', 'document_id', 'tax_code_id', 'vendor_id', 'amount', 'submitted_at', 'due'
    ];

    public function document()
    {
        return $this->belongsTo('App\Document', 'document_id');
    }

    public function taxcode()
    {
        return $this->belongsTo('App\TaxCode', 'tax_code_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Vendor', 'vendor_id');
    }

    public function setDueAttribute($value)
    {
        $this->attributes['due'] = date('Y-m-d', strtotime($value));
    }

    public function setSubmittedAtAttribute($value)
    {
        $this->attributes['submitted_at'] = date('Y-m-d H:i:s', strtotime($value));
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getTaxCodeDepAttribute()
    {
        return $this->tax_code_id ? $this->taxcode->tax_name_code : '---';
    }

    public function getVendorsNameAttribute()
    {
        return $this->vendor_id ? $this->vendor->name : '---';
    }

    public function getSubmittedDateTimeAttribute()
    {
        return $this->submitted_at ? date("m/d/Y g:iA", strtotime($this->submitted_at)) : '---';
    }

    // Get created at year
    public function getYearAttribute()
    {
        return $this->document_id ? date("Y", strtotime($this->document->created_at)) : '0000';
    }

    // Get the filename from documents table
    public function getFilenameAttribute()
    {
        return $this->document_id ? $this->document->filename : null;
    }

    public function getDueAttribute($value)
    {
        return date("m/d/Y", strtotime($value));
    }

    public function getSubmittedAtAttribute($value)
    {
        return date("m/d/Y g:iA", strtotime($value));
    }

    // Get uploaded-by name
    public function getNameAttribute()
    {
        return $this->document_id ? $this->document->user->name : 'unknown';
    }

    public function getDescriptionAttribute()
    {
        return $this->document_id ? $this->document->description : '---';
    }

}
