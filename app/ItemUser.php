<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemUser extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'begins', 'ends'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    // Accessor --------------------------------------------------------------------------------------------------------
    public function getBeginsAttribute($value)
    {
        if($value)
            return date('m/d/Y', strtotime($value));
        else
            return null;
    }

    public function getEndsAttribute($value)
    {
        if($value)
            return date('m/d/Y', strtotime($value));
        else
            return null;
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setBeginsAttribute($value)
    {
        if($value)
            $this->attributes['begins'] = date('Y-m-d', strtotime($value));
        else
            $this->attributes['begins'] = null;
    }

    public function setEndsAttribute($value)
    {
        if($value)
            $this->attributes['ends'] = date('Y-m-d', strtotime($value));
        else
            $this->attributes['ends'] = null;
    }
}
