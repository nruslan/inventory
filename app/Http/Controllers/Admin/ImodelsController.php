<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Imodel;
use App\Type;
//use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ImodelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$imodels = Imodel::orderBy('name_number')->paginate();

        $imodels = Imodel::join('brands', 'brand_id', '=', 'brands.id')
            ->orderBy('brands.name')
            ->paginate(12);

        $i = $imodels->firstItem();

        session()->put('pageId', $imodels->currentPage());

        return view('admin.models.index', compact('imodels', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::orderBy('name')->pluck('name', 'id');
        $types = Type::orderBy('name')->pluck('name', 'id');
        return view('admin.models.create', compact('types', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\ImodelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ImodelRequest $request)
    {
        $model = Imodel::create($request->all());

        return redirect("admin/models/$model->id")->with('message-success', 'New Model has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Imodel::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('admin.models.show', compact('model', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Imodel::findOrFail($id);
        $brands = Brand::orderBy('name')->pluck('name', 'id');
        $types = Type::orderBy('name')->pluck('name', 'id');
        return view('admin.models.edit', compact('types', 'model', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\ImodelRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ImodelRequest $request, $id)
    {
        $model = Imodel::findOrFail($id);
            $model->update($request->all());

        return redirect("admin/models/$model->id")->with('message-success', "Model <b>$model->name_number</b> has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Imodel::findOrFail($id);
        $model->delete();
        $pageId = session()->get('pageId');
        return redirect("admin/models?page=$pageId")->with('message-delete', "Model <b>$model->name_number</b> has been deleted.");
    }
}
