<?php

namespace App\Http\Controllers\Admin;

use App\WebAccountType;
use App\Icon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebAccountTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = WebAccountType::orderBy('name')->paginate();
        session()->put('pageId', $types->currentPage());
        $i = $types->firstItem();
        return view('admin.web-account-types.index', compact('types', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $icons = Icon::all()->pluck('name_html_icon', 'id');
        return view('admin.web-account-types.create', compact('brands', 'icons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = WebAccountType::create($request->all());

        return redirect("/admin/web-account-types/$type->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = WebAccountType::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('admin.web-account-types.show', compact('type', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = WebAccountType::findOrFail($id);
        $icons = Icon::all()->pluck('name_html_icon', 'id');
        $pageId = session()->get('pageId');
        return view('admin.web-account-types.edit', compact('type', 'pageId', 'icons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = WebAccountType::findOrFail($id);
        $type->update($request->all());
        return redirect("/admin/web-account-types/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = WebAccountType::findOrFail($id);
        $type->delete();
        $pageId = session()->get('pageId');
        return redirect("/admin/web-account-types?page=$pageId");
    }
}
