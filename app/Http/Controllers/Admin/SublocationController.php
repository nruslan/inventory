<?php

namespace App\Http\Controllers\Admin;

use App\Location;
use App\Sublocation;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SublocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sublocations = Sublocation::whereNull('deleted_at')->paginate();
        session()->put('pageId', $sublocations->currentPage());
        $i = $sublocations->firstItem();
        return view('admin.sublocation.index', compact('sublocations', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = Location::orderBy('name')
            ->whereNull('deleted_at')
            ->pluck('name', 'id');

        return view('admin.sublocation.create', compact('locations'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Requests\SublocationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Requests\SublocationRequest $request)
    {
        $sublocation = Sublocation::create($request->all());
        return redirect("admin/sublocations/$sublocation->id")->with('message-success', 'New Sublocation has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sublocation = Sublocation::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('admin.sublocation.show', compact('sublocation', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sublocation = Sublocation::findOrFail($id);
        $locations = Location::orderBy('name')
            ->whereNull('deleted_at')
            ->pluck('name', 'id');

        return view('admin.sublocation.edit', compact('locations', 'sublocation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\SublocationRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Requests\SublocationRequest $request, $id)
    {
        $sublocation = Sublocation::findOrFail($id);
            $sublocation->update($request->all());

        return redirect("admin/sublocations/$id")->with('message-success', 'Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sublocation = Sublocation::findOrFail($id);
        $sublocation->delete();
        $pageId = session()->get('pageId');

        return redirect("admin/sublocations?page=$pageId")->with('message-success', 'Sublocation has been deleted successfully.');
    }
}
