<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Group;
use App\Icon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('name')->paginate();
        $i = $categories->firstItem();
        session()->put('pageId', $categories->currentPage());

        return view('admin.categories.index', compact('categories', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $icons = Icon::all()->pluck('name_html_icon', 'id');
        $groups = Group::all()->pluck('name', 'id');
        $pageId = session()->get('pageId');
        return view('admin.categories.create', compact('pageId', 'icons', 'groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\CategoriesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CategoriesRequest $request)
    {
        $category = Category::create($request->all());
        return redirect("/admin/categories/$category->id")->with('message-success', 'The Category has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);
        $pageId = session()->get('pageId');
        return view("admin.categories.show", compact('category', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $icons = Icon::all()->pluck('name_html_icon', 'id');
        $groups = Group::all()->pluck('name', 'id');
        $pageId = session()->get('pageId');
        return view("admin.categories.edit", compact('category', 'pageId', 'icons', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\CategoriesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CategoriesRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
