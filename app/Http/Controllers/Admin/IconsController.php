<?php

namespace App\Http\Controllers\Admin;

use App\Icon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class IconsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $icons = Icon::orderBy('name')->paginate();
        $i = $icons->firstItem();
        session()->put('pageId', $icons->currentPage());
        return view('admin.icons.index', compact('icons', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageId = session()->get('pageId');
        return view('admin.icons.create', compact('icons', 'pageId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\IconRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\IconRequest $request)
    {
        $icon = Icon::create($request->all());

        return redirect("/admin/icons/$icon->id")->with('message-success', 'The Icon has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $icon = Icon::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('admin.icons.show', compact('icon', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $icon = Icon::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('admin.icons.edit', compact('icon', 'pageId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\IconRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\IconRequest $request, $id)
    {
        $icon = Icon::findOrFail($id);
            $icon->update($request->all());

        return redirect("/admin/icons/$icon->id")->with('message-success', 'The Icon has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $icon = Icon::findOrFail($id);
        $name = $icon->name;
        $icon->delete();

        return redirect("/admin/icons")->with('message-delete', "The $name Icon has been deleted.");
    }
}
