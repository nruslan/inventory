<?php

namespace App\Http\Controllers\Admin;

use App\Type;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::orderBy('name')->paginate();
        session()->put('pageId', $types->currentPage());
        $i = $types->firstItem();
        return view('admin.types.index', compact('types', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.types.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Requests\TypeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Requests\TypeRequest $request)
    {
        $type = Type::create($request->all());

        return redirect("admin/types/$type->id")->with('message-success', 'New Type has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $type = Type::findOrFail($id);

        $pageId = session()->get('pageId');

        return view('admin.types.show', compact('type', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = Type::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('admin.types.edit', compact('type', 'brands', 'pageId'));
    }

    /**
     * Update the specified resource in storage.
     * @param Requests\TypeRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Requests\TypeRequest $request, $id)
    {
        $type = Type::findOrFail($id);
            $type->update($request->all());
        return redirect("admin/types/$id")->with('message-success', 'Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = Type::findOrFail($id);
        $type->delete();
        $pageId = session()->get('pageId');
        return redirect("admin/types?page=$pageId")->with('message-success', 'The Brand has been deleted successfully.');
    }
}
