<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Icon;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::orderBy('name')->paginate();
        $i = $groups->firstItem();
        session()->put('pageId', $groups->currentPage());

        return view('admin.groups.index', compact('groups', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $icons = Icon::all()->pluck('name_html_icon', 'id');
        $pageId = session()->get('pageId');
        return view('admin.groups.create', compact('pageId', 'icons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\GroupRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\GroupRequest $request)
    {
        $group = Group::create($request->all());

        return redirect("/admin/groups/$group->id")->with('message-success', 'The Group has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = Group::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('admin.groups.show', compact('group', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::findOrFail($id);
        $icons = Icon::all()->pluck('name_html_icon', 'id');
        $pageId = session()->get('pageId');

        return view('admin.groups.edit', compact('group', 'icons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\GroupRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\GroupRequest $request, $id)
    {
        $group = Group::findOrFail($id);
        $group->update($request->all());

        return redirect("/admin/groups/$group->id")->with('message-success', 'The Group has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::findOrFail($id);
        $name = $group->name;
        $group->delete();

        return redirect("/admin/groups")->with('message-delete', "The $name Group has been deleted.");
    }
}
