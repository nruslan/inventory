<?php

namespace App\Http\Controllers\Admin\User;

use App\User;
use App\Department;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name')->paginate();
        session()->put('pageId', $users->currentPage());
        $i = $users->firstItem();
        return view('admin.users.index', compact('users', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all()->pluck('name', 'id')->toArray();

        asort($departments);

        return view('admin.users.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
        ]);

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($this->shuffled_string()),
        ]);

        // User's privileges
        //$user->roles()->attach($request->privilege_id);

        return redirect("admin/users/$user->id")->with('message-success', 'The User has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('admin.users.show', compact('user', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $user->privilege_id = $user->user_privileges;

        $departments = Arr::sort(Department::all()->pluck('name', 'id'));

        $user->department_id = $user->departments->pluck('name', 'id');

        $pageId = session()->get('pageId');
        //$privileges = Privilege::all()->pluck('name', 'id');

        //$routes = RouteName::all()->pluck('name', 'id');
        //$actions = RouteAction::all()->pluck('name', 'id');

        return view('admin.users.edit', compact('user', 'pageId', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => "required|string|email|max:255|unique:users,email,$id"
        ]);

        $user = User::findOrFail($id);
        $user->update($request->all());
        $user->departments()->sync($request->department_id);

        return redirect("admin/users/$user->id")->with('message-success', 'Details has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        $pageId = session()->get('pageId');
        return redirect("admin/users?page=$pageId")->with('message-delete', "$user->name has been deleted.");;
    }

    public function shuffled_string()
    {
        $digits = rand(100000000,999999999);
        $lowerCaseLetters = 'qwertasdfgzxcvbyuiophjknm'; // no lower case L
        $upperCaseLetters = 'QWERTASDFGZXCVBYUIOPHJKLNM';
        $specialCharacters = '!@$%';
        // united string
        $string = str_shuffle($digits.$lowerCaseLetters.$digits.$upperCaseLetters.$specialCharacters.$digits);
        return substr($string,0,12);
    }
}
