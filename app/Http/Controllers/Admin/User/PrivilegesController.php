<?php

namespace App\Http\Controllers\Admin\User;

use App\User;
use App\Privilege;
use App\RouteAction;
use App\RouteName;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrivilegesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $assigned_privileges = $user->user_privileges_array;
        $routes = RouteName::all();
        return view('admin.users.privileges.edit', compact('user', 'assigned_privileges', 'routes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $user->routeNames()->detach();

        foreach ($request->action_name_id as $routeNameId => $actions) {
            foreach ($actions as $id => $routeActionId) {
                $user->routeNames()->attach($routeNameId, ['action_id' => $routeActionId]);
            }
        }

        return redirect("admin/users/$user->id")->with('message-success', 'Details has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->routeNames()->detach();

        return redirect("admin/users/$user->id")->with('message-success', 'All privileges have been removed successfully.');
    }
}
