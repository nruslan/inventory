<?php

namespace App\Http\Controllers\Master\Documents;

use App\Doctype;
use App\Document;
use App\TaxCode;
use App\Http\Resources\Document as DocumentResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class IndexController extends Controller
{
    protected $template;

    public function __construct()
    {
        $this->template = "master.documents";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get list of all documents
        $documents = Document::orderBy('id')->paginate();

        $i = $documents->firstItem();

        session()->put('pageId', $documents->currentPage());

        return view("$this->template.index", compact('documents', 'i'));


        //Return collection of Documents
        //return DocumentResource::collection($documents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $doctypes = Doctype::all()->pluck('name', 'id');
        $pageId = session()->get('pageId');
        return view("$this->template.create", compact('pageId', 'doctypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'doctype_id' => 'required',
            'title' => 'required',
            'input_file' => 'required'
        ]);

        // all incoming inputs
        $inputs = $request->all();

        $year = date('Y');
        // upload path
        $destinationPath = "public/documents/$year";

        // getting file extension
        $extension = $request->file('input_file')->getClientOriginalExtension();

        // getting file size
        $size = $request->file('input_file')->getSize();

        // Make filename
        $fileName = Str::slug($inputs['title'].'-'.rand(999, 9999), '-').'.'.$extension;

        // uploading file to given path
        $request->file('input_file')->storeAs($destinationPath, $fileName);

        $inputs['filename'] = $fileName;
        $inputs['user_id'] = request()->user()->id;
        $inputs['size'] = $size;

        Document::create($inputs);

        return redirect("/documents");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = Document::findOrFail($id);

        //return response()->file("storage/documents/2018/$document->filename");

        return view();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = Document::findOrFail($id);
        $doctypes = Doctype::all()->pluck('name', 'id');
        $pageId = session()->get('pageId');
        return view("$this->template.edit", compact('document', 'doctypes', 'pageId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $document = Document::findOrFail($id);
        $document->update($request->all());
        return redirect("documents/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    * Upload Document to the server
    * @param object $file
    * @param string $title
    * @return $array
    */
    public static function upload($file, $title)
    {
        $year = date('Y');
        // upload path
        $destinationPath = "public/documents/$year";
        // getting file extension
        $extension = $file->getClientOriginalExtension();
        // getting file size
        $size = $file->getSize();
        // Make filename
        $fileName = Str::slug($title.'-'.rand(999, 9999), '-').'.'.$extension;
        // uploading file to given path
        $file->storeAs($destinationPath, $fileName);

        return ['size' => $size, 'filename' => $fileName];
    }
}
