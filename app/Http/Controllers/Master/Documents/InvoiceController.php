<?php

namespace App\Http\Controllers\Master\Documents;

use App\Invoice;
use App\TaxCode;
use App\Vendor;
use App\Document;
use Illuminate\Http\Request;

class InvoiceController extends IndexController
{
    protected $viewTemplate;

    public function __construct()
    {
        parent::__construct();
        $this->viewTemplate = "$this->template.invoices";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check if http request is exists
        //if(request()->invoices_for)
        request()->invoices_for ? session()->put('invoices_for', request()->invoices_for) : null; //Set the session with requested year
        //Get the year from a session or difine current year
        $theYear = (session()->get('invoices_for') ? session()->get('invoices_for') : date('Y'));
            $from = date("$theYear-01-01 00:00:00");
            $to = date("$theYear-12-31 23:59:59");

        $beginningOfTheYear = date('01/01/Y');
        $currentDate = date('m/d/Y');

        $invoices = Invoice::whereBetween('submitted_at', [$from, $to])
            ->orderBy('submitted_at', 'desc')
            ->paginate();
        $totalAmount = $invoices->sum('amount');
        // Set the increment counter
        $i = $invoices->firstItem();

        $years = [];

        for($y = 2017; $y <= date('Y'); $y++){
            $years[$y] = $y;
        }

        return view("$this->viewTemplate.index", compact('invoices', 'totalAmount', 'i', 'years', 'theYear'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $taxcodes = TaxCode::all()->pluck('tax_name_code', 'id');
        $vendors = Vendor::all()->pluck('name', 'id');
        return view("$this->viewTemplate.create", compact('taxcodes', 'vendors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tax_code_id' => 'required|numeric',
            'vendor_id' => 'required|numeric',
            'invoice_number' => 'required|numeric',
            'amount' => 'required',
            'due' => 'required|date',
            'submitted_at' => 'required',
            'input_file' => 'required'
        ]);
        //Set the title
        $inputs['title'] = "invoice $request->invoice_number";
        //Upload and save the file
        //return [filename and size]
        $uploadedFile = parent::upload($request->file('input_file'), $inputs['title']);
        //Set vars for documents table
        $inputs['user_id'] = request()->user()->id;
        $inputs['doctype_id'] = 1;
        $inputs['description'] = $request->description;
        $inputs['filename'] = $uploadedFile['filename'];
        $inputs['size'] = $uploadedFile['size'];
        //Insert into documents table
        $newDoc = Document::create($inputs);
        //Set vars for Invoices table
        $input['invoice_number'] = $request->invoice_number;
        $input['document_id'] = $newDoc->id;
        $input['tax_code_id'] = $request->tax_code_id;
        $input['vendor_id'] = $request->vendor_id;
        $input['amount'] = $request->amount;
        $input['submitted_at'] = $request->submitted_at;
        $input['due'] = $request->due;
        //Insert into invoices table
        $newInvoice = Invoice::create($input);
        //Redirect to the new invoice page
        return redirect("documents/invoices/$newInvoice->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::findOrFail($id);
        return view("$this->viewTemplate.show", compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoice = Invoice::findOrFail($id);
        $taxcodes = TaxCode::all()->pluck('tax_name_code', 'id');
        $vendors = Vendor::all()->pluck('name', 'id');
        return view("$this->viewTemplate.edit", compact('invoice', 'taxcodes', 'vendors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tax_code_id' => 'required',
            'vendor_id' => 'required',
            'invoice_number' => 'required',
            'amount' => 'required',
            'due' => 'required|date',
            'submitted_at' => 'required'
        ]);
        $invoice = Invoice::findOrFail($id);
        $invoice->update($request->all());

        return redirect("documents/invoices/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
