<?php

namespace App\Http\Controllers\Master\Dashboard;

use App\Type;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;

class ItemsByTypeController extends IndexController
{
    protected $view_template;

    public function __construct()
    {
        parent::__construct();
        $this->view_template = "$this->template.printers";
    }

    public function index()
    {
        $laserPrinters = Type::find(18)->items;
        $mfpPrinters = Type::find(15)->items;
        $inkPrinters = Type::find(22)->items;

        $printers = array_merge($laserPrinters, $mfpPrinters, $inkPrinters);//dd($laserPrinters->items);
        $i = 1;

        return view("$this->view_template.index", compact('printers', 'i'));
    }

    public function printers()
    {
        $laserPrinters = Type::find(18)->items;
        $mfpPrinters = Type::find(15)->items;
        $inkPrinters = Type::find(22)->items;

        $items = array_merge($laserPrinters, $mfpPrinters, $inkPrinters);//dd($laserPrinters->items);
        $i = 1;

        return view('master.dashboard.items-by-type', compact('items', 'i'));
    }

    public function items($id)
    {
        $items = Arr::sort(Type::find($id)->items);
        $i = 1;
        return view('master.dashboard.items-by-type', compact('items', 'i'));
    }
}
