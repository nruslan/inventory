<?php

namespace App\Http\Controllers\Master\Dashboard;

use App\Doctype;
use App\Item;
use App\ItemLog;
use App\Location;
use App\Sublocation;
use App\Type;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    protected $template;

    public function __construct()
    {
        $this->template = "master.dashboard";
    }

    public function index()
    {
        $i = 1;
        $allInOns = Type::find(1)->total;
        $monitors = Type::find(2)->total;
        $pcTowers = Type::find(4)->total;
        $batteries = Type::find(7)->total;
        $laptops = Type::find(9)->total;
        $receiptPrinters = Type::find(8)->total;
        $tablets = Type::find(20)->total;
        $timeClocks = Type::find(12)->total;
        $webCams = Type::find(6)->total;
        $labelMakers = Type::find(3)->total;
        $pointOfSales = Type::find(21)->total;
        $tvs = Type::find(26)->total;


        $laserPrinters = Type::find(18)->total;
        $mfpPrinters = Type::find(15)->total;
        $inkJetPrinters = Type::find(22)->total;
        $printers = $mfpPrinters + $laserPrinters + $inkJetPrinters;

        $totalItems = Item::all()->count();
        $locations = Location::find([1,2,3,4,5]);
        //Network-Communication Rooms
        $subLocations = Sublocation::networkRooms();

        $itemsLogs = ItemLog::orderBy('updated_at', 'desc')->limit(6)->get();

        $itReports = Doctype::findOrFail(9)->documents()->limit(5)->get();
        $invoices = Doctype::findOrFail(1)->documents()->limit(5)->get();
        $orders = Doctype::findOrFail(2)->documents()->limit(5)->get();
        $ii = 1;
        return view("$this->template.index", compact(
            'batteries',
            'monitors',
            'allInOns',
            'pcTowers',
            'laptops',
            'printers',
            'receiptPrinters',
            'tablets',
            'timeClocks',
            'webCams',
            'labelMakers',
            'pointOfSales',
            'tvs',
            'locations',
            'subLocations',
            'totalItems',
            'itemsLogs',
            'i', 'ii',
            'itReports', 'invoices', 'orders'
        ));
    }
}
