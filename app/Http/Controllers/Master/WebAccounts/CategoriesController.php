<?php

namespace App\Http\Controllers\Master\WebAccounts;

use App\Category;
use App\WebAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = WebAccount::findOrFail($id);
        $categories = Category::all()->pluck('group_category_name', 'id');
        $pageId = session()->get('pageId');
        $account->category_id =  $account->categories->pluck('name', 'id');
        $i = 1;

        return view('master.web-accounts.categories.edit', compact('account', 'categories', 'pageId', 'i'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_id' => 'required'
        ]);
        $account = WebAccount::findOrFail($id);
        $account->categories()->detach();
        $account->categories()->attach($request->category_id);
        return redirect("/accounts/$account->id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = WebAccount::findOrFail($id);
        $account->categories()->detach();
        return redirect("/accounts/$account->id");
    }
}
