<?php

namespace App\Http\Controllers\Master\WebAccounts;

use App\User;
use App\WebAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $account = WebAccount::findOrFail(request()->id);
        $users = User::all()->pluck('name', 'id');
        $pageId = session()->get('pageId');
        return view('master.web-accounts.user.create', compact('account', 'users', 'pageId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required'
        ]);

        $account = WebAccount::findOrFail($request->account_id);

        $account->users()->attach($request->user_id);

        return redirect("/accounts/$account->id");


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = WebAccount::findOrFail($id);
        $users = User::all()->pluck('name', 'id');
        $pageId = session()->get('pageId');
        $account->user_id =  $account->users->pluck('name', 'id');
        $i = 1;

        return view('master.web-accounts.user.edit', compact('account', 'users', 'pageId', 'i'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'required'
        ]);

        $account = WebAccount::findOrFail($id);

        $account->users()->detach();
        $account->users()->attach($request->user_id);

        return redirect("/accounts/$account->id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = WebAccount::findOrFail($id);
        $account->users()->detach();

        return redirect("/accounts/$account->id");
    }
}
