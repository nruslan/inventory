<?php

namespace App\Http\Controllers\Master\WebAccounts;

use App\WebAccount;
use App\WebAccountType;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = WebAccount::orderBy('id')->paginate();

        $i = $accounts->firstItem();

        session()->put('pageId', $accounts->currentPage());
        return view('master.web-accounts.index', compact('accounts', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = WebAccountType::all()->pluck('name', 'id');
        $pageId = session()->get('pageId');
        return view('master.web-accounts.create', compact('types', 'pageId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\WebAccountsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\WebAccountsRequest $request)
    {
        $request['user_id'] = request()->user()->id;
        $request['strength'] = strlen($request->access_word);

        $account = WebAccount::create($request->all());
        return redirect("/accounts/$account->id")->with('message-success', 'Account has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = WebAccount::findOrFail($id);
        $pageId = session()->get('pageId');
        $i = 1;
        return view('master.web-accounts.show', compact('account', 'pageId', 'i'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = WebAccount::findOrFail($id);
        $types = WebAccountType::all()->pluck('name', 'id');
        return view('master.web-accounts.edit', compact('account', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\WebAccountsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\WebAccountsRequest $request, $id)
    {
        $account = WebAccount::findOrFail($id);
        $account->update($request->all());

        return redirect("/accounts/$account->id")->with('message-success', 'Account details have been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = WebAccount::findOrFail($id);
        $account->delete();
        $pageId = session()->get('pageId');
        return redirect("/accounts?page=$pageId")->with('message-delete', 'Account has been deleted.');
    }

    // access via API
    public function password(Request $request)
    {
        $account = WebAccount::findOrFail($request->id);
        return $account->access_word;
    }
}
