<?php

namespace App\Http\Controllers\Master\ProximityCards;

use App\Group;
use App\ProximityCard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pCards = ProximityCard::orderBy('badge_number')->paginate();
        $i = $pCards->firstItem();
        session()->put('pageId', $pCards->currentPage());
        return view('master.proximity-cards.index', compact('pCards', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageId = session()->get('pageId');
        return view('master.proximity-cards.create', compact('pageId', 'i'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'badge_number' => 'required|numeric|unique:proximity_cards',
            'card_number' => 'required'
        ]);
        $request['status_id'] = 2;
        $pCard = ProximityCard::create($request->all());
        return redirect("/proximity-cards/$pCard->id")->with('message-success', 'The card data has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pCard = ProximityCard::find($id);
        $pageId = session()->get('pageId');
        return view('master.proximity-cards.show', compact('pCard', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pCard = ProximityCard::find($id);
        $statuses = Group::findOrFail(4)->statuses->pluck('name', 'id');
        $pageId = session()->get('pageId');
        return view('master.proximity-cards.edit', compact('pCard', 'statuses', 'pageId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'badge_number' => "required|numeric|unique:proximity_cards,badge_number,$id",
            'card_number' => 'required'
        ]);

        $pCard = ProximityCard::findOrFail($id);
        $pCard->update($request->all());
        return redirect("/proximity-cards/$pCard->id")->with('message-success', 'The card data has been added successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pCard = ProximityCard::find($id);
        $pCard->delete();
        $pageId = session()->get('pageId');
        return redirect("/proximity-cards?page=$pageId");
    }
}
