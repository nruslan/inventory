<?php

namespace App\Http\Controllers\Master\Employees;

use App\Employee;
use App\ProximityCard;
use App\ProximityCardHistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index()
    {
        $employees = array_map('str_getcsv', file(public_path('Employee-Information-Export.csv')));

        //dd($employees);
        foreach($employees as $id => $values) {
            if($id == 0) continue;
            //if($id == 1) continue;

            // Set vars
            $employeeNumber = $values[0];
            $firstName = ucfirst(strtolower($values[1]));
            $middleName = ucfirst(strtolower($values[2]));
            $lastName = ucfirst(strtolower($values[3]));
            $badgeNumber = $values[5];
            $startDate = date('Y-m-d',strtotime($values[6]));
            //$endDate = date('Y-m-d',strtotime($values[7]));

            $worker = Employee::create([
                'first_name' => $firstName,
                'middle_name' => $middleName,
                'last_name' => $lastName,
                'employee_number' => $employeeNumber,
                'start_date' => $startDate,
                //'end_date' => $endDate
            ]);

            if($badgeNumber)
                ProximityCard::create(['badge_number' => $badgeNumber, 'employee_id' => $worker->id, 'status_id' => 1]);

            unset($employeeNumber);
            unset($firstName);
            unset($middleName);
            unset($lastName);
            unset($badgeNumber);
            unset($startDate);
            unset($endDate);
            unset($worker);
        }

        //return "DONE!";
    } */

    public function index()
    {
        $employees = Employee::orderBy('last_name')->paginate();
        $i = $employees->firstItem();
        session()->put('pageId', $employees->currentPage());
        return view('master.employees.index', compact('employees', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageId = session()->get('pageId');
        return view('master.employees.create', compact('pageId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'employee_number' => 'required|numeric',
            'start_date' => 'required|date'
        ]);

        $employee = Employee::create($request->all());
        return redirect("employees/$employee->id")->with('message-success', 'New employee has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('master.employees.show', compact('employee', 'pageId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('master.employees.edit', compact('employee', 'pageId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'employee_number' => 'required|numeric',
            'start_date' => 'required|date'
        ]);

        $employee = Employee::findOrFail($id);
            $employee->update($request->all());

            //if($employee->end_date) {
                //ProximityCardHistory::create();
            //}

        return redirect("employees/$id")->with('message-success', "Employee's information has been updated successfully.");;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
