<?php

namespace App\Http\Controllers\Master\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    protected $template;

    public function __construct()
    {
        //$this->middleware('auth');
        $this->template = "master.home";
    }

    public function dashboard()
    {
        return view("$this->template.dashboard");
    }
}
