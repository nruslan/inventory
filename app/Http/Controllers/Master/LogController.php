<?php

namespace App\Http\Controllers\Master;

use App\ItemLog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function index()
    {
        $itemsLogs = ItemLog::orderBy('updated_at', 'desc')->paginate(12);
        $i = $itemsLogs->firstItem();
        
        return view('master.log.index', compact('itemsLogs', 'i'));
    }

    public function create()
    {
        //Current Item
        return view('master.log.create');
    }

    public function store(Request $request, ItemLog $log)
    {
        $this->validate($request, [
            'action' => 'required'
        ]);
        
        $request['user_id'] = request()->user()->id;
        
        $log = $log->create($request->all());

        return redirect()->route('log.show', $log->id);
    }

    public function show(ItemLog $log)
    {
        return view('master.log.show', compact('log'));
    }

    public function edit(ItemLog $log)
    {
        return view('master.log.edit', compact('log'));
    }

    public function update(Request $request, ItemLog $log)
    {
        $log->update($request->all());
        return redirect()->route('log.show', $log->id);
    }

    public function destroy($id)
    {
        //
    }
}
