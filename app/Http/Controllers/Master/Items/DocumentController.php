<?php

namespace App\Http\Controllers\Master\Items;

use App\Doctype;
use App\Document;
use App\Item;
use App\ItemDocument;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Current Item
        $item = Item::find(request('id'));
        $doctypes = Doctype::all()->pluck('name', 'id');
        $pageId = session()->get('pageId');
        return view('master.items.document.create', compact('item', 'doctypes', 'pageId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'doctype_id' => 'required',
            'title' => 'required',
            'input_file' => 'required'
        ]);

        // all incoming inputs
        $inputs = $request->all();

        $year = date('Y');
        // upload path
        $destinationPath = "public/documents/$year";

        // getting file extension
        $extension = $request->file('input_file')->getClientOriginalExtension();

        // getting file size
        $size = $request->file('input_file')->getSize();

        // Make filename
        $fileName = Str::slug($inputs['title'].'-'.rand(999, 9999), '-').'.'.$extension;

        // uploading file to given path
        $request->file('input_file')->storeAs($destinationPath, $fileName);

        $inputs['filename'] = $fileName;
        $inputs['user_id'] = request()->user()->id;
        $inputs['size'] = $size;

        $document = Document::create($inputs);

        $document->items()->sync($inputs['item_id']);

        return redirect("/items/".$inputs['item_id']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document_relation = ItemDocument::findOrFail($id);
        $item_id = $document_relation->item_id;
        $document_relation->delete();

        return redirect("/items/$item_id");
    }
}
