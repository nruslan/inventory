<?php

namespace App\Http\Controllers\Master\Items\Reports;

use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DateRangeController extends Controller
{
    public function purchase_date_range()
    {
        $search_results = [];
        $pageId = session()->get('pageId');

        if(request()->date_from && request()->date_to) {
            $dateFrom = date('Y-m-d', strtotime(request()->date_from));
            $dateTo = date('Y-m-d', strtotime(request()->date_to));

            $search_results = Item::whereBetween('purchase_date', [$dateFrom, $dateTo])
                ->orderBy('purchase_date')
                ->paginate(12);

            $i = $search_results->firstItem();
            $date_from = request()->date_from;
            $date_to = request()->date_to;
        }

        return view('master.items.reports.date-range.purchase-date', compact('pageId', 'search_results', 'i', 'date_from', 'date_to'));
    }


    public function exportReport(Request $request)
    {
        if($request['date_from'] && $request['date_to'])
        {
            $fromDate = $request['date_from'];
            $toDate = $request['date_to'];
            $biReport = $this->activityReport($fromDate,$toDate);
            $total = $this->totalSum($biReport);
            $i = 1;

            header("Content-type: application/vnd.ms-word");
            header("Content-Disposition:attachment; Filename=activity-report-$fromDate-$toDate.doc");
            header("Pragma: no-cache");
            header("Expires: 0");

            return view('master.items.reports.date-range.export', compact('biReport', 'i', 'fromDate', 'toDate', 'total'));
        }

        return redirect('/admin/activities-report');
    }

    public function created_date_range()
    {
        //
    }


    private function getReport($dateFrom, $dateTo)
    {
        //
    }
}
