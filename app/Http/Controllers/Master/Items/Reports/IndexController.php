<?php

namespace App\Http\Controllers\Master\Items\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $pageId = session()->get('pageId');
        return view('master.items.reports.index', compact('pageId'));
    }


}
