<?php

namespace App\Http\Controllers\Master\Items;

use App\Iprotocol;
use App\Item;
use App\NetworkDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IpsController extends Controller
{
    public function index()
    {
        $ipAddresses = Iprotocol::orderBy('address')->paginate();
        $i = $ipAddresses->firstItem();
        session()->put('pageId', $ipAddresses->currentPage());
        return view('master.items.ip.index', compact('ipAddresses', 'i'));
    }

    public function create()
    {
        $pageId = session()->get('pageId');
        $networkDetails = NetworkDetail::all()->pluck('name_address', 'id');

        return view('master.items.ip.create', compact('pageId', 'networkDetails'));
    }

    public function store(Request $request)
    {
        //dd($request->all());

        $this->validate($request, [
            'item_id' => 'required',
            'address' => 'required|ip',
            'network_detail_id' => 'required'
        ],[
            'item_id.required' => 'Specify the inventory item tag. Use the search field.',
            'address.required' => "Specify the item's IP address. The field is required.",
            'address.ip' => "Specify the valid IP address. The field is required.",
            'network_detail_id.required' => "Specify all the network details. The field is required."
        ]);

        $ip = Iprotocol::create($request->all());
            $ip->network_details()->attach($request->network_detail_id);

        return redirect("/ips/$ip->id")->with('message-success', 'IP address has been saved and assigned to the Item successfully.');
    }

    public function show($id)
    {
        $ip = Iprotocol::findOrFail($id);
        $item = $ip->item;
        $pageId = session()->get('pageId');
        return view('master.items.ip.show', compact('ip', 'item', 'pageId'));
    }

    public function edit($id)
    {
        $ip = Iprotocol::findOrFail($id);
        $item = $ip->item;
        $ip->network_detail_id = $ip->network_details->pluck('name', 'id')->toArray();
        $networkDetails = NetworkDetail::all()->pluck('name_address', 'id');

        $pageId = session()->get('pageId');
        return view('master.items.ip.edit', compact('ip', 'item', 'networkDetails', 'pageId'));
    }

    public function update(Request $request, $id)
    {
        $ip = Iprotocol::findOrFail($id);
            $ip->update($request->all());
        $ip->network_details()->detach();
        $ip->network_details()->sync($request->network_detail_id);

        return redirect("ips/$id")->with('message-success', 'IP address details have been updated successfully.');
    }

    public function destroy($id)
    {
        $ip = Iprotocol::findOrFail($id);
        $address = $ip->address;
        $ip->delete();
        $pageId = session()->get('pageId');
        return redirect("/ips?page=$pageId")->with('message-delete', "IP address - $address - has been deleted.");;;
    }

    // access via API
    public function get_item(Request $request)
    {
        $search = $request->id;
        $items = Item::whereRaw("inventory_tag_id like '%$search%'")
            ->orderBy('inventory_tag_id')
            ->get();

        $data = view('master.items.ip.search-result', compact('items'))->render();

        //sleep(3);

        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }
}
