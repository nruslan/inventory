<?php

namespace App\Http\Controllers\Master\Items;

use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function tag_id()
    {
        $search = request()->tag_id;

        $items = Item::where('inventory_tag_id', 'LIKE',  '%'.$search.'%')
            ->orderBy('inventory_tag_id')
            ->paginate();

        $i = $items->firstItem();

        $pageId = session()->get('pageId');
        return view('master.items.search.tagid', compact('items', 'i', 'search', 'pageId'));
    }
}
