<?php

namespace App\Http\Controllers\Master\Items;

use App\ComputerName;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComputerNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'operating_system_id' => 'required',
            'name' => 'required'
        ]);
        ComputerName::create($request->all());
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $name = ComputerName::findOrFail($id);
        $name->delete();
        return redirect()->back();
    }
}
