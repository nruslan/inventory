<?php

namespace App\Http\Controllers\Master\Items;

use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DisposalDateController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::findOrFail($id);
        $pageId = session()->get('pageId');
        return view('master.items.disposal-date.edit', compact('item', 'pageId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'disposal_date' => 'required|date'
        ]);
        $item = Item::find($id);
        $item->update($request->all());

        return redirect("/items/$item->id")->with('message-success', 'The item details have been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        $item->update(['disposal_date' => NULL]);
        return redirect("/items/$item->id")->with('message-success', 'Disposal Date has been removed successfully.');
    }
}
