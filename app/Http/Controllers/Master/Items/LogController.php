<?php

namespace App\Http\Controllers\Master\Items;

use App\Item;
use App\ItemLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogController extends Controller
{
    public function index()
    {
        $itemsLogs = ItemLog::orderBy('updated_at', 'desc')->paginate(12);
        $i = $itemsLogs->firstItem();
        return view('master.items.log.index', compact('itemsLogs', 'i'));
    }

    public function create()
    {
        //Current Item
        $item = Item::find(request('id'));
        $pageId = session()->get('pageId');
        return view('master.items.log.create', compact('item', 'pageId'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'action' => 'required'
        ]);
        $request['user_id'] = request()->user()->id;
        $log = ItemLog::create($request->all());

        return redirect("/items/$log->item_id");
    }

    public function show($id)
    {
        $log = ItemLog::find($id);
        //Current Item
        $item = $log->item;
        $pageId = session()->get('pageId');

        return view('master.items.log.show', compact('log', 'item', 'pageId'));
    }
    
    public function edit($id)
    {
        $log = ItemLog::find($id);
        //Current Item
        $item = $log->item;
        $pageId = session()->get('pageId');
        return view('master.items.log.edit', compact('log', 'item', 'pageId'));
    }

    public function update(Request $request, $id)
    {
        $log = ItemLog::find($id);
        $log->update($request->all());
        return redirect("/items/$log->item_id");
    }

    public function destroy($id)
    {
        //
    }
}
