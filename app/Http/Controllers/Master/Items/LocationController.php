<?php

namespace App\Http\Controllers\Master\Items;

use App\Item;
use App\Location;
use App\Http\Controllers\Controller;
use App\Sublocation;

class LocationController extends Controller
{
    public function index()
    {
        $locations = Location::find([1,2,3,4,5]);
        $totalItems = Item::all()->count();
        $i = 1;
        return view('master.items.location.index', compact('i', 'locations', 'totalItems'));
    }

    public function show($id)
    {
        $i = 1;
        $sublocation = Sublocation::find($id);
        $location = $sublocation->location;
        return view('master.items.location.show', compact('location', 'sublocation', 'i'));
    }
}
