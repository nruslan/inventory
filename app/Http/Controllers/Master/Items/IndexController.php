<?php

namespace App\Http\Controllers\Master\Items;


use App\Department;
use App\Imodel;
use App\Item;
use App\OperatingSystem;
use App\Sublocation;
//use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    protected $viewTemplate;
    public function __construct()
    {
        parent::__construct();
        $this->viewTemplate = "master.items";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::orderBy('inventory_tag_id')->paginate();

        $i = $items->firstItem();
        session()->put('tab', 'tab');
        session()->put('pageId', $items->currentPage());
        return view("$this->viewTemplate.index", compact('items', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Arr::sort(Department::all()->pluck('name', 'id'));
        $sublocations = Arr::sort(Sublocation::all()->pluck('location_name', 'id'));
        $models = Arr::sort(Imodel::orderBy('type_id')->get()->pluck('brand_type_model', 'id'));
        $pageId = session()->get('pageId');

        return view("$this->viewTemplate.create", compact('sublocations', 'models', 'departments', 'pageId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\ItemRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ItemRequest $request)
    {
        $item = Item::create($request->all());

        $item->departments()->attach($request->department_id);

        return redirect("/items/$item->id")->with('message-success', 'The item has been added to the inventory list successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        
        //Load Item's data
        $item = Item::find($id);
        $logs = $item->logs()->orderBy('id', 'desc')->paginate();
        // If item is computer
        $computer = $item->imodel->type->type_category_id == 1 ? true : false;
        $os = OperatingSystem::all()->pluck('name', 'id');
        $ii = $logs->firstItem();

        $i = 1;

        $iii = $i;

        $previousUrl = $this->previousUrl;
        return view('master.items.show', compact('item', 'computer', 'os', 'logs', 'previousUrl', 'i', 'ii', 'iii'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $item->department_id = $item->departments->pluck('name', 'id')->toArray();
        $departments = Arr::sort(Department::all()->pluck('name', 'id'));
        $sublocations = Arr::sort(Sublocation::all()->pluck('location_name', 'id'));
        $models = Imodel::orderBy('type_id')
            ->get()
            ->pluck('brand_type_model', 'id');

        return view('master.items.edit', compact('item', 'sublocations', 'models', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Requests\ItemRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ItemRequest  $request, $id)
    {
        $item = Item::find($id);
        $item->update($request->all());
        $item->departments()->detach();
        $item->departments()->attach($request->department_id);

        return redirect("/items/$item->id")->with('message-success', 'The item details have been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        $item->delete();
        $pageId = session()->get('pageId');
        return redirect("/items?page=$pageId")->with('message-delete', 'The item has been deleted.');
    }
}
