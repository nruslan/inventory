<?php

namespace App\Http\Controllers\Master\Items;

use App\Document;
use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocumentListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::findOrFail($id);
        //Assigned documents to the Item
        $assigned_documents = $item->documents->pluck('title_type', 'id')->toArray();
        // List of all documents
        $documents = Document::all()->pluck('title_type', 'id')->toArray();
        // Remove assigned documents from the list
        $filtered_documents = array_diff_key($documents, $assigned_documents);

        $pageId = session()->get('pageId');
        return view('master.items.document.list.edit', compact('item', 'filtered_documents', 'pageId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'document_id' => 'required'
        ], [
            'document_id.required' => "You have to select AT LEAST ONE DOCUMENT from the list above."
        ]);
        $item = Item::findOrFail($id);
        $item->documents()->attach($request->document_id);

        return redirect("/items/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
