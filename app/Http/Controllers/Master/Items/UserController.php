<?php

namespace App\Http\Controllers\Master\Items;

use App\Item;
use App\ItemUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Current Item
        $item = Item::find(request('id'));
        //List of all users
        $users = User::all()->pluck('name', 'id')->toArray();
        //List of assigned users
        $assigned_users =  $item->users->pluck('name', 'id')->toArray();
        //Remove assigned users from the users list
        $filtered_users = array_diff_key($users, $assigned_users);
        $pageId = session()->get('pageId');
        $i = 1;
        return view('master.items.user.create', compact('filtered_users', 'item', 'i', 'pageId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'begins' => 'required|date'
        ],[
            'user_id.required' => 'Select the user. The field is required.',
            'begins.required' => 'Specify the Start Date. The field is required.',
            'begins.date' => '' //TODO write proper sentence.
        ]);

        $item = Item::find($request->item_id);
        $item->users()->attach($request->user_id, ['begins' => date('Y-m-d', strtotime($request->begins))]);
        return redirect("items/$item->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item_user = ItemUser::findOrFail($id);
        $item = $item_user->item;
        $user = $item_user->user;
        $pageId = session()->get('pageId');
        $i = 1;
        return view('master.items.user.edit', compact('item', 'item_user', 'pageId', 'user', 'i'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'begins' => 'required|date'
        ],[
            'begins.required' => 'Specify the Start Date. The field is required.',
            'begins.date' => '' //TODO write proper sentence.
        ]);

        $item_user = ItemUser::findOrFail($id);
        $item_user->update($request->all());
        return redirect("/items/$item_user->item_id")->with('message-success', "The user's dates have been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item_user = ItemUser::findOrFail($id);
        $name = $item_user->user->name;
        $item_user->delete();
        return redirect("/items/$item_user->item_id")->with('message-success', "$name has been removed from the list successfully.");
    }
}
