<?php

namespace App\Http\Controllers\Master\Items;

use App\Item;
use App\ItemAccount;
use App\ItemAccountType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = Item::findOrFail(request()->id);
        $pageId = session()->get('pageId');
        $accountTypes = ItemAccountType::all()->pluck('name', 'id');
        return view('master.items.account.create', compact('item', 'pageId', 'accountTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'item_account_type_id' => 'required',
            'username' => 'required'
        ]);

        $request['strength'] = strlen($request->password);
        ItemAccount::create($request->all());

        return redirect("items/$request->item_id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $iAccount = ItemAccount::findOrFail($id);
        $item = $iAccount->item;
        $pageId = session()->get('pageId');
        $accountTypes = ItemAccountType::all()->pluck('name', 'id');
        return view('master.items.account.edit', compact('item', 'iAccount', 'pageId', 'accountTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'item_account_type_id' => 'required',
            'username' => 'required'
        ]);
        $request['strength'] = strlen($request->password);
        $iAcct = ItemAccount::findOrFail($id);
            $iAcct->update($request->all());
        return redirect("items/$iAcct->item_id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $iAccount = ItemAccount::findOrFail($id);
        $itemId = $iAccount->item_id;
        $iAccount->delete();
        return redirect("items/$itemId");
    }

    // access via API
    public function get_password(Request $request)
    {
        $iAccount = ItemAccount::findOrFail($request->id);
        return $iAccount->password;
    }
}
