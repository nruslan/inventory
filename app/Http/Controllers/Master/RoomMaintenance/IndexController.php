<?php

namespace App\Http\Controllers\Master\RoomMaintenance;

use App\EquipmentCleaning;
use App\RoomMaintenance;
use App\Sublocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    private $template_path;

    public function __construct()
    {
        $this->template_path = "master.room-maintenance";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Network-Communication Rooms
        //$rooms = Sublocation::networkRooms();

        $entries = RoomMaintenance::with('user')
            ->with('location')
            ->paginate(12);

        $i = $entries->firstItem();

        return view("$this->template_path.index", compact('entries', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms = Sublocation::networkRooms();
        return view("$this->template_path.create", compact('rooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'sublocation_id' => 'required'
        ], [
            'sublocation_id.required' => 'Specify the room location and name. The field is required.'
        ]);

        $request['user_id'] = request()->user()->id;
        $request['date_time'] = strtotime("now");

        $entry = RoomMaintenance::create($request->all());

        if(count($request->item_id)) {
            foreach ($request->item_id as $key => $value)
                $entry->cleanedItems()->create(['item_id' => $value]);
        }

        return redirect("room/maintenance");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entry = RoomMaintenance::findOrFail($id);
        return view("$this->template_path.show", compact('entry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template_path = $this->template_path;
        $entry = RoomMaintenance::findOrFail($id);
        $entry['item_id'] = $entry->cleanedItems->pluck('item_id')->toArray();
        $items = Sublocation::find($entry->sublocation_id)->items;

        return view("$this->template_path.edit", compact('items', 'entry', 'template_path'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        EquipmentCleaning::where('room_maintenance_id', $id)->delete();

        $entry = RoomMaintenance::findOrFail($id);
        $entry->update(['description' => $request->description]);

        if(count($request->item_id)) {
            foreach ($request->item_id as $key => $value)
                $entry->cleanedItems()->create(['item_id' => $value]);
        }

        return redirect("room/maintenance/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entry = RoomMaintenance::findOrFail($id);
        EquipmentCleaning::where('room_maintenance_id', $entry->id)->delete();
        $entry->delete();
        return redirect("room/maintenance");
    }


    // access via API --------------------------------------------------------------------------------------------------
    public function getItems(Request $request)
    {
        $items = Sublocation::find($request->id)->items;
        $i = 1;
        $data = view("$this->template_path.items.list", compact('items', 'i'))->render();
        //sleep(3);
        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }
}
