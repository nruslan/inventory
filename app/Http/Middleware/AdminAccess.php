<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = request()->user()->id;
        $allow = [1];

        if(in_array($userId, $allow))
            return $next($request);
        else
            return redirect()->back()->with('modal-warning', 'You don\'t have permission to access Admin Panel.');
    }
}
