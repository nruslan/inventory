<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class PrivilegesCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //request()->user()->id;

        //request()->user()->routeNames; $r_a = Route::currentRouteName();

        //dd(request()->user()->user_privileges);

        //$route_action = explode ('@', Route::currentRouteAction());

            //$action = $route_action[1];

        $routeName = Route::currentRouteName();

        //dd($routeName);

        //dd(request()->user()->user_privileges);

        if(in_array($routeName, request()->user()->user_privileges))
            return $next($request);
        else
            return redirect()->back()->with('modal-warning', 'Your access is limited.');
    }
}
