<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImodelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_id' => 'required',
            'brand_id' => 'required',
            'name_number' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'brand_id.required' => 'Please specify the Brand. Field is required.',
            'type_id.required' => 'Please specify the Type. Field is required.',
            'name_number.required'  => 'Please specify the model name or model number. Field is required.',
        ];
    }
}
