<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WebAccountsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account_type_id' => 'required',
            'username' => 'required',
            'access_word' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'account_type_id.required' => 'Specify the Account Type. Field is required.',
            'access_word.required' => 'Specify the Password. Field is required.',
            'username.required' => 'Specify the Username or Email. Field is required.',
        ];
    }
}
