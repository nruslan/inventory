<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IconRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'icon' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Specify the Name of the Icon. Field is required.',
            'icon.required' => 'Use FA html markup Icon. Field is required.'
        ];
    }
}
