<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department_id' => 'required',
            'sublocation_id' => 'required',
            'imodel_id' => 'required',
            'inventory_tag_id' => 'required|numeric|unique:items,inventory_tag_id,'.request()->segment(2),
            'serial_number' => 'required',
            'purchase_date' => 'date_format:m/d/Y',
            'price' => ''
        ];
    }

    public function messages()
    {
        return [
            'sublocation_id.required' => 'Please specify the item location. Field is required.',
            'imodel_id.required'  => 'Please specify the item model, type and brand. Field is required.',
            'inventory_tag_id.unique' => 'The tag id already exists in the database',
            'inventory_tag_id.numeric' => 'The tag id must be a number'
        ];
    }
}
