<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SublocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location_id' => 'required',
            'room_code' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'location_id.required' => 'Please specify the MAIN LOCATION. Field is required.',
        ];
    }
}
