<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'brand_id' => 'required',
            'name' => 'required',
            //'model_number' => 'required'
        ];
    }

    public function messages()
    {
        return [
            //'brand_id.required' => 'Please specify the BRAND name. Field is required.',
            //'model_number.required'  => 'Please specify the MODEL. Field is required.',
        ];
    }
}
