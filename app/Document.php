<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'user_id', 'doctype_id', 'title', 'description', 'filename', 'size'
    ];

    public function type()
    {
        return $this->belongsTo('App\Doctype', 'doctype_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function items()
    {
        return $this->belongsToMany('App\Item', 'item_documents', 'document_id', 'item_id')->withPivot('id');
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getFileSizeAttribute()
    {
        $kb = $this->size / 1024;
        return round($kb, 2).' KB';
    }

    public function getDateAttribute()
    {
        return date('m/d/Y', strtotime($this->created_at));
    }

    public function getYearAttribute()
    {
        return date('Y', strtotime($this->created_at));
    }

    public function getTitleTypeAttribute()
    {
        return $this->type->name.' - '.$this->title;
    }
}
