<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebAccount extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id', 'account_type_id', 'username', 'access_word', 'strength', 'url', 'comments'
    ];

    public function type()
    {
        return $this->belongsTo('App\WebAccountType','account_type_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'web_account_users', 'web_account_id', 'user_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'web_account_categories', 'web_account_id', 'category_id');
    }

    public function getNameTypeAttribute()
    {
        return $this->type->name;
    }

    public function getPassWordAttribute()
    {
        return str_repeat("*", $this->strength);
    }

    public function getAccessWordAttribute($value)
    {
        return decrypt($value);
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setAccessWordAttribute($value)
    {
        $this->attributes['access_word'] = encrypt($value);
    }
}
