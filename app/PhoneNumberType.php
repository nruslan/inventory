<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneNumberType extends Model
{
    protected $fillable = [
        'title'
    ];
}
