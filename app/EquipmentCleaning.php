<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentCleaning extends Model
{
    protected $table = 'equipment_cleaning';
    public $timestamps = false;
    protected $fillable = [
        'room_maintenance_id', 'item_id'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getTypeNameBrandAttribute()
    {
        return $this->item->imodel->type->name.'/'.$this->item->imodel->name_number.'/'.$this->item->brand;
    }
}
