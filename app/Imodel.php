<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imodel extends Model
{
    public $timestamps = false;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'brand_id', 'type_id', 'name_number', 'description'
    ];

    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Type', 'type_id');
    }

    public function items()
    {
        return $this->hasMany('App\Item', 'imodel_id');
    }


    public function getBrandTypeModelAttribute()
    {
        return $this->brand->name.' • '.$this->name_number.' • '.$this->type->name;
    }

}
