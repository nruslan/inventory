<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function sublocations()
    {
        return $this->hasMany('App\Sublocation', 'location_id');
    }

    //Accessor
    public function getTotalItemsAttribute()
    {
        $sublocations = $this->sublocations;

        $itemsTotal = null;

        foreach ($sublocations as $sublocation) {
            $itemsTotal += $sublocation->items->count();
        }

        return $itemsTotal;

    }

    public function getSublocationListAttribute()
    {
        $result = null;
        foreach ($this->sublocations as $sublocation)
            $result .= $sublocation->room_code.', ';

        return substr($result, 0, -2);
    }
}
