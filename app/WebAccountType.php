<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebAccountType extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'description', 'icon_id'
    ];

    // RELATIONSHIPS ---------------------------------------------------------------------------------------------------
    public function icon()
    {
        return $this->belongsTo('App\Icon', 'icon_id');
    }

    //ACCESSORS --------------------------------------------------------------------------------------------------------
    public function getHtmlIconAttribute()
    {
        if($this->icon_id)
            return $this->icon->icon;
        else
            return '<i class="fa fa-fw fa-times-circle-o" aria-hidden="true"></i>';
    }
}
