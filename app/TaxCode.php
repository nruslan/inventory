<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxCode extends Model
{
    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id');
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getTaxNameCodeAttribute()
    {
        $dep_code = $this->department->dep_code;
        return "$this->name [$dep_code-$this->number]";
    }
}
