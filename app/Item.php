<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'inventory_tag_id', 'serial_number', 'sublocation_id', 'imodel_id', 'purchase_date', 'disposal_date', 'price', 'comments'
    ];

    // DB Relationship -------------------------------------------------------------------------------------------------
    /**
     * Get the name associated with the item.
     */
    public function name()
    {
        return $this->hasOne('App\ComputerName');
    }

    public function sublocation()
    {
        return $this->belongsTo('App\Sublocation', 'sublocation_id');
    }

    public function imodel()
    {
        return $this->belongsTo('App\Imodel', 'imodel_id');
    }

    public function departments()
    {
        return $this->belongsToMany('App\Department', 'department_items', 'item_id', 'department_id');
    }

    public function documents()
    {
        return $this->belongsToMany('App\Document', 'item_documents', 'item_id', 'document_id')->withPivot('id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'item_users', 'item_id', 'user_id')->withPivot('id', 'begins', 'ends');;
    }

    public function logs()
    {
        return $this->hasMany('App\ItemLog', 'item_id');
    }

    public function ip()
    {
        return $this->hasOne('App\Iprotocol', 'item_id', 'id');
    }

    public function itemAccounts()
    {
        return $this->hasMany('App\ItemAccount');
    }

    // Mutators --------------------------------------------------------------------------------------------------------
    public function setPurchaseDateAttribute($value)
    {
        $this->attributes['purchase_date'] = date('Y-m-d', strtotime($value));
    }

    public function setSerialNumberAttribute($value)
    {
        $this->attributes['serial_number'] = strtoupper($value);
    }

    public function setDisposalDateAttribute($value)
    {
        if($value)
            $this->attributes['disposal_date'] = date('Y-m-d', strtotime($value));
        else
            $this->attributes['disposal_date'] = NULL;
    }


    // Accessor --------------------------------------------------------------------------------------------------------
    public function getPurchaseDateAttribute($value)
    {
        return date('m/d/Y', strtotime($value));
    }

    public function getDisposalDateAttribute($value)
    {
        if($value)
            return date('m/d/Y', strtotime($value));
        else
            return null;
    }

    public function getFullLocationAttribute()
    {
        $location = $this->sublocation->location->name;
        if($this->sublocation->sublocation_name_id)
            $sublocation = $this->sublocation->name->name.' ['.$this->sublocation->room_code.']';
        else
            $sublocation = $this->sublocation->room_code;

        return $location . ' <i class="fa fa-angle-double-right" aria-hidden="true"></i> ' . $sublocation;
    }

    public function getBrandAttribute()
    {
        return $this->imodel->brand->name;
    }

    public function getRelatedDepartmentsAttribute()
    {
        $list = null;
        foreach($this->departments as $department)
        {
            $list .= $department->name . ', ';
        }

        return substr($list, 0, -2);
    }

    public function getTypeCategoryAttribute()
    {
        return $this->imodel->type->type_category_id ? $this->imodel->type->typetegory->name : null;
    }

    public function getComputerTypeAttribute()
    {
        return $this->imodel->type->type_category_id === 1 ? true : false;
    }
}
