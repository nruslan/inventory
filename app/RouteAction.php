<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteAction extends Model
{
    public $timestamps = false;
}
