<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'route_name_id', 'route_action_id'
    ];

    /*public function users()
    {
        return $this->belongsToMany('App\User', 'user_privileges', 'user_id', 'privilege_id');
    }*/
}
