<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteName extends Model
{
    public $timestamps = false;

    public function actions()
    {
        return $this->belongsToMany('App\Action', 'route_actions', 'route_name_id', 'action_id');
    }
}
