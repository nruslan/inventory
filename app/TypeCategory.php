<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeCategory extends Model
{
    /**
     * Get the types for the category.
     */
    public function types()
    {
        return $this->hasMany('App\Type', 'type_category_id');
    }
}
