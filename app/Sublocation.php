<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sublocation extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'location_id', 'room_code', 'sublocation_name_id'
    ];

    // Database Relationships ------------------------------------------------------------------------------------------
    public function location()
    {
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function items()
    {
        return $this->hasMany('App\Item', 'sublocation_id');
    }

    public function name()
    {
        return $this->hasOne('App\SublocationName', 'id', 'sublocation_name_id');

    }

    public function roomMaintenance()
    {
        return $this->hasMany('App\RoomMaintenance', 'sublocation_id');
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getLocationNameAttribute()
    {
        if($this->sublocation_name_id)
            $name = $this->name->name." [$this->room_code]";
        else
            $name = $this->room_code;

        return $this->location->name.' → '.$name;
    }

    public function getTotalItemsPerRoomAttribute()
    {
        return $this->items->count();
    }

    public function getRoomNameAttribute()
    {
        if($this->sublocation_name_id)
            $name = $this->name->name;
        else
            $name = 'N/A';

        return $name;
    }

    public function getRoomCodeSlugAttribute()
    {
        return strtolower($this->room_code);
    }

    /**
     * Return All Network Rooms
     * @return mixed
     */
    public static function networkRooms()
    {
        return Sublocation::find([10,31,40,50,67,64,68]);
    }
}
