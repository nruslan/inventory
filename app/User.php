<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The roles that belong to the user.

    public function roles()
    {
        return $this->belongsToMany('App\Privilege', 'user_privileges', 'user_id', 'privilege_id');
    }*/


    public function departments()
    {
        return $this->belongsToMany(Department::class, 'department_users', 'user_id', 'department_id');
    }

    public function routeNames()
    {
        return $this->belongsToMany('App\RouteName', 'privileges', 'user_id', 'route_name_id')->withPivot('action_id');
    }

    public function items()
    {
        return $this->belongsToMany('App\Item', 'item_users', 'user_id', 'item_id');
    }

    public function picture()
    {
        return $this->belongsTo('App\Picture', 'picture_id');
    }

    //Mutators
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    // Accessor
    public function getUserPrivilegesAttribute()
    {
        $routes = $this->routeNames()->orderBy('id')->get();

        $result = [];
        foreach ($routes as $route) {
            $action = Action::find($route->pivot->action_id);
            $result[] = $route->name.'.'.$action->name;
        }
        return $result;
    }

    public function getUserPrivilegesArrayAttribute()
    {
        $routes = $this->routeNames()->orderBy('id')->get();
        $result = [];
        $cId = null;
        foreach ($routes as $route) {
            $action = Action::find($route->pivot->action_id);
            if($cId == $route->id)
                array_push($result[$route->id], $action->id);
            else
                $result[$route->id] = [$action->id];
            $cId = $route->id;
        }
        return $result;
    }

    public function getUserPrivilegesListAttribute()
    {
        $privileges = $this->getUserPrivilegesArrayAttribute();

        if(count($privileges) > 0) {
            $result = null;
            foreach ($privileges as $routeId => $routeActions) {
                $route = RouteName::find($routeId);
                $a_name = null;
                foreach ($routeActions as $id => $aid) {
                    $action = Action::find($aid);
                    $a_name .= $action->name.', ';
                }
                $a_name = substr($a_name, 0, -2);
                $result .= "<li><strong>$route->name</strong>: $a_name</li>";
            }
            return "<ul>$result</ul>";
        } else {
            return 'none';
        }
    }

    public function getProfilePictureAttribute()
    {
        return ($this->picture_id) ? asset('storage/users/'.$this->picture->file_name) : asset('storage/users/default.jpg');
    }

    public function getShortenedNameAttribute()
    {
        $name = explode(' ' ,$this->name);
        $first_name = $name[0];
        $last_name = array_reverse($name);
        return $first_name.' '.ucfirst($last_name[0][0]);
    }

    public function getAllDepartmentsAttribute()
    {
        if($this->departments->count() > 0) {
            $result = null;
            foreach ($this->departments as $department) {
                $result .= $department->name.', ';
            }

            return substr($result, 0, -2);
        } else {
            return null;
        }
    }
}
