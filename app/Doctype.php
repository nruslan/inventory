<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctype extends Model
{
    public function documents()
    {
        return $this->hasMany('App\Document', 'doctype_id')->orderBy('id', 'desc');
    }
}
