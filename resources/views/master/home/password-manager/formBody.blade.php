<div class="row">
    <div class="col-lg-6 col-lg-offset-3">

        <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
            {!! Form::label('url', 'URL') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-link fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'url', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('url'))
                <span class="help-block"><strong>{{ $errors->first('url') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            {!! Form::label('username', 'Username / Email') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-fw fa-lock" aria-hidden="true"></i></span>
                {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'username or email', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('username'))
                <span class="help-block"><strong>{{ $errors->first('username') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('access_word') ? ' has-error' : '' }}">
            {!! Form::label('access_word', 'Password') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-fw fa-key" aria-hidden="true"></i></span>
                {!! Form::text('access_word', null, ['class' => 'form-control', 'placeholder' => 'password', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('access_word'))
                <span class="help-block"><strong>{{ $errors->first('access_word') }}</strong></span>
            @endif
        </div>
        <!-- Textarea with ckeditor -->
        @include('parts.textarea', ['name' => 'comments', 'title' => 'Comments'])
    </div>
</div>