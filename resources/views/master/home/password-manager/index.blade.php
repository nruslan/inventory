@extends('layouts.master')

@section('title', 'Password Manager')

@section('hTitle')
    Password Manager
    <small>Control panel</small>
@endsection

@section('breadcrumbs'){{ Breadcrumbs::render('password_manager') }}@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Passwords</h3>
        <a class="btn btn-primary btn-xs pull-right aBtn" data-toggle="tooltip" data-placement="left" title="Add new" href="{{ url('home/password-manager/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body"></div>
    <!-- /.box-body -->
    <div class="box-footer"></div>
</div>
@endsection