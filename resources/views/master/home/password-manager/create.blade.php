@extends('layouts.master')

@section('title', 'Add New Password')

@section('hTitle')
    Password Manager
    <small>Control panel</small>
@endsection

@section('breadcrumbs'){{ Breadcrumbs::render('password_manager') }}@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add New Account/Password</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::open(['url' => '/accounts', 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('master.home.password-manager.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "/accounts?page="])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection