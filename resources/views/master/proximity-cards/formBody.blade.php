<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="form-group{{ $errors->has('badge_number') ? ' has-error' : '' }}">
            {!! Form::label('badge_number', 'Badge Number') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-hashtag fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('badge_number', null, ['class' => 'form-control', 'placeholder' => 'enter badge number', 'autocomplete' => 'off', 'autofocus']) !!}
            </div>
            @if ($errors->has('badge_number'))
                <span class="help-block"><strong>{{ $errors->first('badge_number') }}</strong></span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('card_number') ? ' has-error' : '' }}">
            {!! Form::label('card_number', 'Card Number') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-hashtag fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('card_number', null, ['class' => 'form-control', 'placeholder' => 'enter card number', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('card_number'))
                <span class="help-block"><strong>{{ $errors->first('card_number') }}</strong></span>
            @endif
        </div>
        <!-- Textarea with ckeditor -->
        @include('parts.textarea', ['name' => 'comments', 'title' => 'Comments'])
    </div>
</div>