@extends('layouts.master')

@section('title', "Proximity Card $pCard->card_number")

@section('hTitle')
{{ $pCard->card_number or 'N/A' }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/proximity-cards?page=$pageId") }}">Proximity Cards</a></li>
    <li class="active">{{ $pCard->card_number }}</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
            Card Number: {{ $pCard->card_number }}
            <a href="{{ url("/proximity-cards/$pCard->id/edit") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Edit card's data"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit</a></li>
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <ul>
            <li>Badge Number: <strong>{{ $pCard->badge_number }}</strong></li>
            <li>Card Status: {{ $pCard->status->name }}</li>
            <li>Card holder: {!! $pCard->full_employee_name !!}</li>
        </ul>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <ul class="list-unstyled">
            <li></li>
            <li class="pull-right">@include('parts.trashBtn', ['action' => ['Master\ProximityCards\IndexController@destroy', $pCard->id], 'btnTitle' => 'Delete the card', 'position' => 'left', 'class' => 'btn-sm'])</li>
        </ul>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection