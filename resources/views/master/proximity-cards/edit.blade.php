@extends('layouts.master')

@section('title', 'Edit the Card')

@section('hTitle')
    @include('parts.backBtn', ['url' => "/proximity-cards?page=$pageId", 'title' => 'back to the list'])
    Edit the Card
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/proximity-cards?page=$pageId") }}">Proximity Cards</a></li>
    <li class="active">Edit Proximity Card {{ $pCard->card_number }}</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($pCard, ['method' => 'PATCH', 'action' => ['Master\ProximityCards\IndexController@update', $pCard->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
                        {!! Form::label('status_id', 'Item Location') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                        {!! Form::select('status_id', $statuses, null, ['class' => 'form-control', 'autofocus']) !!}
                        @if ($errors->has('status_id'))
                            <span class="help-block"><strong>{{ $errors->first('status_id') }}</strong></span>
                        @endif
                    </div>
                </div>
            </div>
            @include('master.proximity-cards.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "proximity-cards?page=$pageId"])
        </div>
        {!! Form::close() !!}
        <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection