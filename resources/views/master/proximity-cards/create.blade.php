@extends('layouts.master')

@section('title', 'Add New Card')

@section('hTitle')
    @include('parts.backBtn', ['url' => "/proximity-cards?page=$pageId", 'title' => 'back to the list'])
    Add New Card
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/proximity-cards?page=$pageId") }}">Proximity Cards</a></li>
    <li class="active">Add New Proximity Card</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
    </div>
    <!-- /.box-header -->
    {!! Form::open(['url' => 'proximity-cards', 'id' => 'myForm']) !!}
    <div class="box-body">
        @include('master.proximity-cards.formBody')
    </div>
    <div class="box-footer">
        @include('parts.saveCancelBtns', ['cancel' => "proximity-cards?page=$pageId"])
    </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
</div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection