@extends('layouts.master')

@section('title', 'Proximity Cards')

@section('hTitle')
    Proximity Cards
@endsection

@section('breadcrumbs')
    <li class="active">Proximity Cards</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
            <i class="fa fa-id-card-o fa-fw"></i> Total <span class="badge">{{ $pCards->total() }}</span>
            <a class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add new card" href="{{ url('proximity-cards/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a>
        </h3>
        <div class="box-tools">
            {{ $pCards->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        @include('parts.session-message')
        <div class="table-responsive">
            <table class="table table-condensed table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Card Number</th>
                    <th>Badge Number</th>
                    <th>Status</th>
                    <th>Employee Name</th>
                    <th colspan="2"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($pCards as $card)
                    <tr>
                        <th scope="row">{{ $i++ }}</th>
                        <td>{{ $card->card_number or '- NA -' }}</td>
                        <td>{{ $card->badge_number }}</td>
                        <td>{{ $card->status->name }}</td>
                        <td>{!! $card->full_employee_name !!}</td>
                        <td><a class="btn btn-default btn-xs aBtn" href="{{ url("/proximity-cards/$card->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                        <td><a class="btn btn-primary btn-xs aBtn" href="{{ url("/proximity-cards/$card->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        {{ $pCards->links('vendor.pagination.admin-lte') }}
    </div>
</div>
@endsection