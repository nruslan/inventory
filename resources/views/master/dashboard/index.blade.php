@extends('layouts.master')

@section('title', 'Home')

@section('hTitle')
    SB2 Inventory <small>Control Panel</small>
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('dashboard') }}
@endsection

@section('content')
    @if (session('status'))<div class="alert alert-success">{{ session('status') }}</div>@endif
    {{-- <dashboard></dashboard> --}}
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("dashboard/items/20") }}" title="view details">
                    <span class="info-box-icon bg-olive"><i class="fa fa-tablet"></i></span>
                </a>

                <div class="info-box-content">
                    <span class="info-box-text">Tablets</span>
                    <span class="info-box-number">{{ $tablets }}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col  Tablets -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("dashboard/items/9") }}" title="view details">
                    <span class="info-box-icon bg-gray"><i class="fa fa-laptop"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">Laptops</span>
                    <span class="info-box-number">{{ $laptops }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div><!-- /.col Laptops -->
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("dashboard/items/1") }}" title="view details">
                    <span class="info-box-icon bg-red"><i class="fa fa-desktop"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">AIO PCs</span>
                    <span class="info-box-number">{{ $allInOns }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div><!-- /.col AIO -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("dashboard/items/4") }}" title="view details">
                    <span class="info-box-icon bg-green"><i class="fa fa-power-off"></i></span>
                </a>

                <div class="info-box-content">
                    <span class="info-box-text">PC Towers</span>
                    <span class="info-box-number">{{ $pcTowers }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div><!-- /.col PC TOWERS -->
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("dashboard/items/2") }}" title="view details">
                    <span class="info-box-icon bg-aqua">
                        <i class="fa fa-tv"></i>
                    </span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">Monitors</span>
                    <span class="info-box-number">{{ $monitors }}</span>
                </div>
                <!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col Monitors -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("dashboard/items/7") }}" title="view details">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-battery-2"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">Battery BackUps</span>
                    <span class="info-box-number">{{ $batteries }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div><!-- /.col Battery Backups -->
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("dashboard/items/8") }}" title="view details">
                    <span class="info-box-icon bg-blue"><i class="fa fa-check"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">Receipt Printers</span>
                    <span class="info-box-number">{{ $receiptPrinters }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div><!-- /.col Receipt Printers -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("dashboard/items/21") }}" title="view details">
                <span class="info-box-icon bg-black"><i class="fa fa-power-off"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">POS Touch Screens</span>
                    <span class="info-box-number">{{ $pointOfSales }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div><!-- /.col POS Touch Screen -->
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("/dashboard/printers/") }}" title="view details">
                    <span class="info-box-icon bg-purple"><i class="fa fa-print"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">Printers</span>
                    <span class="info-box-number">{{ $printers }}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col Printers -->

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("dashboard/items/3") }}" title="view details">
                    <span class="info-box-icon bg-purple-gradient"><i class="fa fa-print"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">Label Maker/Printer</span>
                    <span class="info-box-number">{{ $labelMakers }}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col Label Printers -->

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("dashboard/items/26") }}" title="view details">
                    <span class="info-box-icon bg-blue-gradient"><i class="fa fa-tv"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">TVs</span>
                    <span class="info-box-number">{{ $tvs }}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col TimeClocks -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <a href="{{ url("dashboard/items/6") }}" title="view details">
                    <span class="info-box-icon bg-gray-light"><i class="fa fa-video-camera"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">Web Cams</span>
                    <span class="info-box-number">{{ $webCams }}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div><!-- /.col WebCams -->
    </div>
    <!-- Second Row -->
    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Total Items per Location</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @foreach($locations as $location)
                        <li class="item">
                            <div class="product-img">
                                <img src="{{ asset("storage/$location->name.jpg") }}" alt="{{ $location->name }} logo">
                            </div>
                            <div class="product-info">
                                <a href="{{ url("/items/location") }}" class="product-title">
                                    {{ $location->name }}
                                    <span class="label label-info pull-right">{{ $location->total_items }}</span>
                                </a>
                                <span class="product-description">{{ $location->sublocation_list }}</span>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a class="btn btn-link btn-xs aBtn" href="{{ url("/items/location") }}">
                        <i class="fa fa-fw fa-eye"></i> Total Inventoried Items: <strong>{{ $totalItems }}</strong>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Recent Log Entries</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="hidden-md hidden-sm hidden-xs">#</th>
                            <th class="text-center">Item ID</th>
                            <th>Type / Location</th>
                            <th class="hidden-md hidden-sm hidden-xs">Action</th>
                            <th>Date / Time</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($itemsLogs as $log)
                        <tr>
                            <td class="hidden-md hidden-sm hidden-xs">{{ $i++ }}</td>
                            <td class="text-center">{{ $log->item_tag_id }}</td>
                            <td>
                                <a class="btn btn-link btn-xs" href="{{ url("items/$log->item_id") }}" data-toggle="tooltip" data-placement="top" title="view details">{{ $log->item_type_name }}</a><br>
                                <small>{!! $log->items_location !!}</small>
                            </td>
                            <td class="hidden-md hidden-sm hidden-xs">{{ $log->action }}</td>
                            <td>{{ $log->date_time }}</td>
                            <th><a class="btn btn-default btn-xs aBtn" href="{{ url("items/$log->item_id") }}" data-toggle="tooltip" data-placement="top" title="view details"><i class="fa fa-eye fa-fw"></i></a></th>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a class="btn btn-link btn-xs aBtn" href="{{ url('/log') }}"><i class="fa fa-eye fa-fw"></i> View all log entries</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Third Row -->
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Recent WEB / IT Reports</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-striped">
                        <tbody>
                        @foreach($itReports as $itReport)
                            <tr>
                                <td>{{ $ii++ }}</td>
                                <td>{{ $itReport->type->name }}</td>
                                <td>{{ $itReport->title }}</td>
                                <td>
                                    <a class="btn btn-default btn-xs" download="{{ $itReport->filename }}" href="{{ asset("storage/documents/$itReport->year/$itReport->filename") }}" data-toggle="tooltip" data-placement="top" title="download the file"><i class="fa fa-fw fa-download"></i></a>
                                    <a class="btn btn-default btn-xs" href="{{ asset("storage/documents/$itReport->year/$itReport->filename") }}" target="_blank" data-toggle="tooltip" data-placement="top" title="view in browser"><i class="fa fa-fw fa-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a class="btn btn-link btn-xs aBtn" href="{{ url("/documents/type/9") }}">
                        <i class="fa fa-fw fa-eye"></i> View More Reports
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Recent Invoices</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-striped">
                        <tbody>
                        @foreach($invoices as $invoice)
                            <tr>
                                <td>{{ $ii++ }}</td>
                                <td>{{ $invoice->type->name }}</td>
                                <td>{{ $invoice->title }}</td>
                                <td>
                                    <a class="btn btn-default btn-xs" download="{{ $invoice->filename }}" href="{{ asset("storage/documents/$invoice->year/$invoice->filename") }}" data-toggle="tooltip" data-placement="top" title="download the file"><i class="fa fa-fw fa-download"></i></a>
                                    <a class="btn btn-default btn-xs" href="{{ asset("storage/documents/$invoice->year/$invoice->filename") }}" target="_blank" data-toggle="tooltip" data-placement="top" title="view in browser"><i class="fa fa-fw fa-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a class="btn btn-link btn-xs aBtn" href="{{ url("/documents/type/1") }}">
                        <i class="fa fa-fw fa-eye"></i> View More Invoices
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Recent Orders</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-striped">
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{ $ii++ }}</td>
                                <td>{{ $order->type->name }}</td>
                                <td>{{ $order->title }}</td>
                                <td>
                                    <a class="btn btn-default btn-xs" download="{{ $order->filename }}" href="{{ asset("storage/documents/$order->year/$order->filename") }}" data-toggle="tooltip" data-placement="top" title="download the file"><i class="fa fa-fw fa-download"></i></a>
                                    <a class="btn btn-default btn-xs" href="{{ asset("storage/documents/$order->year/$order->filename") }}" target="_blank" data-toggle="tooltip" data-placement="top" title="view in browser"><i class="fa fa-fw fa-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a class="btn btn-link btn-xs aBtn" href="{{ url("/documents/type/2") }}">
                        <i class="fa fa-fw fa-eye"></i> View More Orders
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Fourth Row -->
    <div class="row">
        <div class="col-lg-4">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Server / Communication Rooms Maintenance Schedule</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-striped">
                        <tbody>
                        @foreach($subLocations as $itRoom)
                            <tr>
                                <td><a class="btn btn-link btn-xs" href="{{ url("room/code/$itRoom->room_code_slug") }}">{{ $itRoom->location_name }}</a></td>
                                <td>{{ $itRoom->roomMaintenance()->orderBy('date_time', 'desc')->first()['date_time'] }}</td>
                                <td><a class="btn btn-default btn-xs aBtn" href="{{ url("room/code/$itRoom->room_code_slug") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a class="btn btn-link btn-xs aBtn" href="{{ url("room/maintenance") }}">
                        <i class="fa fa-fw fa-eye"></i> View Full List
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Chart</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <canvas id="myChart" width="400" height="400"></canvas>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a class="btn btn-link btn-xs aBtn" href="{{ url("room/maintenance") }}">
                        <i class="fa fa-fw fa-eye"></i> View Full List
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
@endsection
