@extends('layouts.master')

@section('title', 'Items')

@section('hTitle')
    @include('parts.backBtn', ['url' => "/dashboard", 'title' => 'back to the dashboard'])
    Inventory Items
    <small>List</small>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">
                Items Total <span class="badge">{{ count($items) }}</span>
            </h3>
            <div class="box-tools"></div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            @include('parts.session-message')
            <div class="table-responsive">
                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Tag ID</th>
                        <th>Type / Model / Brand</th>
                        <th>Serial Number</th>
                        <th>Location</th>
                        <th>Purchase Date</th>
                        <th colspan="2"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $item->inventory_tag_id }}</td>
                            <td>
                                <a class="btn btn-link btn-xs aBtn" href="{{ url("/items/$item->id") }}">
                                    <i class="fa fa-fw fa-eye"></i>
                                    <span data-toggle="tooltip" data-placement="top" title="{{ $item->imodel->type->description }}">{{ $item->brand }} / {{ $item->imodel->type->name }} / {{ $item->imodel->name_number }}</span>
                                </a>
                            </td>
                            <td>{{ $item->serial_number }}</td>
                            <td>{!! $item->full_location !!}</td>
                            <td>{{ $item->purchase_date }}</td>
                            <td><a class="btn btn-default btn-xs aBtn" href="{{ url("/items/$item->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                            <td><a class="btn btn-primary btn-xs aBtn" href="{{ url("/items/$item->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div> <!-- /.box-body -->
    </div>
@endsection