<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group{{ $errors->has('department_id') ? ' has-error' : '' }}">
            <strong>Departments</strong> <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <ul class="list-unstyled">
                @foreach($departments as $id => $name)
                    <li><label>{!! Form::checkbox("department_id[$id]", $id); !!} {{ $name }}</label></li>
                @endforeach
            </ul>
            @if ($errors->has('department_id'))
                <span class="help-block"><strong>{{ $errors->first('department_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('sublocation_id') ? ' has-error' : '' }}">
            {!! Form::label('sublocation_id', 'Item Location') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('sublocation_id', $sublocations, null, ['class' => 'form-control', 'placeholder' => 'select location', 'autofocus']) !!}
            @if ($errors->has('sublocation_id'))
                <span class="help-block"><strong>{{ $errors->first('sublocation_id') }}</strong></span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('imodel_id') ? ' has-error' : '' }}">
            {!! Form::label('imodel_id', 'Model / Type / Brand') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('imodel_id', $models, null, ['class' => 'form-control', 'placeholder' => 'select model/type/brand']) !!}
            @if ($errors->has('imodel_id'))
                <span class="help-block"><strong>{{ $errors->first('imodel_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('serial_number') ? ' has-error' : '' }}">
            {!! Form::label('serial_number', 'Item Serial / Product Number') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-hashtag fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('serial_number', null, ['class' => 'form-control', 'placeholder' => 'serial or product number', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('serial_number'))
                <span class="help-block"><strong>{{ $errors->first('serial_number') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('inventory_tag_id') ? ' has-error' : '' }}">
            {!! Form::label('inventory_tag_id', 'Tag ID') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-tag fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('inventory_tag_id', null, ['class' => 'form-control', 'placeholder' => 'tag id', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('inventory_tag_id'))
                <span class="help-block"><strong>{{ $errors->first('inventory_tag_id') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group{{ $errors->has('purchase_date') ? ' has-error' : '' }}">
            {!! Form::label('purchase_date', 'Purchase Date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group date" id="datepicker">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Item purchase date"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('purchase_date', null, ['class' => 'form-control', 'placeholder' => 'purchase date', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('purchase_date'))
                <span class="help-block"><strong>{{ $errors->first('purchase_date') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
            {!! Form::label('price', 'Item Price') !!}
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-usd fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'item price', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('price'))
                <span class="help-block"><strong>{{ $errors->first('price') }}</strong></span>
            @endif
        </div>
        <!-- Textarea with ckeditor -->
        @include('parts.textarea', ['name' => 'comments', 'title' => 'Comments'])
    </div>
</div>