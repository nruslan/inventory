@extends('layouts.master')

@section('title', $sublocation->room_name)

@section('hTitle')
    {{ $location->name }} &bullet; {{ $sublocation->room_name }}
    <small>{{ $sublocation->room_code }}</small>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Tag ID</th>
                <th>Type / Model / Brand</th>
                <th>Serial Number</th>
                <th>Purchase Date</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($sublocation->items as $item)
                <tr>
                    <th scope="row">{{ $i++ }}</th>
                    <td>{{ $item->inventory_tag_id }}</td>
                    <td>
                        <a class="btn btn-link btn-xs aBtn" href="{{ url("/items/$item->id") }}">
                            <i class="fa fa-fw fa-eye"></i>
                            <span data-toggle="tooltip" data-placement="top" title="{{ $item->imodel->type->description }}">{{ $item->imodel->type->name }}</span> / {{ $item->imodel->name_number }} / {{ $item->brand }}
                        </a>
                    </td>
                    <td>{{ $item->serial_number }}</td>
                    <td>{{ $item->purchase_date }}</td>
                    <td><a class="btn btn-default btn-xs aBtn" href="{{ url("/items/$item->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer"></div>
</div>
@endsection