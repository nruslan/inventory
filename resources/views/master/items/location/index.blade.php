@extends('layouts.master')

@section('title', 'Items Location')

@section('hTitle')
    SB2 Inventory
    <small>Total Items per Location</small>
@endsection

@section('content')
    @foreach($locations as $location)
    @php $i = 1 @endphp
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Total Items at {{ $location->name }} <span class="badge">{{ $location->total_items }}</span></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Location</th>
                    <th>Room Code</th>
                    <th>Room Name</th>
                    <th class="text-center">Items Total</th>
                    <th class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($location->sublocations as $sublocation)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $location->name }}</td>
                            <td>{{ $sublocation->room_code }}</td>
                            <td>{{ $sublocation->room_name }}</td>
                            <td class="text-center">{{ $sublocation->total_items_per_room }}</td>
                            <td class="text-center">
                                <a class="btn btn-default btn-xs aBtn" href="{{ url("/items/location/$sublocation->id") }}" data-toggle="tooltip" data-placement="top" title="view details"><i class="fa fa-fw fa-eye"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer text-center">
            Total Inventoried Items at {{ $location->name }}: <strong>{{ $location->total_items }}</strong>
        </div>
    </div>
    @endforeach
@endsection