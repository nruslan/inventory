@extends('layouts.master')

@section('title', 'Items Report')

@section('hTitle')
    @include('parts.backBtn', ['url' => "/items?page=$pageId", 'title' => 'back to the items list'])
    Report by date of purchase
@endsection

@section('breadcrumbs')
    <li class="active">Items Reports</li>
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! Form::open(['method' => 'GET', 'url' => 'items/reports/purchase-date-range', 'id' => 'myForm']) !!}
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="form-group{{ $errors->has('date_from') ? ' has-error' : '' }}">
                        {!! Form::label('date_from', 'From') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group date" id="datepicker-from">
                            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                            {!! Form::text('date_from', null, ['class' => 'form-control', 'placeholder' => 'mm/dd/yyyy', 'autocomplete' => 'off', 'autofocus', 'required']) !!}
                        </div>
                        @if ($errors->has('date_from'))<span class="help-block"><strong>{{ $errors->first('date_from') }}</strong></span>@endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('date_to') ? ' has-error' : '' }}">
                        {!! Form::label('date_from', 'To') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group date" id="datepicker-to">
                            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                            {!! Form::text('date_to', null, ['class' => 'form-control', 'placeholder' => 'mm/dd/yyyy', 'autocomplete' => 'off', 'required']) !!}
                        </div>
                        @if ($errors->has('date_to'))<span class="help-block"><strong>{{ $errors->first('date_to') }}</strong></span>@endif
                    </div>
                </div>
            </div>
            @include('parts.saveCancelBtns', ['cancel' => 'items'])
            {!! Form::close() !!}

            @if($search_results)
                <div class="table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th colspan="7">
                                Total Items: {{ $search_results->total() }}
                            </th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Tag ID</th>
                            <th>Type / Model / Brand</th>
                            <th>Serial Number</th>
                            <th>Location</th>
                            <th>Purchase Date</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($search_results as $item)
                            <tr>
                                <th scope="row">{{ $i++ }}</th>
                                <td>{{ $item->inventory_tag_id }}</td>
                                <td>
                                    <a class="btn btn-link btn-xs aBtn" href="{{ url("/items/$item->id") }}">
                                        <i class="fa fa-fw fa-eye"></i>
                                        <span data-toggle="tooltip" data-placement="top" title="{{ $item->imodel->type->description }}">{{ $item->imodel->type->name }}</span> / {{ $item->imodel->name_number }} / {{ $item->brand }}
                                    </a>
                                </td>
                                <td>{{ $item->serial_number }}</td>
                                <td>{!! $item->full_location !!}</td>
                                <td>{{ $item->purchase_date }}</td>
                                <td><a class="btn btn-default btn-xs aBtn" href="{{ url("/items/$item->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
        <!-- /.box-body -->
        @if($search_results)
            <div class="box-footer">
                {{ $search_results->appends(['date_from' => $date_from, 'date_to' => $date_to])->links('vendor.pagination.admin-lte') }}
            </div>
        @endif
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker-from").datetimepicker({
                format: 'MM/DD/YYYY'
            });

            $("#datepicker-to").datetimepicker({
                format: 'MM/DD/YYYY'
            });

            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection