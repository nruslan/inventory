@extends('layouts.master')

@section('title', 'Items Report')

@section('hTitle')
    @include('parts.backBtn', ['url' => "/items?page=$pageId", 'title' => 'back to the items list'])
    Items Report
@endsection

@section('breadcrumbs')
    <li class="active">Items Reports</li>
@endsection

@section('content')
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <ul class="list-inline">
            <li><a class="btn btn-primary" href="{{ url('items/reports/by-purchase-date') }}">Report by Purchase Date</a></li>
        </ul>
    </div>
    <!-- /.box-body -->
    <div class="box-footer"></div>
</div>
@endsection