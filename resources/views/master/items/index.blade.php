@extends('layouts.master')

@section('title', 'Items')

@section('hTitle')
    Inventory Items
    <small>List</small>
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('items') }}
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">
                Items Total <span class="badge">{{ $items->total() }}</span>
                <a class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add new item" href="{{ url('items/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a>
            </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" title="Reports">
                        <i class="fa fa-cog"></i></button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url("items/reports/purchase-date-range") }}">Report by date</a></li>
                        <li><a href="#">Report by Department</a></li>
                        <li><a href="#">Report by Item</a></li>
                        <li class="divider"></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4">
                    {!! Form::open(['method' => 'GET', 'url' => 'items/search', 'id' => 'myForm']) !!}
                    <div class="input-group input-group-sm">
                        {!! Form::text('tag_id', null, ['class' => 'form-control', 'placeholder' => 'search by tag id', 'autocomplete' => 'off']) !!}
                        <span class="input-group-btn"><button class="btn btn-info btn-flat">Go!</button></span>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            @include('parts.session-message')
            <div class="table-responsive">
                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Tag ID</th>
                        <th>Type / Model / Brand</th>
                        <th>Serial Number</th>
                        <th>Location</th>
                        <th class="text-center">Purchase Date</th>
                        <th colspan="2"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>
                                {{ $item->inventory_tag_id }}
                                @if($item->disposal_date)<i class="icon fa fa-ban fa-fw" data-toggle="tooltip" data-placement="top" title="The Item was disposed on {{ $item->disposal_date }}"></i>@endif
                            </td>
                            <td>
                                <a class="btn btn-link btn-xs aBtn" href="{{ url("/items/$item->id") }}">
                                    <i class="fa fa-fw fa-eye"></i>
                                    <span data-toggle="tooltip" data-placement="top" title="{{ $item->imodel->type->description }}">{{ $item->imodel->type->name }}</span> / {{ $item->imodel->name_number }} / {{ $item->brand }}
                                </a>
                            </td>
                            <td>{{ $item->serial_number }}</td>
                            <td>{!! $item->full_location !!}</td>
                            <td class="text-center">{{ $item->purchase_date }}</td>
                            <td><a class="btn btn-default btn-xs aBtn" href="{{ url("/items/$item->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                            <td><a class="btn btn-primary btn-xs aBtn" href="{{ url("/items/$item->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{ $items->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $(".pagination li a").on('click', function(){
                $(this).html('<i class="fa fa-spinner fa-pulse"></i>');
                $(this).addClass('disabled');
            });
        });
    </script>
@endsection
