@extends('layouts.master')

@section('hTitle', 'Edit Item Details')
    
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit Inventory Item Details</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::model($item,['method' => 'PATCH', 'action' => ['Master\Items\IndexController@update', $item->id], 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('master.items.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => "items/$item->id"])
                </div>
            {!! Form::close() !!}
            <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $( "#datepicker" ).datetimepicker({
                viewMode: 'years',
                format: 'MM/DD/YYYY'
            });
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection
