@extends('layouts.master')

@section('title', $item->imodel->name_number)

@section('hTitle')
    @include('parts.backBtn', ['url' => "$previousUrl", 'title' => 'go back'])
    Inventory Item Details
    <small>Control panel</small>
@endsection

@section('breadcrumbs', Breadcrumbs::render('items-show', $item))

@section('content')
    @if($computer && !$item->name)
    <div class="modal fade in" id="c-name" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Computer Name</h4>
                </div>
                {!! Form::open(['url' => "item/computer-name", 'id' => 'cNameForm']) !!}
                {!! Form::hidden('item_id', $item->id) !!}
                <div class="modal-body">
                    <div class="form-group{{ $errors->has('operating_system_id') ? ' has-error' : '' }}">
                        {!! Form::label('operating_system_id', 'Operating System') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                        {!! Form::select('operating_system_id', $os, null, ['class' => 'form-control', 'placeholder' => 'select operating system']) !!}
                        @if ($errors->has('operating_system_id'))<span class="help-block"><strong>{{ $errors->first('operating_system_id') }}</strong></span>@endif
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Computer Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'computer name']) !!}
                        @if ($errors->has('name'))<span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>@endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    @endif
    @if($item->disposal_date)
        <div class="callout callout-danger">
            <h4><i class="icon fa fa-ban"></i> Disposed Item</h4>
            <p>The Item was disposed on {{ $item->disposal_date }}. Check the log for more information.</p>
        </div>
    @endif
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Item <i class="fa fa-fw fa-hashtag"></i> {{ $item->inventory_tag_id }} {{ $item->type_category }}</h3>
                    @if($computer && !$item->name)<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#c-name"><i class="fa fa-plus-circle"></i></button>@endif

                    <a href="{{ url("items/$item->id/edit") }}" class="btn btn-primary btn-xs pull-right aBtn" data-toggle="tooltip" data-placement="top" data-container="body" title="Edit Item's Details"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @include('parts.session-message')
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <h4>Item Details</h4>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <ul class="list-unstyled">
                                        <li><strong>Brand:</strong> {{ $item->brand }}</li>
                                        <li><strong>Model:</strong> {{ $item->imodel->name_number }} &bullet; {{ $item->imodel->type->name }}</li>
                                        <li><strong>Serial Number:</strong> {{ $item->serial_number }}</li>
                                        @if($item->imodel->description)<li><strong>Item Description:</strong> {!!  $item->imodel->description !!}</li>@endif
                                    </ul>
                                    @if($item->name)
                                        <hr>
                                        <ul class="list-unstyled">
                                            <li><strong>Computer Name:</strong> {{ $item->name->name }} <span class="pull-right">@include('parts.defaultDeleteBtn', ['action' => ['Master\Items\ComputerNameController@destroy', $item->id], 'btnTitle' => 'Delete Computer Name', 'class' => 'btn-xs', 'placement' => 'left'])</span></li>
                                            <li><strong>Operating System:</strong> {{ $item->name->os->name }}</li>
                                        </ul>
                                    @endif
                                    @if($item->ip)
                                        <hr>
                                        <ul class="list-unstyled">
                                            <li>Network Connection Details</li>
                                            <li><strong>IP</strong>: {!! $item->ip->address !!}</li>
                                            @foreach($item->ip->network_details as $detail)
                                                <li><strong>{{ $detail->name }}</strong>: {{ $detail->address }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <hr>
                                    <ul class="list-unstyled">
                                        <li><strong>Purchase Price:</strong> <i class="fa fa-usd"></i>{{ $item->price }}</li>
                                        <li><strong>Purchase Date:</strong> {{ $item->purchase_date }}</li>
                                        <li><strong>Departments:</strong> {{ $item->related_departments }}</li>
                                        <li><strong>Location:</strong> {!! $item->full_location !!}</li>
                                    </ul>
                                    @if($item->comments)
                                        <hr>
                                        <h4>Comments</h4>
                                        {!! $item->comments !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="table-responsive">
                                <h4>
                                    Users <small class="badge">{{ count($item->users) }}</small>
                                    <a href="{{ url("/item/user/create?id=$item->id") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add user to the list"><i class="fa fa-fw fa-plus" aria-hidden="true"></i> Add User</a>
                                </h4>
                                @if(count($item->users) > 0)
                                <table class="table table-condensed table-hover" role="grid">
                                    <thead>
                                    <tr role="row">
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th colspan="2"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($item->users as $user)
                                        <tr role="row">
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->pivot->begins }}</td>
                                            <td>{{ $user->pivot->ends }}</td>
                                            <td><a href="{{ url("/item/user/".$user->pivot->id."/edit") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Edit users list"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i></a></td>
                                            <td>@include('parts.trashBtnSm', ['action' => ['Master\Items\UserController@destroy', $user->pivot->id], 'btnTitle' => $user->name])</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table><!-- End Users -->
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="list-inline">
                        <li class="pull-left">
                            @if($item->disposal_date)
                                @include('parts.trashBtn', ['action' => ['Master\Items\DisposalDateController@destroy', $item->id], 'btnTitle' => 'Restore the Item', 'class' => 'btn-xs', 'name' => ' Restore', 'position' => 'right'])
                            @else
                                <a href="{{ url("item/disposal-date/$item->id/edit") }}" class="btn btn-warning btn-xs pull-right aBtn" data-toggle="tooltip" data-placement="top" title="dispose of the item"><i class="fa fa-fw fa-calendar-times-o" aria-hidden="true"></i> Disposal Date</a>
                            @endif
                        </li>
                        <li class="pull-right">@include('parts.trashBtn', ['action' => ['Master\Items\IndexController@destroy', $item->id], 'btnTitle' => 'Delete the item', 'class' => 'btn-xs', 'name' => ' Delete', 'position' => 'left'])</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i> Item's Log <span class="label label-success">{{ $logs->total() }}</span></h3>
                    <a href="{{ url("item/log/create?id=$item->id") }}" class="btn btn-primary btn-xs aBtn pull-right"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Add Log Entry</a>
                </div>
                <!-- /.box-header -->
                @if($logs->total())
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-condensed" role="grid">
                        <thead>
                        <tr role="row">
                            <th>#</th>
                            <th>Action</th>
                            <th>User</th>
                            <th>Created at</th>
                            <th>Entry</th>
                            <th colspan="2"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($logs as $log)
                            <tr role="row">
                                <th scope="row">{{ $ii++ }}</th>
                                <td>{{ $log->action }}</td>
                                <td>{{ $log->user->name }}</td>
                                <td>{{ $log->created_at }}</td>
                                <td>{{ $log->short_entry }}</td>
                                <td><a href="{{ url("/item/log/$log->id") }}" class="btn btn-default btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="view details"><i class="fa fa-fw fa-eye" aria-hidden="true"></i></a></td>
                                <td><a href="{{ url("/item/log/$log->id/edit") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="edit"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        @if($item->computer_type)
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Item's Accounts <span class="badge">{{ $item->itemAccounts->count() }}</span></h3>
                    <a href="{{ url("item/account/create?id=$item->id") }}" class="btn btn-primary btn-xs aBtn pull-right" data-toggle="tooltip" data-placement="top" title="Add Item's Account"><i class="fa fa-plus-circle fa-fw"></i></a>
                </div>
                <!-- /.box-header -->
                @if($item->itemAccounts->count())
                <div class="box-body">
                    @foreach($item->itemAccounts as $iAccount)
                        <div class="form-inline">
                            <label for="">Item's {{ $iAccount->type->name }} account</label><br>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon" data-toggle="tooltip" data-placement="top" title="Username"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                                    {!! Form::text('username', $iAccount->username, ['class' => 'form-control', 'placeholder' => 'username', 'readonly']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    @if($iAccount->hidden_password)
                                    <span class="input-group-addon" data-toggle="tooltip" data-placement="top" title="Password"><i class="fa fa-fw fa-key" aria-hidden="true"></i></span>
                                    {!! Form::input('password', 'password', $iAccount->hidden_password, ['class' => 'form-control passwordInput', 'placeholder' => 'password', 'readonly']) !!}
                                    <span class="input-group-addon" data-toggle="tooltip" data-container="body" data-placement="top" title="check to view"><input type="checkbox" value="{{ $iAccount->id }}" class="aId"></span>
                                    @else
                                        --- no password ---
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <ul class="list-inline">
                                    <li><a href="{{ url("item/account/$iAccount->id/edit") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Edit account info"><i class="fa fa-fw fa-pencil"></i></a></li>
                                    <li>@include('parts.trashBtn', ['action' => ['Master\Items\AccountController@destroy', $iAccount->id], 'btnTitle' => "Delete Item's account", 'class' => 'btn-xs'])</li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- /.box-body -->
                @endif
            </div>
        </div>
        @endif
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Documents <small class="badge">{{ $item->documents->count() }}</small></h3>
                    <ul class="list-inline pull-right">
                        <li><a href="{{ url("item/documents/list/$item->id/edit") }}"
                                class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="left"
                                title="Attach from the list"><i class="fa fa-fw fa-paperclip" aria-hidden="true"></i>
                                Attach</a>
                        </li>
                        <li><a href="{{ url("item/document/create?id=$item->id") }}" class="btn btn-primary btn-xs aBtn"
                                data-toggle="tooltip" data-placement="left" title="Attach new document"><i
                                        class="fa fa-fw fa-paperclip" aria-hidden="true"></i> Attach New</a></li>
                    </ul>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    @if($item->documents->count())
                        <table class="table table-condensed table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Title</th>
                                <th class="hidden-xs hidden-sm hidden-md">Uploaded by</th>
                                <th>File Size</th>
                                <th class="hidden-xs hidden-sm hidden-md">Date</th>
                                <th colspan="2" class="hidden-xs hidden-sm hidden-md"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($item->documents as $document)
                                <tr>
                                    <th scope="row">{{ $iii++ }}</th>
                                    <td>{{ $document->type->name }}</td>
                                    <td>
                                        <a class="btn btn-link btn-xs" href="{{ asset("storage/documents/$document->year/$document->filename") }}" target="_blank" data-toggle="tooltip" data-placement="top" title="view in browser">
                                            <i class="fa fa-fw fa-eye"></i> {{ $document->title }}</a>
                                    </td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{ $document->user->name }}</td>
                                    <td>{{ $document->file_size }}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">{{ $document->date }}</td>
                                    <td class="hidden-xs hidden-sm hidden-md">
                                        <a class="btn btn-default btn-xs" download="{{ $document->filename }}" href="{{ asset("storage/documents/$document->year/$document->filename") }}" data-toggle="tooltip" data-placement="top" title="download the file"><i class="fa fa-fw fa-download"></i></a>
                                    </td>
                                    <td class="hidden-xs hidden-sm hidden-md"><a class="btn btn-primary btn-xs aBtn" href="{{ url("/documents/$document->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                                    <td>@include('parts.trashBtnSm', ['action' => ['Master\Items\DocumentController@destroy', $document->pivot->id], 'btnTitle' => $document->title])</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- End Documents -->
                    @endif
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
//
let elements = document.querySelectorAll(".aId");
elements.forEach(function(aI){
    aI.addEventListener('click', function(e) {
        var currentEl = e.target;
        var x = currentEl.parentElement.parentElement.closest(".input-group").children[1];
        if(currentEl.checked) {
            x.type = "text";
            var id = currentEl.value;
                axios.post('/api/items-password', {
                    id: id
                }).then(function (response) {
                    x.value = response.data;
                    setTimeout(function(){
                        x.type = "password";
                        x.value = x.value.replace(/./g, "*");
                        currentEl.checked = false;
                    }, 10000);
                }).catch(function (error) {
                    console.log(error);
                });
        } else {
            x.type = "password";
            x.value = x.value.replace(/./g, "*");
        }
    });
});
//
$('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]'
});

$(".deleteBtnForm").submit(function() {
    $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
});
$("#cNameForm").submit(function() {
    $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
});
</script>
@endsection
