@extends('layouts.master')

@section('title', 'Search Items')

@section('hTitle')
    @include('parts.backBtn', ['url' => "/items?page=$pageId", 'title' => 'back to items list'])
    Search Inventory Items
    <small>by Tag ID <strong>{{ $search }}</strong></small>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Total Items Found <span class="badge">{{ $items->total() }}</span>
            </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="table-responsive">
                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Tag ID</th>
                        <th>Type / Model / Brand</th>
                        <th>Serial Number</th>
                        <th>Location</th>
                        <th>Purchase Date</th>
                        <th colspan="2"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $item->inventory_tag_id }}</td>
                            <td>
                                <a class="btn btn-link btn-xs aBtn" href="{{ url("/items/$item->id") }}">
                                    <i class="fa fa-fw fa-eye"></i>
                                    <span data-toggle="tooltip" data-placement="top" title="{{ $item->imodel->type->description }}">{{ $item->imodel->type->name }}</span> / {{ $item->imodel->name_number }} / {{ $item->brand }}
                                </a>
                            </td>
                            <td>{{ $item->serial_number }}</td>
                            <td>{!! $item->full_location !!}</td>
                            <td>{{ $item->purchase_date }}</td>
                            <td><a class="btn btn-default btn-xs aBtn" href="{{ url("/items/$item->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                            <td><a class="btn btn-primary btn-xs aBtn" href="{{ url("/items/$item->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{ $items->appends(['tag_id' => $search])->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $(".pagination li a").on('click', function(){
                $(this).html('<i class="fa fa-spinner fa-pulse"></i>');
                $(this).addClass('disabled');
            });
        });
    </script>
@endsection
