<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
            <div class="row">
                <div class="col-md-4">
                    @foreach($filtered_users as $id => $name)
                        <div class="radio"><label>{!! Form::radio("user_id[]", $id) !!} {{ $name }}</label></div>
                        @if($i++ % 9 == 0)</div><div class="col-md-4">@endif
                    @endforeach
                </div>
            </div>
            @if ($errors->has('user_id'))
                <span class="help-block"><strong>{{ $errors->first('user_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('begins') ? ' has-error' : '' }}">
            {!! Form::label('begins', 'Start Date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group date" id="datepicker">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the user started to use the item"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('begins', null, ['class' => 'form-control', 'id' => 'datepicker', 'placeholder' => 'date', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('begins'))
                <span class="help-block"><strong>{{ $errors->first('begins') }}</strong></span>
            @endif
        </div>
    </div>
</div>