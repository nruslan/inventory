@extends('layouts.master')

@section('title', 'Edit Users List')

@section('hTitle')
    Edit Users Dates
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Dates for {{ $user->name }} <small>{{ $item->imodel->type->name }} &bullet; {{ $item->imodel->name_number }}</small> </h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($item_user, ['method' => 'PATCH', 'action' => ['Master\Items\UserController@update', $item_user->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="form-group{{ $errors->has('begins') ? ' has-error' : '' }}">
                        {!! Form::label('begins', 'Start Date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group date" id="datepicker-begins">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the user started to use the item"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('begins', null, ['class' => 'form-control', 'placeholder' => 'date', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('begins'))
                            <span class="help-block"><strong>{{ $errors->first('begins') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('ends') ? ' has-error' : '' }}">
                        {!! Form::label('ends', 'End Date') !!}
                        <div class="input-group date" id="datepicker-ends">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="date when the user started to use the item"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('ends', null, ['class' => 'form-control', 'placeholder' => 'date', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('ends'))
                            <span class="help-block"><strong>{{ $errors->first('ends') }}</strong></span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "/items/$item->id"])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#datepicker-begins").datetimepicker({
                format: 'MM/DD/YYYY'
            });

            $("#datepicker-ends").datetimepicker({
                format: 'MM/DD/YYYY'
            });

            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection










