@extends('layouts.master')

@section('title', "Add Account")

@section('hTitle')
    Add Account for {{ $item->imodel->type->name }} &bullet; {{ $item->imodel->name_number }}
@endsection

@section('content')
<div class="box box-primary">
    {!! Form::open(['url' => 'item/account', 'id' => 'myForm']) !!}
    <div class="box-body">
        {!! Form::hidden('item_id', $item->id) !!}
        @include('master.items.account.formBody')
    </div>
    <div class="box-footer">
        @include('parts.saveCancelBtns', ['cancel' => "items/$item->id"])
    </div>
{!! Form::close() !!}
<!-- /.box-body -->
</div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection