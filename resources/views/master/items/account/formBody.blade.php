<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="form-group{{ $errors->has('item_account_type_id') ? ' has-error' : '' }}">
            {!! Form::label('item_account_type_id', 'Account Type') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('item_account_type_id', $accountTypes, null, ['class' => 'form-control', 'autofocus']) !!}
            @if ($errors->has('item_account_type_id'))
                <span class="help-block"><strong>{{ $errors->first('item_account_type_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            {!! Form::label('username', 'Username') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-fw fa-lock" aria-hidden="true"></i></span>
                {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'username', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('username'))
                <span class="help-block"><strong>{{ $errors->first('username') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            {!! Form::label('password', 'Password') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-fw fa-key" aria-hidden="true"></i></span>
                {!! Form::text('password', null, ['class' => 'form-control', 'placeholder' => 'password', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('password'))
                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
        <!-- Textarea with ckeditor -->
        @include('parts.textarea', ['name' => 'comments', 'title' => 'Comments'])
    </div>
</div>