@extends('layouts.master')

@section('title', "Edit Account")

@section('hTitle')
    Edit {{ $iAccount->type->name }} Account for {{ $item->imodel->type->name }} &bullet; {{ $item->imodel->name_number }}
@endsection

@section('content')
    <div class="box box-primary">
        {!! Form::model($iAccount, ['method' => 'PATCH', 'action' => ['Master\Items\AccountController@update', $iAccount->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('master.items.account.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "items/$item->id"])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection