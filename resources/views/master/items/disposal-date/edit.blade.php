@extends('layouts.master')

@section('title', 'Set or Update Disposal date')

@section('hTitle')
    Set or Update Disposal date
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Set Disposal Date for {{ $item->imodel->type->name }} &bullet; {{ $item->imodel->name_number }}</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($item, ['method' => 'PATCH', 'action' => ['Master\Items\DisposalDateController@update', $item->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="form-group{{ $errors->has('disposal_date') ? ' has-error' : '' }}">
                        {!! Form::label('disposal_date', 'Disposal Date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
                        <div class="input-group date" id="datepicker">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Item purchase date"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('disposal_date', null, ['class' => 'form-control', 'placeholder' => 'disposal date', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('disposal_date'))
                            <span class="help-block"><strong>{{ $errors->first('disposal_date') }}</strong></span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "items/$item->id"])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({
                format: 'MM/DD/YYYY'
            });

            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection
