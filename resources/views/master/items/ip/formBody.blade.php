<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
            {!! Form::label('address', 'IP Address') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="IP address"><i class="fa fa-info fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'ip address', 'autocomplete' => "nope"]) !!}
            </div>
            @if ($errors->has('address'))<span class="help-block"><strong>{{ $errors->first('address') }}</strong></span>@endif
        </div>
        <div class="form-group{{ $errors->has('network_detail_id') ? ' has-error' : '' }}">
            @foreach($networkDetails as $id => $name)
            <div class="checkbox">
                <label>{!! Form::checkbox("network_detail_id[$id]", $id) !!} {{ $name }}</label>
            </div>
            @endforeach
            @if ($errors->has('network_detail_id'))<span class="help-block"><strong>{{ $errors->first('network_detail_id') }}</strong></span>@endif
        </div>
        <!-- Textarea with ckeditor -->
        @include('parts.textarea', ['name' => 'comments', 'title' => 'Comments'])
    </div>
</div>