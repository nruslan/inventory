@extends('layouts.master')

@section('title', $ip->address)

@section('hTitle')
    @include('parts.backBtn', ['url' => "/ips?page=$pageId", 'title' => 'back to the list'])
    {{ $ip->address }} for item# {{ $ip->item->inventory_tag_id }}
@endsection

@section('content')
<div class="box box-default">
    <div class="box-header with-border">
        <a href="{{ url("/ips/$ip->id/edit") }}" class="btn btn-primary btn-xs aBtn pull-right" data-toggle="tooltip" data-placement="top" title="edit details"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> edit</a>
        <h3 class="box-title">
            {{ $ip->address }}
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @include('parts.session-message')
        <div class="row">
            <div class="col-lg-6">
                <ul class="list-unstyled">
                    @foreach($ip->network_details as $detail)
                        <li><strong>{{ $detail->name }}</strong>: {{ $detail->address }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="col-lg-6">
                <ul class="list-unstyled">
                    <li><strong>Brand:</strong> {{ $item->brand }}</li>
                    <li><strong>Model:</strong> {{ $item->imodel->name_number }} &bullet; {{ $item->imodel->type->name }}</li>
                    <li><strong>Serial Number:</strong> {{ $item->serial_number }}</li>
                    <li><strong>Item Description:</strong> {{ $item->imodel->description or '-- N/A --'}}</li>
                </ul>
            </div>
        </div>

    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <ul class="list-unstyled">
            <li></li>
            <li class="pull-right">@include('parts.trashBtn', ['action' => ['Master\Items\IpsController@destroy', $ip->id], 'btnTitle' => 'Remove the IP address', 'class' => 'btn-xs', 'name' => ' Delete', 'position' => 'left'])</li>
        </ul>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection