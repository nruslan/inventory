<label>Items</label> <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
@foreach($items as $item)
    <div class="radio">
        <label>
            {!! Form::radio('item_id', $item->id) !!} {{ $item->inventory_tag_id }} &bullet; {{ $item->imodel->type->name }} / {{ $item->imodel->name_number }} / {{ $item->brand }} &bullet; {!! $item->full_location !!}
        </label>
    </div>
@endforeach