@extends('layouts.master')

@section('title', 'IP Addresses')

@section('hTitle', 'Fixed IP Addresses')
    
@section('content')
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">IP Addresses <span class="badge">{{ $ipAddresses->total() }}</span>
            <a class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add new IP" href="{{ url('ips/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a>
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @include('parts.session-message')
        <div class="table-responsive">
            <table class="table table-condensed table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>IP Address</th>
                    <th>Item's Tag ID</th>
                    <th>Type / Model / Brand</th>
                    <th>Location</th>
                    <th colspan="2"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($ipAddresses as $ip)
                    <tr>
                        <th scope="row">{{ $i++ }}</th>
                        <td>{{ $ip->address }}</td>
                        <td>{{ $ip->item->inventory_tag_id }}</td>
                        <td>
                            <a class="btn btn-link btn-xs aBtn" href="{{ url("/items/". $ip->item->id) }}">
                                <i class="fa fa-fw fa-eye"></i>
                                <span data-toggle="tooltip" data-placement="top" title="{{ $ip->item->imodel->type->description }}">{{ $ip->item->imodel->type->name }}</span> / {{ $ip->item->imodel->name_number }} / {{ $ip->item->brand }}
                            </a>
                        </td>
                        <td>{!! $ip->item->full_location !!}</td>
                        <td><a class="btn btn-default btn-xs aBtn" href="{{ url("/ips/$ip->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                        <td><a class="btn btn-primary btn-xs aBtn" href="{{ url("/ips/$ip->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        {{ $ipAddresses->links('vendor.pagination.admin-lte') }}
    </div>
</div>
@endsection