@extends('layouts.master')

@section('title', 'Edit IP Address Details')

@section('hTitle')
    Edit IP Address Details
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add IP Address</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($ip,['method' => 'PATCH', 'action' => ['Master\Items\IpsController@update', $ip->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('master.items.ip.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "ips/$ip->id"])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection