@extends('layouts.master')

@section('title', 'Add IP Address')

@section('hTitle')
    Add IP Address for the Item
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add IP Address</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::open(['url' => 'ips', 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="input-group{{ $errors->has('tag_number') ? ' has-error' : '' }}">
                        {!! Form::text('tag_number', null, ['class' => 'form-control', 'id' => 'tagId', 'placeholder' => 'item tag number', 'autocomplete' => 'off']) !!}
                        <span class="input-group-btn"><button type="button" class="btn btn-info btn-flat" onclick="getItemId()"><i class="fa fa-search fa-fw"></i></button></span>
                    </div>
                    <hr>
                    <div class="input-group{{ $errors->has('item_id') ? ' has-error' : '' }}" id="result">
                        @if ($errors->has('item_id'))
                            <span class="help-block"><strong>{{ $errors->first('item_id') }}</strong></span>
                        @endif
                    </div>
                </div>
            </div>
            @include('master.items.ip.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "ips?page=$pageId"])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        function getItemId() {
            var id = document.getElementById("tagId").value;

            axios.post('/api/item', {
                id: id
            })
                .then(function (response) {
                    document.getElementById("result").innerHTML = response.data;
                    //x.value = response.data;
                    //console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection