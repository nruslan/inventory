@extends('layouts.master')

@section('hTitle')
    Add New Item
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add Inventory Item</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::open(['url' => 'items', 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('master.items.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => 'items'])
        </div>
        {!! Form::close() !!}
        <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection
