<div class="form-group{{ $errors->has('action') ? ' has-error' : '' }}">
    {!! Form::label('action', 'Action') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
    <div class="input-group">
        <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-bolt fa-fw" aria-hidden="true"></i></span>
        {!! Form::text('action', null, ['class' => 'form-control', 'placeholder' => 'action', 'autocomplete' => 'off']) !!}
    </div>
    @if ($errors->has('action'))<span class="help-block"><strong>{{ $errors->first('action') }}</strong></span>@endif
</div>
<!-- Textarea with ckeditor -->
@include('parts.textarea', ['name' => 'entry', 'title' => 'Detailed Description'])