@extends('layouts.master')

@section('title', 'Log Entry')

@section('hTitle')
    Log Entry for Item
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">New Log Entry for <strong>{{ $item->imodel->type->name }} &bullet; {{ $item->imodel->name_number }}</strong></h3>
                </div>
                <!-- /.box-header -->
                {!! Form::open(['url' => 'item/log', 'id' => 'myForm']) !!}
                {!! Form::hidden('item_id', $item->id) !!}
                <div class="box-body">
                    @include('master.items.log.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => "items/$item->id"])
                </div>
                {!! Form::close() !!}
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({
                format: 'MM/DD/YYYY'
            });

            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection
