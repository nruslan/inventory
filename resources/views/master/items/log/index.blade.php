@extends('layouts.master')

@section('title', 'Log Entries')

@section('hTitle', 'Items Log Entries')
    
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Items Log Entries <span class="badge">{{ $itemsLogs->total() }}</span></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Item ID</th>
                    <th>Type</th>
                    <th>Location</th>
                    <th>Action</th>
                    <th>Date / Time</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($itemsLogs as $log)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $log->item_tag_id }}</td>
                        <td><a class="btn btn-link btn-xs" href="{{ url("items/$log->item_id") }}" data-toggle="tooltip" data-placement="top" title="view details">{{ $log->item_type_name }}</a></td>
                        <td>{!! $log->items_location !!}</td>
                        <td>{{ $log->action }}</td>
                        <td>{{ $log->date_time }}</td>
                        <th><a class="btn btn-default btn-xs aBtn" href="{{ url("items/$log->item_id") }}" data-toggle="tooltip" data-placement="top" title="view details"><i class="fa fa-eye fa-fw"></i></a></th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{ $itemsLogs->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection