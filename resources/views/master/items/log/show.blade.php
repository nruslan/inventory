@extends('layouts.master')

@section('title', $item->imodel->name_number)

@section('hTitle')
    @include('parts.backBtn', ['url' => "/items/$item->id", 'title' => 'back to the item details'])
    {{ $item->imodel->type->name }} &bullet; {{ $item->imodel->name_number }}
    <small>Log</small>
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Log by {{ $log->user->name }}</h3>
            <a href="{{ url("items/$item->id/edit") }}" class="btn btn-primary btn-xs aBtn pull-right"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! $log->entry !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer"></div>
    </div>
@endsection