@extends('layouts.master')

@section('title', 'Attach New Document')

@section('hTitle')
    Attach New Document
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New Document for <strong>{{ $item->imodel->type->name }} &bullet; {{ $item->imodel->name_number }}</strong></h3>
                </div>
                {!! Form::open(['url' => 'item/document', 'files' => true, 'id' => 'myForm']) !!}
                {!! Form::hidden('item_id', $item->id) !!}
                <div class="box-body">
                    @include('master.documents.formBody')
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">
                            <div class="form-group{{ $errors->has('input_file') ? ' has-error' : '' }}">
                                {!! Form::label('input_file', 'File') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                                {!! Form::file('input_file',['class' => 'form-control', 'id' => 'inputFile']); !!}
                                <p class="help-block">Acceptable file types: PDF, DOC, DOCX, TXT</p>
                                @if ($errors->has('input_file'))
                                    <span class="help-block"><strong>{{ $errors->first('input_file') }}</strong></span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => "items/$item->id"])
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection
