@extends('layouts.master')

@section('title', 'Attach Document')

@section('hTitle')
    Attach the Document
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Document for <strong>{{ $item->imodel->type->name }} &bullet; {{ $item->imodel->name_number }}</strong></h3>
                </div>
                {!! Form::model($item, ['method' => 'PATCH', 'action' => ['Master\Items\DocumentListController@update', $item->id], 'id' => 'myForm']) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-6 col-lg-offset-3">
                            <div class="form-group{{ $errors->has('document_id') ? ' has-error' : '' }}">
                                @foreach($filtered_documents as $id => $title)
                                <div class="checkbox">
                                    <label>{!! Form::checkbox('document_id[]', $id)!!} {{ $title }}</label>
                                </div>
                                @endforeach
                            @if ($errors->has('document_id'))
                                <span class="help-block"><strong>{{ $errors->first('document_id') }}</strong></span>
                            @endif
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                @include('parts.saveCancelBtns', ['cancel' => "items/$item->id"])
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection
