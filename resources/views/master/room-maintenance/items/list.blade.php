@if($items->count())
<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th colspan="5" class="text-center">
            cleaned with compressed air
        </th>
    </tr>
    <tr>
        <th colspan="2">#</th>
        <th>Tag ID</th>
        <th>Type / Model / Brand</th>
        <th>Serial Number</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr>
            <td>{{ Form::checkbox("item_id[]", $item->id) }}</td>
            <th scope="row">{{ $i++ }}</th>
            <td>{{ $item->inventory_tag_id }}</td>
            <td>
                <a class="btn btn-link btn-xs aBtn" href="{{ url("/items/$item->id") }}">
                    <i class="fa fa-fw fa-eye"></i>
                    <span data-toggle="tooltip" data-placement="top" title="{{ $item->imodel->type->description }}">{{ $item->imodel->type->name }}</span> / {{ $item->imodel->name_number }} / {{ $item->brand }}
                </a>
            </td>
            <td>{{ $item->serial_number }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<p class="help-block">Check off the equipment that has been cleaned with compressed air.</p>
@else
    <p class="help-block">There is no equipment.</p>
@endif