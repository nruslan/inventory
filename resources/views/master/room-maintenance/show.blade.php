@extends('layouts.master')

@section('title', 'Server / Communication Room Maintenance')

@section('hTitle')
    @include('parts.backBtn', ['url' => "room/maintenance", 'title' => 'back to the list'])
    Server / Communication Room
    <small>{{ $entry->location->location_name }}</small>
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('room', $entry) }}
@endsection

@section('content')
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $entry->location->location_name }}</h3>
        <a href="{{ url("room/maintenance/$entry->id/edit") }}" class="btn btn-primary btn-xs pull-right aBtn" data-toggle="tooltip" data-placement="top" title="Edit Details"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-lg-6">
                <p class="lead">
                    Done by {{ $entry->user->name }}<br>
                    Maintenance Date: {{ $entry->date }}<br>
                    Maintenance Time: <i class="fa fa-fw fa-clock-o"></i> {{ $entry->time }}
                </p>
                @if($entry->description)
                    <h4>Comments / Description</h4>
                    {!! $entry->description !!}
                @endif
            </div>
            <div class="col-lg-6">
                <p class="lead">List of equipment that has been cleaned with compressed air by {{ $entry->user->name }}.</p>
                <ul class="list-unstyled">
                    @foreach($entry->cleanedItems as $item)
                        <li>
                            <a class="btn btn-link btn-xs aBtn" href="{{ url("items/".$item->item->id) }}">
                                <i class="fa fa-fw fa-eye"></i>
                                {{ $item->item->inventory_tag_id }} &bull; {{ $item->type_name_brand }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div><!-- /.box-body -->
    <div class="box-footer text-right">
        @include('parts.trashBtn', ['action' => ['Master\RoomMaintenance\IndexController@destroy', $entry->id], 'btnTitle' => 'Delete the entry', 'position' => 'left'])
    </div>
</div>
@endsection


@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection