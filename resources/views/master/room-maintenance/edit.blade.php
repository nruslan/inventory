@extends('layouts.master')
@section('title', 'Edit Entry')
@section('hTitle', "Edit Entry")

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit Entry</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($entry, ['method' => 'PATCH', 'url' => "room/maintenance/$entry->id", 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-6">
                    @include("$template_path.items.list", ['items' => $items, 'i' => 1])
                </div>
                <div class="col-lg-6">
                    <!-- Textarea with ckeditor -->
                    @include('parts.textarea', ['name' => 'description', 'title' => 'Comments / Description'])
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => 'room/maintenance'])
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection
