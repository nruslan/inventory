@extends('layouts.master')

@section('title', "$location->room_name")

@section('hTitle')
    @include('parts.backBtn', ['url' => "room/maintenance", 'title' => 'back to the list'])
    {{ $location->location_name }}
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('maintenance_by_room', $location) }}
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                Total Entries <span class="badge">{{ $entries->total() }}</span>
            </h3>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date / Time</th>
                    <th>Done by</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($entries as $entry)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $entry->date }} <i class="fa fa-fw fa-clock-o"></i> {{ $entry->time }}</td>
                        <td>{{ $entry->user->name }}</td>
                        <td><a class="btn btn-default btn-xs aBtn" href="{{ url("room/maintenance/$entry->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
            {{ $entries->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection