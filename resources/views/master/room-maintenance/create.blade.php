@extends('layouts.master')
@section('title', 'Add Entry')
@section('hTitle', "Add Entry")

@section('breadcrumbs')
    <li><a href="{{ url("/items?page") }}">Items List</a></li>
    <li class="active">Add New Item</li>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add New Entry</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::open(['url' => 'room/maintenance', 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group{{ $errors->has('sublocation_id') ? ' has-error' : '' }}">
                        <strong>Room</strong> <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                        <ul class="list-unstyled">
                            @foreach($rooms as $room)
                                <li><label>{!! Form::radio("sublocation_id", $room->id, null, ['onchange' => 'checkAll(this)']); !!} {{ $room->location_name }}</label></li>
                            @endforeach
                        </ul>
                        @if ($errors->has('sublocation_id'))
                            <span class="help-block"><strong>{{ $errors->first('sublocation_id') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="col-lg-4" id="result"></div>
                <div class="col-lg-4">
                    <!-- Textarea with ckeditor -->
                    @include('parts.textarea', ['name' => 'description', 'title' => 'Comments / Description'])
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => 'room/maintenance'])
        </div>
    {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        function checkAll(e) {
            let checkbox = document.querySelectorAll("ul.list-unstyled input");
            if (e.checked) {
                axios.post('/api/get-items', {
                    id: e.value
                }).then(function (response) {
                        document.getElementById("result").innerHTML = response.data;
                        //x.value = response.data;
                        //console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

            }
        }

        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection
