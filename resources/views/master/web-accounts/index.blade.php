@extends('layouts.master')

@section('title', 'Web Accounts')

@section('hTitle', 'Web Accounts')

@section('content')
    <div class="box box-default">
        <div class="box-header">
            <h3 class="box-title">
                Accounts Total <span class="badge">{{ $accounts->total() }}</span>
                <a class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add new item" href="{{ url('/accounts/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a>
            </h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            @include('parts.session-message')
            <div class="table-responsive">
                <table class="table table-hover table-condensed" role="grid">
                <thead>
                    <tr role="row">
                        <th>#</th>
                        <th>Type</th>
                        <th>URL</th>
                        <th>Username / Email</th>
                        <th>Password</th>
                        <th>Strength</th>
                        <th colspan="2"></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($accounts as $account)
                    <tr role="row">
                        <th scope="row">{{ $i++ }}</th>
                        <td>{{ $account->name_type }}</td>
                        <td><a href="{{ $account->url }}" target="_blank">{{ $account->url }}</a></td>
                        <td><a href="{{ url("/accounts/$account->id") }}" class="btn btn-link btn-xs aBtn"><i class="fa fa-fw fa-eye"></i> {{ $account->username }}</a></td>
                        <td>{!! $account->pass_word !!}</td>
                        <td>{{ $account->strength }}</td>
                        <td class="text-center">
                            <a href="{{ url("/accounts/$account->id") }}" class="btn btn-default btn-xs aBtn"><i class="fa fa-fw fa-eye"></i></a>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-primary btn-xs aBtn" href="{{ url("/accounts/$account->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th rowspan="1" colspan="1">#</th>
                    <th rowspan="1" colspan="1">Type</th>
                    <th rowspan="1" colspan="1">URL</th>
                    <th rowspan="1" colspan="1">Username / Email</th>
                    <th rowspan="1" colspan="1">Password</th>
                    <th rowspan="1" colspan="1">Strength</th>
                    <th rowspan="1" colspan="2"></th>
                </tr>
                </tfoot>
            </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{ $accounts->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection