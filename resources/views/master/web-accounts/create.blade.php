@extends('layouts.master')

@section('title', 'Add Web Account')

@section('hTitle', 'Add New Account')
    
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add New Account</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::open(['url' => '/accounts', 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('master.web-accounts.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => "/accounts?page=$pageId"])
                </div>
                {!! Form::close() !!}
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection