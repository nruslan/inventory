@extends('layouts.master')

@section('title', 'Edit Web Account')

@section('hTitle', 'Edit Web Account')
    
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit Web Account</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::model($account ,['method' => 'PATCH', 'action' => ['Master\WebAccounts\IndexController@update', $account->id], 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('master.web-accounts.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => "/accounts/$account->id"])
                </div>
            {!! Form::close() !!}
            <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        function viewFunction() {
            var x = document.getElementById("myInput");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection