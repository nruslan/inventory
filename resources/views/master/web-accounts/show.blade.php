@extends('layouts.master')

@section('title', $account->name_type)

@section('hTitle')
    {{ $account->name_type }} Account
@endsection

@section('content')
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $account->username }}</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @include('parts.session-message')
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    {!! Form::label('access_word', 'Password') !!}
                    <div class="input-group">
                        <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-fw fa-key" aria-hidden="true"></i></span>
                        {!! Form::input('password', 'access_word', $account->pass_word, ['class' => 'form-control', 'placeholder' => 'password', 'id' => 'myInput']) !!}
                        <span class="input-group-addon" data-toggle="tooltip" data-container="body" data-placement="top" title="check to view">
                            <input type="checkbox" onclick="viewFunction()" value="{{ $account->id }}" id="aId">
                        </span>
                    </div>
                </div>
                <h4>Comments</h4>
                {!! $account->comments !!}
            </div>
            <div class="col-lg-4">
                <h4>Categories</h4>
                <table class="table table-hover" role="grid">
                    <tbody>
                    @if(count($account->categories) > 0)
                        @foreach($account->categories as $category)
                            <tr role="row">
                                <td>{{ $i++ }}</td>
                                <td>{{ $category->name }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr role="row">
                            <td>0</td>
                            <td>---</td>
                        </tr>
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <th rowspan="1" colspan="2">
                            <ul class="list-inline">
                                <li><a href="{{ url("/accounts/categories/$account->id/edit") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Edit users list"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Assign Categories</a></li>
                                @if(count($account->categories) > 0)<li>@include('parts.delBtn', ['action' => ['Master\WebAccounts\CategoriesController@destroy', $account->id], 'btnTitle' => 'categories'])</li>@endif
                            </ul>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-lg-4">
                <h4>Users <small class="badge">{{ count($account->users) }}</small></h4>
                <table class="table table-hover" role="grid">
                    <tbody>
                    @if(count($account->users) > 0)
                    @foreach($account->users as $user)
                        <tr role="row">
                            <td>{{ $i++ }}</td>
                            <td>{{ $user->name }}</td>
                        </tr>
                    @endforeach
                    @else
                        <tr role="row">
                            <td>0</td>
                            <td>---</td>
                        </tr>
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <th rowspan="1" colspan="2">
                            <ul class="list-inline">
                                <li><a href="{{ url("/accounts/user/$account->id/edit") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Edit users list"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit List</a></li>
                                @if(count($account->users) > 0)<li>@include('parts.delBtn', ['action' => ['Master\WebAccounts\UsersController@destroy', $account->id], 'btnTitle' => 'users'])</li>@endif
                            </ul>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @include('parts.trashBtn', ['action' => ['Master\WebAccounts\IndexController@destroy', $account->id], 'btnTitle' => $account->name_type])
    </div>
</div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        function viewFunction() {
            var x = document.getElementById("myInput");

            if (x.type === "password") {
                x.type = "text";
                var id = document.querySelector('#aId:checked').value;
                axios.post('/api/password', {
                    id: id
                })
                .then(function (response) {
                    x.value = response.data;
                    setTimeout(function(){
                        x.type = "password";
                        x.value = x.value.replace(/./g, "*");
                        document.getElementById('aId').checked = false;
                    }, 10000);
                })
                .catch(function (error) {
                    console.log(error);
                });

            } else {
                x.type = "password";
                x.value = x.value.replace(/./g, "*");
            }
        }

        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                // other options
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });

    </script>
@endsection