@extends('layouts.master')

@section('title', 'Assign New User')

@section('hTitle')
    Assign New User to {{ $account->username }}
@endsection

@section('content')
<div class="box box-default">
    <!-- /.box-header -->
    {!! Form::open(['url' => '/accounts/user', 'id' => 'myForm']) !!}
    <div class="box-body">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                    {!! Form::hidden("account_id", $account->id) !!}
                    @foreach($users as $id => $name)
                        <label for="">{!! Form::checkbox("user_id[$id]", $id) !!} {{ $name }}</label>
                    @endforeach

                    @if ($errors->has('user_id'))
                        <span class="help-block"><strong>{{ $errors->first('user_id') }}</strong></span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        @include('parts.saveCancelBtns', ['cancel' => "/accounts/$account->id"])
    </div>
{!! Form::close() !!}
<!-- /.box-body -->
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection










