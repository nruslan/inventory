@extends('layouts.master')

@section('title', 'Edit Users List')

@section('hTitle')
    Edit Users List
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Users List on {{ $account->username }}</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($account, ['method' => 'PATCH', 'action' => ['Master\WebAccountUserController@update', $account->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                        <div class="row">
                            <div class="col-md-4">
                                @foreach($users as $id => $name)
                                    <div class="checkbox"><label>{!! Form::checkbox("user_id[$id]", $id) !!} {{ $name }}</label></div>
                                    @if($i++ % 9 == 0)</div><div class="col-md-4">@endif
                                @endforeach
                            </div>
                        </div>
                        @if ($errors->has('user_id'))
                            <span class="help-block"><strong>{{ $errors->first('user_id') }}</strong></span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "/accounts/$account->id"])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection










