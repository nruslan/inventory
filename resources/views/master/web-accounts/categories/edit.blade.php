@extends('layouts.master')

@section('title', 'Assign Categories')

@section('hTitle')
    Assign Categories
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Assign Categories for {{ $account->username }}</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($account, ['method' => 'PATCH', 'action' => ['Master\WebAccounts\CategoriesController@update', $account->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                        <div class="row">
                            <div class="col-md-4">
                                @foreach($categories as $id => $name)
                                    <div class="checkbox"><label>{!! Form::checkbox("category_id[$id]", $id) !!} {!! $name !!}</label></div>
                                    @if($i++ % 9 == 0)</div><div class="col-md-4">@endif
                                @endforeach
                            </div>
                        </div>
                        @if ($errors->has('category_id'))
                            <span class="help-block"><strong>{{ $errors->first('category_id') }}</strong></span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "/accounts/$account->id"])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection










