@extends('layouts.master')

@section('title', 'log')

@section('hTitle')
    Log
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Log by {{ $log->user->name }} <small>{{ $log->action }}</small></h3>
            <a href="{{ route("log.edit", $log->id) }}" class="btn btn-primary btn-xs aBtn pull-right"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {!! $log->entry !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">{{ $log->date_time }}</div>
    </div>
@endsection