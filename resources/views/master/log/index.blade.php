@extends('layouts.master')

@section('title', 'Log Entries')

@section('hTitle')
    Log Entries
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">
                Log Entries <span class="badge">{{ $itemsLogs->total() }}</span>
                <a class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add Log Entry" href="{{ route('log.create') }}"><i class="fa fa-fw fa-plus"></i> Add</a>
            </h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date / Time</th>
                    <th>Action</th>
                    <th>Item ID</th>
                    <th>Type</th>
                    <th>Location</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($itemsLogs as $log)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $log->date_time }}</td>
                        <td>{{ $log->action }}</td>
                        <td>{{ $log->item_tag_id }}</td>
                        <td><a class="btn btn-link btn-xs" href="{{ route('log.show', $log->id) }}" data-toggle="tooltip" data-placement="top" title="view details">{{ $log->item_type_name }}</a></td>
                        <td>{!! $log->items_location !!}</td>
                        <th><a class="btn btn-default btn-xs aBtn" href="{{ route('log.show', $log->id) }}" data-toggle="tooltip" data-placement="top" title="view details"><i class="fa fa-eye fa-fw"></i></a></th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{ $itemsLogs->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection