@extends('layouts.master')

@section('title', 'Edit Log Entry')

@section('hTitle')
    Log Entry
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit Log Entry</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::model($log ,['method' => 'PATCH', 'url' => route('log.update', $log->id), 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('master.log.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => "log/$log->id"])
                </div>
                {!! Form::close() !!}
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection
