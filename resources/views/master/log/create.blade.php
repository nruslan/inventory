@extends('layouts.master')

@section('title', 'Log Entry')

@section('hTitle')
    Log Entry
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">New Log Entry</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::open(['url' => route('log.store'), 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('master.log.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => "log"])
                </div>
                {!! Form::close() !!}
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection
