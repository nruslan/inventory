@extends('layouts.master')

@section('title', 'New Employee')

@section('hTitle')
    Edit Employee's info
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/employees?page=$pageId") }}">Employees</a></li>
    <li><a href="{{ url("/employees/$employee->id") }}">{{ $employee->full_employee_name }}</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
    <div class="box box-default">
        <!-- /.box-header -->
        {!! Form::model($employee, ['method' => 'PATCH' ,'action' => ['Master\Employees\IndexController@update', $employee->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    @include('master.employees.formBody')
                    <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                        {!! Form::label('end_date', 'End Date') !!}
                        <div class="input-group date" id="datepicker1">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Item purchase date"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                            {!! Form::text('end_date', null, ['class' => 'form-control', 'placeholder' => 'end date', 'autocomplete' => 'off']) !!}
                        </div>
                        @if ($errors->has('end_date'))
                            <span class="help-block"><strong>{{ $errors->first('end_date') }}</strong></span>
                        @endif
                    </div>
                    <!-- Textarea with ckeditor -->
                    @include('parts.textarea', ['name' => 'comments', 'title' => 'Comments'])
                </div>
            </div>
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "employees/$employee->id"])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({
                format: 'MM/DD/YYYY'
            });

            $("#datepicker1").datetimepicker({
                format: 'MM/DD/YYYY'
            });

            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection