@extends('layouts.master')

@section('title', 'Employees List')

@section('hTitle')
    Employees
@endsection

@section('breadcrumbs')
    <li class="active">SB2 Employees</li>
@endsection

@section('content')
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">
            Total <span class="badge">{{ $employees->total() }}</span>
            <a class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add new employee" href="{{ url('employees/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a>
        </h3>
        <div class="box-tools">
            {{ $employees->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        @include('parts.session-message')
        <div class="table-responsive">
            <table class="table table-condensed table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Employee Name</th>
                    <th>Employee Number</th>
                    <th>Badge Number</th>
                    <th colspan="2"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <th scope="row">{{ $i++ }}</th>
                        <td>{{ $employee->full_employee_name }}</td>
                        <td>{{ $employee->employee_number }}</td>
                        <td>{!! $employee->active_proximity_card_badge !!}</td>
                        <td><a class="btn btn-default btn-xs aBtn" href="{{ url("/employees/$employee->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                        <td><a class="btn btn-primary btn-xs aBtn" href="{{ url("/employees/$employee->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        {{ $employees->links('vendor.pagination.admin-lte') }}
    </div>
</div>
@endsection