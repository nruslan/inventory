@extends('layouts.master')

@section('title', $employee->full_employee_name)

@section('hTitle')
    @include('parts.backBtn', ['url' => "/employees?page=$pageId", 'title' => 'back to the list'])
    Employee Information
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/employees?page=$pageId") }}">Employees</a></li>
    <li class="active">{{ $employee->full_employee_name }}</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active">
                    <h3 class="widget-user-username">{{ $employee->full_employee_name }}</h3>
                    <h5 class="widget-user-desc">#{{ $employee->employee_number }}</h5>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle" src="{{ asset("/storage/default.jpg") }}" alt="User Avatar">
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">Since</h5>
                                <span class="description-text">{{ $employee->since_date }}</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">13,000</h5>
                                <span class="description-text">FOLLOWERS</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header">35</h5>
                                <span class="description-text">PRODUCTS</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="box box-default">
                <div class="box-header with-border">
                    <a href="{{ url("/employees/$employee->id/edit") }}" class="btn btn-primary btn-xs aBtn pull-right" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit</a></td>
                    <h3 class="box-title">{{ $employee->full_employee_name }} #{{ $employee->employee_number }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @include('parts.session-message')
                    Proximity Card Info:
                    <ul>
                        <li>Badge: {!! $employee->active_proximity_card_badge !!}</li>
                        <li>Card: {!! $employee->active_proximity_card_number !!}</li>
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <ul class="list-unstyled">
                        <li></li>
                        <li class="pull-right">@include('parts.trashBtn', ['action' => ['Master\Employees\IndexController@destroy', $employee->id], 'btnTitle' => "Delete $employee->full_employee_name", 'position' => 'left', 'class' => 'btn-sm'])</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection