@extends('layouts.master')

@section('title', 'New Employee')

@section('hTitle')
    Add New Employee
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/employees?page=$pageId") }}">Employees</a></li>
    <li class="active">Add New Employee</li>
@endsection

@section('content')
<div class="box box-default">
    <!-- /.box-header -->
    {!! Form::open(['url' => 'employees', 'id' => 'myForm']) !!}
    <div class="box-body">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                @include('master.employees.formBody')
            </div>
        </div>
    </div>
    <div class="box-footer">
        @include('parts.saveCancelBtns', ['cancel' => "employees?page=$pageId"])
    </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
</div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({
                format: 'MM/DD/YYYY'
            });

            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection