<div class="form-group{{ $errors->has('employee_number') ? ' has-error' : '' }}">
    {!! Form::label('employee_number', 'Employee Number') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
    <div class="input-group">
        <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-hashtag fa-fw" aria-hidden="true"></i></span>
        {!! Form::text('employee_number', null, ['class' => 'form-control', 'placeholder' => 'employee number', 'autocomplete' => 'off', 'autofocus']) !!}
    </div>
    @if ($errors->has('employee_number'))
        <span class="help-block"><strong>{{ $errors->first('employee_number') }}</strong></span>
    @endif
</div>
<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
    {!! Form::label('first_name', 'First Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
    <div class="input-group">
        <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i></span>
        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'first name', 'autocomplete' => 'off']) !!}
    </div>
    @if ($errors->has('first_name'))
        <span class="help-block"><strong>{{ $errors->first('first_name') }}</strong></span>
    @endif
</div>
<div class="form-group{{ $errors->has('middle_name') ? ' has-error' : '' }}">
    {!! Form::label('middle_name', 'Middle Name') !!}
    <div class="input-group">
        <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i></span>
        {!! Form::text('middle_name', null, ['class' => 'form-control', 'placeholder' => 'middle name', 'autocomplete' => 'off']) !!}
    </div>
    @if ($errors->has('middle_name'))
        <span class="help-block"><strong>{{ $errors->first('middle_name') }}</strong></span>
    @endif
</div>
<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
    {!! Form::label('last_name', 'Last Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
    <div class="input-group">
        <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i></span>
        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'last name', 'autocomplete' => 'off']) !!}
    </div>
    @if ($errors->has('last_name'))
        <span class="help-block"><strong>{{ $errors->first('last_name') }}</strong></span>
    @endif
</div>
<div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
    {!! Form::label('start_date', 'Start Date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
    <div class="input-group date" id="datepicker">
        <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Item purchase date"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
        {!! Form::text('start_date', null, ['class' => 'form-control', 'placeholder' => 'start date', 'autocomplete' => 'off']) !!}
    </div>
    @if ($errors->has('start_date'))
        <span class="help-block"><strong>{{ $errors->first('start_date') }}</strong></span>
    @endif
</div>