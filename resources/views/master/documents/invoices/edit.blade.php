@extends('layouts.master')

@section('title', $invoice->title)

@section('hTitle')
    Edit {{ $invoice->title }}
@endsection

@section('breadcrumbs')@endsection


@section('content')
    <div class="box">
        <!-- /.box-header -->
        {!! Form::model($invoice, ['method' => 'PATCH', 'action' => ['Master\Documents\InvoiceController@update', $invoice->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('master.documents.invoices.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "documents/invoices/$invoice->id"])
        </div>
    {!! Form::close() !!}<!-- /.box-body -->
    </div>
@endsection


@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({
                format: 'MM/DD/YYYY'
            });
            $("#datetimepicker").datetimepicker();
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection
