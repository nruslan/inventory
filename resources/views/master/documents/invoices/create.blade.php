@extends('layouts.master')

@section('title', 'Add Invoice')

@section('hTitle')
    Upload a new Invoice file
@endsection

@section('breadcrumbs'){{ Breadcrumbs::render('new_invoice') }}@endsection

@section('content')
    <div class="box">
        <!-- /.box-header -->
        {!! Form::open(['url' => 'documents/invoices', 'files' => true, 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('master.documents.invoices.formBody')
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                        {!! Form::label('input_file', 'File') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                        <div class="input-group">
                            <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="file"><i class="fa fa-file fa-fw" aria-hidden="true"></i></span>
                            {!! Form::file('input_file',['class' => 'form-control', 'id' => 'inputFile']); !!}
                        </div>
                        <p class="help-block">Acceptable file types: PDF, DOC, DOCX, TXT</p>
                        @if($errors->has('input_file'))<span class="help-block"><strong>{{ $errors->first('input_file') }}</strong></span>@endif
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "documents?page="])
        </div>
        {!! Form::close() !!}<!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#datepicker").datetimepicker({ format: 'MM/DD/YYYY' });
            $("#datetimepicker").datetimepicker();

            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection
