<div class="row">
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('tax_code_id') ? ' has-error' : '' }}">
            {!! Form::label('tax_code_id', 'Tax Code') !!}
            {!! Form::select('tax_code_id', $taxcodes, null, ['class' => 'form-control', 'placeholder' => 'select tax code', 'autofocus']) !!}
            @if ($errors->has('tax_code_id'))
                <span class="help-block"><strong>{{ $errors->first('tax_code_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('vendor_id') ? ' has-error' : '' }}">
            {!! Form::label('vendor_id', 'Vendor') !!}
            {!! Form::select('vendor_id', $vendors, null, ['class' => 'form-control', 'placeholder' => 'choose a vendor', 'autofocus']) !!}
            @if ($errors->has('vendor_id'))<span class="help-block"><strong>{{ $errors->first('vendor_id') }}</strong></span>@endif
        </div>
        <div class="form-group{{ $errors->has('invoice_number') ? ' has-error' : '' }}">
            {!! Form::label('invoice_number', 'Invoice Number') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="amount"><i class="fa fa-hashtag fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('invoice_number', null, ['class' => 'form-control', 'placeholder' => 'invoice number', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('invoice_number'))
                <span class="help-block"><strong>{{ $errors->first('invoice_number') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
            {!! Form::label('amount', 'Amount') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="amount"><i class="fa fa-usd fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'invoice amount', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('amount'))
                <span class="help-block"><strong>{{ $errors->first('amount') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('due') ? ' has-error' : '' }}">
            {!! Form::label('due', 'Invoice Due Date') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group date" id="datepicker">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="invoice due date"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('due', null, ['class' => 'form-control', 'placeholder' => 'due date', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('due'))<span class="help-block"><strong>{{ $errors->first('due') }}</strong></span>@endif
        </div>
        <div class="form-group{{ $errors->has('submitted_at') ? ' has-error' : '' }}">
            {!! Form::label('submitted_at', 'Invoice Submitted Date / Time') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="required field"></i>
            <div class="input-group date" id="datetimepicker">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="invoice submitted date and time"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('submitted_at', null, ['class' => 'form-control', 'placeholder' => 'invoice submitted date and time', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('submitted_at'))<span class="help-block"><strong>{{ $errors->first('submitted_at') }}</strong></span>@endif
        </div>
    </div>
</div>
