@extends('layouts.master')

@section('title', 'Invoices')

@section('hTitle', 'Invoices')
    
@section('breadcrumbs'){{ Breadcrumbs::render('invoices') }}@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Invoices Total <span class="badge">{{ $invoices->total() }}</span>
                <a class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add new document" href="{{ url('documents/invoices/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a>
            </h3>
            <div class="box-tools">{{ $invoices->links('vendor.pagination.admin-lte') }}</div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            @include('parts.session-message')
            {!! Form::open(['method' => 'GET', 'url' => 'documents/invoices', 'class' => 'form-inline', 'id' => 'orderBy']) !!}
                <div class="form-group">
                    {!! Form::label('invoices_for', 'Invoices for') !!}
                    {!! Form::select('invoices_for', $years, $theYear, ['class' => 'form-control input-sm', 'onchange' => 'this.form.submit()']) !!}
                </div>
            {!! Form::close() !!}

            <div class="table-responsive">
                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Invoice #</th>
                        <th>Amount</th>
                        <th>Tax Code</th>
                        <th>Vendor</th>
                        <th>Submited</th>
                        <th>Uploaded by</th>
                        <th colspan="2"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($invoices as $invoice)
                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td><a class="btn btn-link btn-xs" href="{{ url("documents/invoices/$invoice->id") }}">{{ $invoice->invoice_number ?? '---' }}</a></td>
                            <td>{{ $invoice->amount ?? '0.00' }}</td>
                            <td>{{ $invoice->tax_code_dep }}</td>
                            <td>{{ $invoice->vendors_name }}</td>
                            <td>{{ $invoice->submitted_date_time }}</td>
                            <td>{{ $invoice->name }}</td>
                            <td>
                                <a class="btn btn-default btn-xs" download="{{ $invoice->filename }}" href="{{ asset("storage/documents/$invoice->year/$invoice->filename") }}" data-toggle="tooltip" data-placement="top" title="download the file"><i class="fa fa-fw fa-download"></i></a>
                                <a class="btn btn-default btn-xs" href="{{ asset("storage/documents/$invoice->year/$invoice->filename") }}" target="_blank" data-toggle="tooltip" data-placement="top" title="view in browser"><i class="fa fa-fw fa-eye"></i></a>
                            </td>
                            <td><a class="btn btn-primary btn-xs aBtn" href="{{ url("documents/invoices/$invoice->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Amount total: ${{ $totalAmount }}
            {{ $invoices->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection
