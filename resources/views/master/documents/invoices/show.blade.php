@extends('layouts.master')

@section('title', "Invoice #")
@section('hTitle')
    Invoice # {{ $invoice->invoice_number }}
@endsection

@section('breadcrumbs'){{ Breadcrumbs::render('invoice', $invoice) }}@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Invoice from {{ $invoice->vendors_name }}
                <a class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="edit" href="{{ url("documents/invoices/$invoice->id/edit") }}"><i class="fa fa-fw fa-pencil"></i> Edit</a>
            </h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! $invoice->description !!}
            <iframe src="{{ asset("storage/documents/$invoice->year/$invoice->filename") }}" width="100%" height="750px" frameborder="0"></iframe>
        </div>
    </div>
@endsection
