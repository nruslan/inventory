@extends('layouts.master')

@section('title', $document->title)

@section('hTitle')
    Edit {{ $document->title }}
@endsection

@section('content')
    <div class="box">
        <!-- /.box-header -->
        {!! Form::model($document, ['method' => 'PATCH', 'action' => ['Master\DocumentsController@update', $document->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('master.documents.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "documents?page=$pageId"])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection


@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection