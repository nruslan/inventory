<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="form-group{{ $errors->has('doctype_id') ? ' has-error' : '' }}">
            {!! Form::label('doctype_id', 'Type') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('doctype_id', $doctypes, null, ['class' => 'form-control', 'placeholder' => 'select type', 'autofocus']) !!}
            @if ($errors->has('doctype_id'))
                <span class="help-block"><strong>{{ $errors->first('doctype_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title', 'Title') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-hashtag fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'document title', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('title'))
                <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <!-- Textarea with ckeditor -->
        @include('parts.textarea', ['name' => 'description', 'title' => 'Description'])
    </div>
</div>
