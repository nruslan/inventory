@extends('layouts.master')

@section('title', 'Documents')

@section('hTitle', 'Documents')
    
@section('breadcrumbs'){{ Breadcrumbs::render('documents') }}@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Documents Total <span class="badge">{{ $documents->total() }}</span>
                <a class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add new document" href="{{ url('documents/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a>
            </h3>
            <div class="box-tools">
                {{ $documents->links('vendor.pagination.admin-lte') }}
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            @include('parts.session-message')
            <div class="table-responsive">
                <table class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Type</th>
                        <th>Title</th>
                        <th>Uploaded by</th>
                        <th>File Size</th>
                        <th>Date</th>
                        <th colspan="2"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($documents as $document)
                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $document->type->name }}</td>
                            <td>{{ $document->title }}</td>
                            <td>{{ $document->user->name }}</td>
                            <td>{{ $document->file_size }}</td>
                            <td>{{ $document->date }}</td>
                            <td>
                                <a class="btn btn-default btn-xs" download="{{ $document->filename }}" href="{{ asset("storage/documents/$document->year/$document->filename") }}" data-toggle="tooltip" data-placement="top" title="download the file"><i class="fa fa-fw fa-download"></i></a>
                                <a class="btn btn-default btn-xs" href="{{ asset("storage/documents/$document->year/$document->filename") }}" target="_blank" data-toggle="tooltip" data-placement="top" title="view in browser"><i class="fa fa-fw fa-eye"></i></a>
                            </td>
                            <td><a class="btn btn-primary btn-xs aBtn" href="{{ url("/documents/$document->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{ $documents->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection
