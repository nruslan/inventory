@extends('layouts.master')

@section('title', 'Add Document')

@section('hTitle', 'Upload new Document file')
    
@section('content')
    <div class="box">
        <!-- /.box-header -->
        {!! Form::open(['url' => 'documents', 'files' => true, 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('master.documents.formBody')
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                        {!! Form::label('input_file', 'File') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
                        {!! Form::file('input_file',['class' => 'form-control', 'id' => 'inputFile']); !!}
                        <p class="help-block">Acceptable file types: PDF, DOC, DOCX, TXT</p>
                        @if ($errors->has('input_file'))
                            <span class="help-block"><strong>{{ $errors->first('input_file') }}</strong></span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "documents?page=$pageId"])
        </div>
        {!! Form::close() !!}
        <!-- /.box-body -->
    </div>
@endsection


@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection