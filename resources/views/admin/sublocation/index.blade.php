@extends('layouts.admin')

@section('title', 'Sublocations')

@section('hTitle')
    Sublocations
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Sublocations List <a class="btn btn-primary btn-xs aBtn" href="{{ url('admin/sublocations/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a></h3>

                    <div class="box-tools">
                        {{ $sublocations->links('vendor.pagination.admin-lte') }}
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    @include('parts.session-message')
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Main</th>
                            <th>Sublocation Name</th>
                            <th class="text-center">Sublocation Code</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sublocations as $sublocation)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $sublocation->location->name }}</td>
                                <td>{{ $sublocation->room_name }}</td>
                                <td class="text-center">{{ $sublocation->room_code }}</td>
                                <td>
                                    <a class="btn btn-default btn-xs aBtn" href="{{ url("admin/sublocations/$sublocation->id") }}"><i class="fa fa-fw fa-eye"></i></a>
                                    <a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/sublocations/$sublocation->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $sublocations->links('vendor.pagination.admin-lte') }}
                </div>
            </div>
        </div>
    </div>
@endsection
