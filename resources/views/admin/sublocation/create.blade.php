@extends('layouts.admin')

@section('title', 'Sublocation')

@section('hTitle')
    Add New Sublocation
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add New Sublocation</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::open(['url' => 'admin/sublocations', 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('admin.sublocation.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => 'admin/sublocations'])
                </div>
            {!! Form::close() !!}
            <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection