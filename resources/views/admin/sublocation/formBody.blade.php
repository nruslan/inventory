<div class="row">
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
            {!! Form::label('location_id', 'Main Location') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('location_id', $locations, null, ['class' => 'form-control', 'placeholder' => 'select main location', 'autofocus']) !!}
            @if ($errors->has('location_id'))
                <span class="help-block"><strong>{{ $errors->first('location_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('room_code') ? ' has-error' : '' }}">
            {!! Form::label('room_code', 'Room Code') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-tag fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('room_code', null, ['class' => 'form-control', 'placeholder' => 'room code', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('room_code'))
                <span class="help-block"><strong>{{ $errors->first('room_code') }}</strong></span>
            @endif
        </div>
        <!-- Textarea with ckeditor -->
        @include('parts.textarea', ['name' => 'description', 'title' => 'Description'])
    </div>
</div>