@extends('layouts.admin')

@section('title', 'Edit Sublocation')

@section('hTitle')
    Edit Sublocation
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit Sublocation</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::model($sublocation, ['method' => 'PATCH', 'action' => ['Admin\SublocationController@update', $sublocation->id], 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('admin.sublocation.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => "admin/sublocations/$sublocation->id"])
                </div>
            {!! Form::close() !!}
            <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection