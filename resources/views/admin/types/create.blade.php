@extends('layouts.admin')

@section('title', 'Types')

@section('hTitle')
    Add New Type
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add New Type</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::open(['url' => 'admin/types', 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('admin.types.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => 'admin/types'])
                </div>
            {!! Form::close() !!}
            <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection