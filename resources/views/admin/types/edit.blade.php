@extends('layouts.admin')

@section('title', 'Types')

@section('hTitle')
    Edit Type
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit Type</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::model($type, ['method' => 'PATCH', 'action' => ['Admin\TypesController@update', $type->id], 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('admin.types.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => 'admin/types'])
                </div>
            {!! Form::close() !!}
            <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection