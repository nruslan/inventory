@extends('layouts.admin')

@section('hTitle')
    Types
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Types List <a class="btn btn-primary btn-xs aBtn" href="{{ url('admin/types/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a></h3>

                    <div class="box-tools">
                        {{ $types->links('vendor.pagination.admin-lte') }}
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    @include('parts.session-message')
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th colspan="2"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($types as $type)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $type->name }}</td>
                                <td><a class="btn btn-default btn-xs aBtn" href="{{ url("admin/types/$type->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                                <td><a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/types/$type->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $types->links('vendor.pagination.admin-lte') }}
                </div>
            </div>
        </div>
    </div>
@endsection
