@extends('layouts.admin')

@section('title', 'Add group')

@section('hTitle')
    Add New Group
@endsection

@section('breadcrumbs')
    <li class="active">Add Groups</li>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add New Group</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::open(['url' => 'admin/groups', 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.groups.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "/admin/groups?page=$pageId"])
        </div>
        {!! Form::close() !!}
        <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection