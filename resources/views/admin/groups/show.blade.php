@extends('layouts.admin')

@section('title', $group->name)

@section('hTitle')
    @include('parts.backBtn', ['url' => "/admin/groups?page=$pageId", 'title' => 'back to the list'])
    {{ $group->name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/groups?page=$pageId") }}">Groups</a></li>
    <li class="active">{{ $group->name }} Group</li>
@endsection

@section('content')

@endsection