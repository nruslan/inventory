@extends('layouts.admin')

@section('title', "Edit $group->name group")

@section('hTitle')
    Edit {{ $group->name }} group
@endsection

@section('breadcrumbs')
    <li class="active">Add Groups</li>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit {{ $group->name }} group details</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($group, ['method' => 'PATCH', 'action' => ['Admin\GroupsController@update', $group->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.groups.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "/admin/groups/$group->id"])
        </div>
        {!! Form::close() !!}
        <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection