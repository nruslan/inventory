@extends('layouts.admin')

@section('title', 'Brands')

@section('hTitle')
    Brands
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Brands List <a class="btn btn-primary btn-xs aBtn" href="{{ url('admin/brands/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a></h3>
                    <div class="box-tools">
                        {{ $brands->links('vendor.pagination.admin-lte') }}
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    @include('parts.session-message')
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th colspan="2"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($brands as $brand)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td><a class="btn btn-link btn-xs aBtn" href="{{ url("admin/brands/$brand->id") }}"><i class="fa fa-fw fa-eye"></i> {{ $brand->name }}</a></td>
                                <td><a class="btn btn-default btn-xs aBtn" href="{{ url("admin/brands/$brand->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                                <td class="text-right"><a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/brands/$brand->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    {{ $brands->links('vendor.pagination.admin-lte') }}
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection
