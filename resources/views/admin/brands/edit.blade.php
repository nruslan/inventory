@extends('layouts.admin')

@section('title', 'Brands')

@section('hTitle')
    Edit Brand: {{ $brand->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit Brand</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::model($brand, ['method' => 'PATCH', 'action' => ['Admin\BrandsController@update', $brand->id], 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('admin.brands.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => 'admin/brands'])
                </div>
                {!! Form::close() !!}
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection