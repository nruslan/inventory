@extends('layouts.admin')

@section('title', 'Brands')

@section('hTitle')
    Add New Brand
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add New Brand</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::open(['url' => 'admin/brands', 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.brands.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => 'admin/brands'])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection