@extends('layouts.admin')

@section('title', "$brand->name")

@section('hTitle')
    @include('parts.backBtn', ['url' => "admin/brands?page=$pageId", 'title' => 'back to brands list'])
    Brand {{ $brand->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{ $brand->name }}</h3>
                    <a href="{{ url("admin/brands/$brand->id/edit") }}" class="btn btn-primary pull-right aBtn"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit</a>
                </div><!-- /.box-header -->

                <div class="box-body">
                    @include('parts.session-message')
                    <div class="row">
                        <div class="col-lg-6">

                        </div>
                        <div class="col-lg-6">

                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    @include('parts.trashBtn', ['action' => ['Admin\BrandsController@destroy', $brand->id], 'btnTitle' => $brand->name])
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                // other options
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection