@extends('layouts.admin')

@section('title', "$user->name")

@section('hTitle')
    @include('parts.backBtn', ['url' => "admin/users?page=$pageId", 'title' => 'back to users list'])
    User: {{ $user->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{ $user->name }} <small>{{ $user->all_departments }}</small></h3>
                    <a href="{{ url("admin/users/$user->id/edit") }}" class="btn btn-primary pull-right aBtn"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit</a>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4>Privileges</h4>
                            <p>{!! $user->user_privileges_list !!}</p>
                            <ul class="list-inline">
                                @if($user->user_privileges_array)<li>@include('parts.trashBtn', ['action' => ['Admin\User\PrivilegesController@destroy', $user->id], 'class' => 'btn-xs', 'name' => 'Remove', 'btnTitle' => 'Remove All Privileges'])</li>@endif
                                <li><a href="{{ url("admin/users/privileges/$user->id/edit") }}" class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Edit privileges"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> edit</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-6">

                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer text-right">
                    @include('parts.trashBtn', ['action' => ['Admin\User\IndexController@destroy', $user->id], 'btnTitle' => $user->name])
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                // other options
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection