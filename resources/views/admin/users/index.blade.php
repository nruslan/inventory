@extends('layouts.admin')

@section('title', 'Users')

@section('hTitle')
    Users List
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Users List <span class="badge">{{ $users->total() }}</span> <a class="btn btn-primary btn-xs aBtn" href="{{ url('admin/users/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a></h3>

                    <div class="box-tools">
                        {{ $users->links('vendor.pagination.admin-lte') }}
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding table-responsive">
                    @include('parts.session-message')
                    <table class="table ttable-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Privileges</th>
                            <th colspan="2"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td><a class="btn btn-link btn-xs aBtn" href="{{ url("admin/users/$user->id") }}"><i class="fa fa-fw fa-eye"></i> {{ $user->name }}</a></td>
                                <td>{{ $user->email }}</td>
                                <td></td>
                                <td><a class="btn btn-default btn-xs aBtn" href="{{ url("admin/users/$user->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                                <td><a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/users/$user->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $users->links('vendor.pagination.admin-lte') }}
                </div>
            </div>
        </div>
    </div>
@endsection
