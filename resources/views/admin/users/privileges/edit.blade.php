@extends('layouts.admin')

@section('title', "Edit User's Privileges")

@section('hTitle')
    Edit User's Privileges
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit {{ $user->name }}'s Privileges</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::model($user, ['method' => 'PATCH', 'action' => ['Admin\User\PrivilegesController@update', $user->id], 'id' => 'myForm']) !!}
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('route_name_id', 'Privileges') !!}
                        @foreach($routes as $route)
                            <div class="checkbox">
                                <label>
                                    {{ $route->title }}
                                </label>
                            </div>
                            @foreach($route->actions as $action)
                                <label class="checkbox-inline">
                                    @if(isset($assigned_privileges[$route->id]) and in_array($action->id ,$assigned_privileges[$route->id]))
                                        {!! Form::checkbox("action_name_id[$route->id][$action->id]", $action->id, true) !!} {{ $action->name }}
                                    @else
                                        {!! Form::checkbox("action_name_id[$route->id][$action->id]", $action->id) !!} {{ $action->name }}
                                    @endif
                                </label>
                            @endforeach
                            <hr>
                        @endforeach
                    </div>
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => "admin/users/$user->id"])
                </div>
            {!! Form::close() !!}
            <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection