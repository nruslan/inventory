@extends('layouts.admin')

@section('title', 'New User')

@section('hTitle')
    Add New User
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add New User</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::open(['url' => 'admin/users', 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('admin.users.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => 'admin/users'])
                </div>
            {!! Form::close() !!}
            <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection