<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name', 'Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('name'))
                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            {!! Form::label('email', 'Email') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-envelope fa-fw" aria-hidden="true"></i></span>
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'email address', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('email'))
                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('department_id') ? ' has-error' : '' }}">
            <strong>Departments</strong> <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <ul class="list-unstyled">
                @foreach($departments as $id => $name)
                    <li><label>{!! Form::checkbox("department_id[$id]", $id); !!} {{ $name }}</label></li>
                @endforeach
            </ul>
            @if ($errors->has('department_id'))
                <span class="help-block"><strong>{{ $errors->first('department_id') }}</strong></span>
            @endif
        </div>
    </div>
</div>