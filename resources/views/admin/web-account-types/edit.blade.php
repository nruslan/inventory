@extends('layouts.admin')

@section('title', 'Edit Web Account Type')

@section('hTitle')
    Edit Web Account Type
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit {{ $type->name }} Type</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::model($type, ['method' => 'PATCH', 'action' => ['Admin\WebAccountTypesController@update', $type->id], 'id' => 'myForm']) !!}
                <div class="box-body">
                    @include('admin.web-account-types.formBody')
                </div>
                <div class="box-footer">
                    @include('parts.saveCancelBtns', ['cancel' => "admin/web-account-types/$type->id"])
                </div>
            {!! Form::close() !!}
            <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i> processing...');
            });
        });
    </script>
@endsection