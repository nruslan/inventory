<div class="row">
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name', 'Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-tag fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('name'))
                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('icon_id') ? ' has-error' : '' }}">
            <strong>Icons</strong> <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <ul class="list-unstyled">
                @foreach($icons as $id => $name)
                    <li><label>{!! Form::radio("icon_id", $id); !!} {!! $name !!}</label></li>
                @endforeach
            </ul>
            @if ($errors->has('icon_id'))
                <span class="help-block"><strong>{{ $errors->first('icon_id') }}</strong></span>
            @endif
        </div>
        <!-- Textarea with ckeditor -->
        @include('parts.textarea', ['name' => 'description', 'title' => 'Description'])
    </div>
</div>