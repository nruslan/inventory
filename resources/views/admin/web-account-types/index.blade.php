@extends('layouts.admin')

@section('hTitle')
    Web Account Types
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Account Types List <span class="badge">{{ $types->total() }}</span> <a class="btn btn-primary btn-xs aBtn" href="{{ url('admin/web-account-types/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a></h3>

                    <div class="box-tools">
                        {{ $types->links() }}
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    @include('parts.session-message')
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-center">Icon</th>
                            <th>Name</th>
                            <th>Url</th>
                            <th colspan="3"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($types as $type)
                            <tr>
                                <th scope="row">{{ $i++ }}</th>
                                <td class="text-center">{!! $type->html_icon !!}</td>
                                <td>{{ $type->name }}</td>
                                <td>{{ $type->url }}</td>
                                <td class="text-center"><a class="btn btn-default btn-xs aBtn" href="{{ url("admin/web-account-types/$type->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                                <td class="text-center"><a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/web-account-types/$type->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                                <td class="text-right">@include('parts.trashBtnSm', ['action' => ['Admin\WebAccountTypesController@destroy', $type->id], 'btnTitle' => "$type->name type"])</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    @include('parts.session-message')
                    {{ $types->links('vendor.pagination.admin-lte') }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection
