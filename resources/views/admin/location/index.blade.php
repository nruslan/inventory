@extends('layouts.admin')

@section('title', 'Main locations')

@section('hTitle')
    Main locations
@endsection

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Main locations List <a class="btn btn-primary btn-xs aBtn" href="{{ url('admin/sublocations/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a></h3>
        <div class="box-tools"></div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Location Name</th>
                <th>Sublocation</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($locations as $location)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $location->name }}</td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer"></div>
</div>
@endsection
