@extends('layouts.admin')

@section('title', "$icon->name Icon")

@section('hTitle')
    Edit Icon's Details
@endsection

@section('breadcrumbs')
    <li class="active">Edit {{ $icon->name }} Icon</li>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ $icon->name }} Icon</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($icon, ['method' => 'PATCH', 'action' => ['Admin\IconsController@update', $icon->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.icons.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "/admin/icons/$icon->id"])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection