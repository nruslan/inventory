@extends('layouts.admin')

@section('title', $icon->name)

@section('hTitle')
    @include('parts.backBtn', ['url' => "/admin/icons?page=$pageId", 'title' => 'back to the list'])
    {{ $icon->name }}
@endsection

@section('breadcrumbs')

@endsection

@section('content')
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $icon->name }} Icon {!! $icon->icon !!}</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @include('parts.session-message')
    </div>
    <!-- /.box-body -->
    <div class="box-footer"></div>
</div>
@endsection