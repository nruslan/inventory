@extends('layouts.admin')

@section('title', 'Icons')

@section('hTitle')
    Icons
@endsection

@section('breadcrumbs')
    <li class="active">Icons</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Icons list <span class="badge">{{ $icons->total() }}</span></h3>
            <a class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add new Group" href="{{ url('/admin/icons/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="table-responsive">
                <table class="table table-hover table-condensed" role="grid">
                    <thead>
                    <tr role="row">
                        <th>#</th>
                        <th>Name</th>
                        <th class="text-center">Icon</th>
                        <th colspan="3"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($icons as $icon)
                        <tr role="row">
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $icon->name }}</td>
                            <td class="text-center">{!! $icon->icon !!}</td>
                            <td class="text-center"><a class="btn btn-default btn-xs aBtn" href="{{ url("admin/icons/$icon->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                            <td class="text-center"><a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/icons/$icon->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                            <td class="text-center">@include('parts.trashBtnSm', ['action' => ['Admin\IconsController@destroy', $icon->id], 'btnTitle' => "$icon->name icon"])</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @include('parts.session-message')
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{ $icons->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection