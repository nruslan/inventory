@extends('layouts.admin')

@section('title', 'Add New Icon')

@section('hTitle')
    Add New Icon
@endsection

@section('breadcrumbs')
    <li class="active">Add Icon</li>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add New Icon</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::open(['url' => 'admin/icons', 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.icons.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "/admin/icons?page=$pageId"])
        </div>
        {!! Form::close() !!}
        <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection