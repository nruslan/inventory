<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name', 'Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Icon's name"><i class="fa fa-tag fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('name'))
                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
            {!! Form::label('icon', 'FA Icon') !!}
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="HTML markup"><i class="fa fa-image fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('icon', null, ['class' => 'form-control', 'placeholder' => 'html icon', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('icon'))
                <span class="help-block"><strong>{{ $errors->first('icon') }}</strong></span>
            @endif
        </div>
        <!-- Textarea with ckeditor -->
        @include('parts.textarea', ['name' => 'description', 'title' => 'Description'])
    </div>
</div>