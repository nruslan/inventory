@extends('layouts.admin')

@section('title', 'Add category')

@section('hTitle')
    Add New Category
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/categories?page=$pageId") }}">Categories</a></li>
    <li class="active">Add</li>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add New Category</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::open(['url' => 'admin/categories', 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.categories.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "/admin/categories?page=$pageId"])
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection