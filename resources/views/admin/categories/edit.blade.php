@extends('layouts.admin')

@section('title', 'Edit category')

@section('hTitle')
    Edit {{ $category->name }} Category
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/categories?page=$pageId") }}">Categories</a></li>
    <li><a href="{{ url("/admin/categories/$category->id") }}">{{ $category->name }}</a></li>
    <li class="active">Edit</li>
@endsection

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit {{ $category->name }} Category</h3>
        </div>
        <!-- /.box-header -->
        {!! Form::model($category, ['method' => 'PATCH', 'action' => ['Admin\CategoriesController@update', $category->id], 'id' => 'myForm']) !!}
        <div class="box-body">
            @include('admin.categories.formBody')
        </div>
        <div class="box-footer">
            @include('parts.saveCancelBtns', ['cancel' => "/admin/categories?page=$pageId"])
        </div>
        {!! Form::close() !!}
        <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection