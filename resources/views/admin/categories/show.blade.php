@extends('layouts.admin')

@section('title', $category->name)

@section('hTitle')
    @include('parts.backBtn', ['url' => "/admin/categories?page=$pageId", 'title' => 'back to the list'])
    {{ $category->name }}
@endsection

@section('breadcrumbs')
    <li><a href="{{ url("/admin/categories?page=$pageId") }}">Categories</a></li>
    <li class="active">{{ $category->name }} Category</li>
@endsection

@section('content')

@endsection