<div class="row">
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
            {!! Form::label('group_id', 'Group') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('group_id', $groups, null, ['class' => 'form-control', 'placeholder' => 'select group', 'autofocus']) !!}
            @if ($errors->has('group_id'))
                <span class="help-block"><strong>{{ $errors->first('group_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name', 'Category Name') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="Serial Number"><i class="fa fa-hashtag fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'group name', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('name'))
                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('icon_id') ? ' has-error' : '' }}">
            <strong>Icons</strong>
            <ul class="list-unstyled">
                @foreach($icons as $id => $name)
                    <li><label>{!! Form::radio("icon_id", $id); !!} {!! $name !!}</label></li>
                @endforeach
            </ul>
            @if ($errors->has('icon_id'))
                <span class="help-block"><strong>{{ $errors->first('icon_id') }}</strong></span>
            @endif
        </div>
    </div>
</div>
<!-- Textarea with ckeditor -->
@include('parts.textarea', ['name' => 'description', 'title' => 'Description'])