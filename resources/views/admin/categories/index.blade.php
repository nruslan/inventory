@extends('layouts.admin')

@section('title', 'Categories')

@section('hTitle')
    Categories
@endsection

@section('breadcrumbs')
    <li class="active">Categories</li>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Categories list <span class="badge">{{ $categories->total() }}</span></h3>
            <a class="btn btn-primary btn-xs aBtn" data-toggle="tooltip" data-placement="top" title="Add new Group" href="{{ url('/admin/categories/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="table-responsive">
                <table class="table table-hover table-condensed" role="grid">
                    <thead>
                    <tr role="row">
                        <th>#</th>
                        <th>Name</th>
                        <th class="text-center">Icon</th>
                        <th colspan="3"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr role="row">
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $category->name }}</td>
                            <td class="text-center">{!! $category->html_icon !!}</td>
                            <td class="text-center"><a class="btn btn-default btn-xs aBtn" href="{{ url("admin/categories/$category->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                            <td class="text-center"><a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/categories/$category->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                            <td class="text-center">@include('parts.trashBtnSm', ['action' => ['Admin\CategoriesController@destroy', $category->id], 'btnTitle' => "$category->name Category"])</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th class="text-center">Icon</th>
                        <th colspan="3"></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            @include('parts.session-message')
            {{ $categories->links('vendor.pagination.admin-lte') }}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection