@extends('layouts.admin')

@section('title', 'Model')

@section('hTitle')
    Edit Model
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit Model</h3>
                </div>
                <!-- /.box-header -->
                {!! Form::model($model, ['method' => 'PATCH', 'action' => ['Admin\ImodelsController@update', $model->id], 'id' => 'myForm']) !!}
                    <div class="box-body">
                        @include('admin.models.formBody')
                    </div>
                    <div class="box-footer">
                        @include('parts.saveCancelBtns', ['cancel' => 'admin/models'])
                    </div>
                {!! Form::close() !!}
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $("#myForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection