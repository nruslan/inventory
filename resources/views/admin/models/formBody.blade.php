<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="form-group{{ $errors->has('type_id') ? ' has-error' : '' }}">
            {!! Form::label('type_id', 'Type') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('type_id', $types, null, ['class' => 'form-control', 'placeholder' => 'select type', 'autofocus']) !!}
            @if ($errors->has('type_id'))
                <span class="help-block"><strong>{{ $errors->first('type_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('brand_id') ? ' has-error' : '' }}">
            {!! Form::label('brand_id', 'Brand') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            {!! Form::select('brand_id', $brands, null, ['class' => 'form-control', 'placeholder' => 'select brand', 'autofocus']) !!}
            @if ($errors->has('brand_id'))
                <span class="help-block"><strong>{{ $errors->first('brand_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('name_number') ? ' has-error' : '' }}">
            {!! Form::label('name_number', 'Model Name/Number') !!} <i class="fa fa-asterisk text-danger" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="required field"></i>
            <div class="input-group">
                <span class="input-group-addon" data-toggle="tooltip" data-placement="left" title="address"><i class="fa fa-tag fa-fw" aria-hidden="true"></i></span>
                {!! Form::text('name_number', null, ['class' => 'form-control', 'placeholder' => 'model name or number', 'autocomplete' => 'off']) !!}
            </div>
            @if ($errors->has('name_number'))
                <span class="help-block"><strong>{{ $errors->first('name_number') }}</strong></span>
            @endif
        </div>
        <!-- Textarea with ckeditor -->
        @include('parts.textarea', ['name' => 'description', 'title' => 'Description'])
    </div>
</div>