@extends('layouts.admin')

@section('title', "$model->name_number")

@section('hTitle')
    @include('parts.backBtn', ['url' => "admin/models?page=$pageId", 'title' => 'back to the list'])
    Models {{ $model->name_number }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{ $model->name_number }}</h3>
                    <a href="{{ url("admin/models/$model->id/edit") }}" class="btn btn-primary pull-right aBtn"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> Edit</a>
                </div><!-- /.box-header -->

                <div class="box-body">
                    @include('parts.session-message')
                    <div class="row">
                        <div class="col-lg-6">

                        </div>
                        <div class="col-lg-6">

                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    @include('parts.trashBtn', ['action' => ['Admin\ImodelsController@destroy', $model->id], 'btnTitle' => $model->name_number])
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        // JavaScript Document
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                // other options
            });

            $(".deleteBtnForm").submit(function() {
                $(this).closest('form').find(':submit').prop('disabled',true).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            });
        });
    </script>
@endsection