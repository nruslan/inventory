@extends('layouts.admin')

@section('hTitle')
    Models
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Models List <a class="btn btn-primary btn-xs aBtn" href="{{ url('admin/models/create') }}"><i class="fa fa-fw fa-plus"></i> Add</a></h3>
                    <div class="box-tools">
                        {{ $imodels->links('vendor.pagination.admin-lte') }}
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    @include('parts.session-message')
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Brand/Model</th>
                            <th>Type</th>
                            <th colspan="2"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($imodels as $imodel)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td><strong>{{ $imodel->brand->name }}</strong> &bullet; {{ $imodel->name_number }}</td>
                                <td>{{ $imodel->type->name }}</td>
                                <td><a class="btn btn-default btn-xs aBtn" href="{{ url("admin/models/$imodel->id") }}"><i class="fa fa-fw fa-eye"></i></a></td>
                                <td><a class="btn btn-primary btn-xs aBtn" href="{{ url("admin/models/$imodel->id/edit") }}"><i class="fa fa-fw fa-pencil"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $imodels->links('vendor.pagination.admin-lte') }}
                </div>
            </div>
        </div>
    </div>
@endsection
