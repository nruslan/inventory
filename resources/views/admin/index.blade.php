@extends('layouts.admin')

@section('hTitle')
    Welcome to SB2 Inventory
    <small>Control panel</small>
@endsection

@section('content')
    @include('parts.modal-message')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Admin Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection