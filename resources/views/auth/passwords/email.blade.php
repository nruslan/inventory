@extends('layouts.auth')
@section('title', 'Reset Password')
@section('content')
    <div class="login-box-body">
        @if (session('status'))<div class="alert alert-success">{{ session('status') }}</div>@endif
        <p class="login-box-msg">Reset Password</p>
        <form method="POST" action="{{ route('password.email') }}" id="logInForm">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>@endif
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-fw fa-send"></i> Send Password Reset Link</button>
            </div>
        </form>
    </div>
@endsection
