<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    {!! Form::label($name, $title) !!}
    {!! Form::textarea($name, null, ['class' => 'form-control', 'id' => 'textarea_1', 'autocomplete' => 'off']) !!}
    @if ($errors->has($name))<span class="help-block"><strong>{{ $errors->first($name) }}</strong></span>@endif
</div>