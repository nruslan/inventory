{!! Form::open(['method' => 'DELETE', 'action' => $action, 'class' => 'deleteBtnForm']) !!}
<button type="submit" class="btn btn-danger btn-xs" data-toggle="confirmation"
        data-btn-ok-label="Continue" data-btn-ok-icon="glyphicon glyphicon-share-alt"
        data-btn-ok-class="btn-success"
        data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
        data-btn-cancel-class="btn-danger"
        data-content=""
        data-popout="true" title="Remove all {{ $btnTitle }}?">
    <i class="fa fa-trash-o fa-fw"></i> Remove all
</button>
{!! Form::close() !!}