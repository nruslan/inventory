{!! Form::open(['method' => 'DELETE', 'action' => $action]) !!}
<button type="submit" class="btn btn-danger {{ $class ?? '' }}" data-toggle="confirmation"
        data-placement="{{ $placement ?? 'top' }}"
        data-popout="true" title="{{ $btnTitle }}?">
    <i class="fa fa-trash-o fa-fw"></i>{{ $name ?? '' }}
</button>
{!! Form::close() !!}