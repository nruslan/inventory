@if(session()->has('message-success'))
    <div class="col-lg-12">
        <div class="alert alert-success alert-dismissible text-center lead" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <i class="fa fa-thumbs-up fa-lg"></i> {!! session()->get('message-success') !!}
        </div>
    </div>
@endif

@if(session()->has('message-delete'))
    <div class="col-lg-12">
        <div class="alert alert-warning alert-dismissible text-center lead" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <i class="fa fa-trash-o fa-lg"></i> {!! session()->get('message-delete') !!}
        </div>
    </div>
@endif