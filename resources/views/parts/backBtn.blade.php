<a class="btn btn-primary aBtn" href="{{ $url }}" data-toggle="tooltip" data-placement="top" title="{{ $title }}">
    <i class="fa fa-fw fa-angle-double-left" aria-hidden="true"></i> Back
</a>
