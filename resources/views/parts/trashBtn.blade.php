{!! Form::open(['method' => 'DELETE', 'action' => $action]) !!}
<button type="submit" class="btn btn-danger {{ $class ?? '' }}" data-toggle="confirmation"
        data-placement="{{ $position ?? 'top' }}"
        data-btn-ok-label="Continue" data-btn-ok-icon="glyphicon glyphicon-share-alt"
        data-btn-ok-class="btn-success"
        data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
        data-btn-cancel-class="btn-danger"
        data-content=""
        data-popout="true" title="{{ $btnTitle }}?">
    <i class="fa fa-trash-o fa-fw"></i>{{ $name ?? '' }}
</button>
{!! Form::close() !!}