<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Ruslan Niyazimbetov">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | SB2 Inventory</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
    <link rel="shortcut icon" href="/favicon.ico">
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
@include('parts.modal-message')
<div class="wrapper" id="app">
    <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>iSB</b>2</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>inventory</b>SB2</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ Auth::user()->profile_picture }}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{ Auth::user()->shortened_name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{ Auth::user()->profile_picture }}" class="img-circle" alt="User Image">
                                <p>
                                    {{ Auth::user()->shortened_name }}
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-4 text-center">
                                        <a href="{{ url('admin') }}" title="administration panel">Admin</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#"></a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#"></a>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ route('logout') }}" class="btn btn-default btn-flat"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Auth::user()->profile_picture }}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->shortened_name }}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">USER NAVIGATION</li>
                <li class="treeview @if(request()->segment(1) == 'home')active menu-open @endif">
                    <a href="#"><i class="fa fa-map-marker"></i> <span>Home</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li @if(request()->segment(2) == 'dashboard')class="active" @endif><a href="{{ url('home/dashboard') }}" class="aBtn"><i class="fa fa-dashboard"></i> <span>User Dashboard</span></a></li>
                        <li @if(request()->segment(2) == 'password-manager')class="active" @endif><a href="{{ url('home/password-manager') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>Password Manager</span></a></li>
                        <li @if(request()->segment(2) == 'how-to')class="active" @endif><a href="{{ url('home/accounts') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>How To</span></a></li>
                    </ul>
                </li>
                <li class="header">INVENTORY</li>
                <li @if(request()->segment(1) == 'dashboard')class="active" @endif><a href="{{ url('dashboard') }}" class="aBtn"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li @if(request()->segment(1) == 'log')class="active" @endif><a href="{{ url('log') }}" class="aBtn"><i class="fa fa-pencil-square-o"></i> <span>Log</span></a></li>
                
                <li @if(request()->segment(1) == 'items' or request()->segment(1) == 'item')class="active" @endif><a href="{{ url('items') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>Items</span></a></li>
                <li @if(request()->segment(1) == 'ips' or request()->segment(1) == 'ip')class="active" @endif><a href="{{ url('ips') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>IP Addresses</span></a></li>
                <li @if(request()->segment(1) == 'accounts')class="active" @endif><a href="{{ url('accounts') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>Web Accounts</span></a></li>
                <li class="treeview @if(request()->segment(1) == 'documents')active menu-open @endif">
                    <a href="#"><i class="fa fa-file-text-o"></i> <span>Documents</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li @if(request()->segment(2) == '')class="active" @endif><a href="{{ url('documents') }}" class="aBtn"><i class="fa fa-file-o"></i> <span>All</span></a></li>
                        <li @if(request()->segment(2) == 'invoices')class="active" @endif><a href="{{ url('documents/invoices') }}" class="aBtn"><i class="fa fa-file-text"></i> <span>Invoices</span></a></li>
                    </ul>
                </li>

                <li class="treeview @if(request()->segment(1) == 'room')active menu-open @endif">
                    <a href="#"><i class="fa fa-server"></i> <span>Server Rooms</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li @if(request()->segment(2) == 'maintenance')class="active" @endif><a href="{{ url('room/maintenance') }}" class="aBtn"><i class="fa fa-list"></i> <span>Maintenance</span></a></li>
                        <li @if(request()->segment(3) == 'a213')class="active" @endif><a href="{{ url('room/code/a213') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>Admin</span></a></li>
                        <li @if(request()->segment(3) == 'mv508')class="active" @endif><a href="{{ url('room/code/mv508') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>MV-FB office</span></a></li>
						<li @if(request()->segment(3) == 'mv525')class="active" @endif><a href="{{ url('room/code/mv525') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>MV-Kitchen</span></a></li>
                        <li @if(request()->segment(3) == 'a235')class="active" @endif><a href="{{ url('room/code/a235') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>Arts&Crafts Electrical Room</span></a></li>
                        <li @if(request()->segment(3) == 'mg646')class="active" @endif><a href="{{ url('room/code/mg646') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>Mesquite Grill</span></a></li>
                        <li @if(request()->segment(3) == 'dv3814')class="active" @endif><a href="{{ url('room/code/dv3814') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>DesertView</span></a></li>
                        <li @if(request()->segment(3) == 'p707')class="active" @endif><a href="{{ url('room/code/p707') }}" class="aBtn"><i class="fa fa-th-large"></i> <span>Preserve</span></a></li>
                    </ul>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('hTitle')
            </h1>
            @yield('breadcrumbs')
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b><a href="https://adminlte.io">AdminLTE</a> Version</b> 2.4.10 | Powered by <strong>Laravel {{ app()->version() }}</strong> &amp; <strong>PHP {{ phpversion() }}</strong>
        </div>
        <strong>&copy; 2018 @if(date('Y') != 2018)- {{ date('Y') }}@endif <a href="http://nruslan.com">NRuslan</a> All rights reserved.</strong>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                <p>Will be 23 on April 24th</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-user bg-yellow"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                <p>New phone +1(800)555-1234</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                <p>nora@example.com</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="menu-icon fa fa-file-code-o bg-green"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                <p>Execution time 5 seconds</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Custom Template Design
                                <span class="label label-danger pull-right">70%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Update Resume
                                <span class="label label-success pull-right">95%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Laravel Integration
                                <span class="label label-warning pull-right">50%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <h4 class="control-sidebar-subheading">
                                Back End Framework
                                <span class="label label-primary pull-right">68%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Some information about this general settings option
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Allow mail redirect
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Other sets of options are available
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Expose author name in posts
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Allow the user to show his name in blog posts
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <h3 class="control-sidebar-heading">Chat Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Show me as online
                            <input type="checkbox" class="pull-right" checked>
                        </label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Turn off notifications
                            <input type="checkbox" class="pull-right">
                        </label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Delete chat history
                            <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                        </label>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- Scripts -->
<script>var CKEDITOR_BASEPATH = '/ckeditor/';</script>
<script src="{{ asset('js/app.js') }}"></script>
<script>CKEDITOR.replace('textarea_1');</script>
<script>
    $(document).ready(function() {
        $("#warningModal").modal();

        $('[data-toggle="tooltip"]').tooltip();

        $(".aBtn").on('click', function(){
            $(this).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            $(this).addClass('disabled');
        });

        $("#myForm").submit(function() {
            $(this).closest('form').find(':submit').prop('disabled',true).children("i.fa").replaceWith('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
        });
    });
</script>
@yield('scripts')
</body>
</html>
